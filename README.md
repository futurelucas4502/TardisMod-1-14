# New TARDIS Mod
Explore, Pilot and Customise a Doctor Who inspired TARDIS Time Machine!

<div style="text-align: center;">
<a href="https://www.curseforge.com/minecraft/mc-mods/new-tardis-mod/files">
<img src="https://i.imgur.com/FE6S0d6.png" alt="drawing" width="203" height="192"/>
</a>
</div>
<p align="center">
  <img src="http://cf.way2muchnoise.eu/versions/new-tardis-mod.svg">
  <img src="http://cf.way2muchnoise.eu/new-tardis-mod.svg"> <br>
</p>
<p align="center">
  <b>Links:</b><br>
  <a href="https://discord.gg/wdhbhTt">Discord</a> |
  <a href="https://www.curseforge.com/minecraft/mc-mods/new-tardis-mod">Download</a> 
  <br><br>
</p>

<table style="width:10%">
  <tr>
    <th>Credit</th>
    <th>Contribution</th>
  </tr>
  <tr>
    <td><a href="https://twitter.com/Spectre09871">Spectre0987</a></td>
    <td>Owner, Code</td>
  </tr>
  <tr>
    <td><a href="https://twitter.com/Suff1999">Suff</a></td>
    <td>Code</td>
  </tr>
  <tr>
    <td><a href="https://twitter.com/VandendaelenC">LotuxPunk</a></td>
    <td>Code</td>
  </tr>
  <tr>
    <td><a href="https://twitter.com/50ap5ud5">50ap5ud5</a></td>
    <td>Code</td>
  </tr>
  <tr>
    <td><a href="http://arcanetimes.com/">DocArcane</a></td>
    <td>Models/Textures</td>
  </tr>
</table>

## Developers
You can integrate the Tardis Mod as an API into your project by adding this mod as a dependency via a third party maven such as <a href="https://jitpack.io/#com.gitlab.Spectre0987/TardisMod-1-14">Jitpack</a>.

## Screenshots
Below are some screenshots of the mod
<div style="text-align: center;"> 
 <img src="https://i.imgur.com/jMV4Rlq.gif" width="400">
</div>
<br>
<div style="text-align: center;"> 
<a href="https://gyazo.com/73925b63ede8bc7c3649676ac5e9665d"><img src="https://i.gyazo.com/73925b63ede8bc7c3649676ac5e9665d.gif" alt="Tardis Console Flight Events" width="400"/></a>
</div>
<br>
<div style="text-align: center;"> 
 <img src="https://i.imgur.com/gxcurHE.png/" width="400">
</div>