package net.tardis.api.space.cap;

import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.tardis.mod.cap.SpaceDimensionCapability;

public class SpaceCapabilities {

    @CapabilityInject(IOxygenSealer.class)
    public static final Capability<IOxygenSealer> OXYGEN_SEALER = null;
    
    /** Instance of the SpaceDim Capability
     * <p> Implement your version of ISpaceDimProperties by implement ISpaceDimProperties in your class
     * In {@linkplain net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent FMLCommonSetupEvent} call 
     * <p> {@code CapabilityManager.INSTANCE.register(ISpaceDimProperties.class, new ISpaceDimProperties.Storage(), () -> new MySpaceCapabilityImplementationClass(null));}
     * */
    @CapabilityInject(ISpaceDimProperties.class)
    public static final Capability<ISpaceDimProperties> SPACE_DIM_PROPERTIES = null;
}
