package net.tardis.mod.cap.entity;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.RegistryKey;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.config.TConfig;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.SyncPlayerMessage;
import net.tardis.mod.world.dimensions.TDimensions;

public class PlayerDataCapability implements IPlayerData {

    private PlayerEntity player;
    private SpaceTimeCoord coord = SpaceTimeCoord.UNIVERAL_CENTER;
    private double displacement;
    private int shakingTicks = 0;
    private float shakeMagnitude = 1F;
    private int countdownTicks = 0;

    public PlayerDataCapability(PlayerEntity ent) {
        this.player = ent;
    }

    @Override
    public CompoundNBT serializeNBT() {
        CompoundNBT tag = new CompoundNBT();
        tag.put("coord", this.coord.serialize());
        tag.putDouble("displacement", this.displacement);
        tag.putInt("shaking_ticks", this.shakingTicks);
        tag.putFloat("shake_magnitude", this.shakeMagnitude);
        tag.putInt("countdown_ticks", this.countdownTicks);
        return tag;
    }

    @Override
    public void deserializeNBT(CompoundNBT nbt) {
        if (nbt.contains("coord"))
            this.coord = SpaceTimeCoord.deserialize(nbt.getCompound("coord"));
        this.displacement = nbt.getDouble("displacement");
        this.shakingTicks = nbt.getInt("shaking_ticks");
        if (nbt.contains("shake_magnitude"))
            this.shakeMagnitude = nbt.getFloat("shake_magnitude");
        this.countdownTicks = nbt.getInt("countdown_ticks");

    }

    @Override
    public SpaceTimeCoord getDestination() {
        return coord;
    }

    @Override
    public void setDestination(SpaceTimeCoord coord) {
        this.coord = coord;
    }

    @Override
    public void tick() {
        if (player instanceof ServerPlayerEntity) {
            ServerPlayerEntity serverPlayer = (ServerPlayerEntity) player;
            if (serverPlayer.getServerWorld() != null && serverPlayer.getServerWorld().getDimensionKey() == TDimensions.VORTEX_DIM) {
                if (serverPlayer.getPosY() < 0) {
                    RegistryKey<World> worldKey = RegistryKey.getOrCreateKey(Registry.WORLD_KEY, coord.getDimRL());
                    ServerWorld world = player.world.getServer().getWorld(worldKey);
                    if (world != null) {
                        if (world.getDimensionKey() == TDimensions.VORTEX_DIM) {
                            ServerWorld worldDest = player.world.getServer().getWorld(World.OVERWORLD);
                            world = worldDest; //Set dimension to Overworld if they try to teleport whilst in the vortex
                        }
                        BlockPos pos = coord.getPos();
                        serverPlayer.fallDistance = 0;
                        serverPlayer.teleport(world, pos.getX() + 0.5, pos.getY() + 1, pos.getZ() + 0.5, 0, 0);
                    }
                }
            }
        } else {//Client- only stuff
            //Shake dat ass
            if (this.shakingTicks > 0 && TConfig.CLIENT.interiorShake.get()) {
                player.rotationYaw = WorldHelper.getFixedRotation(player.rotationYaw) + (player.getRNG().nextFloat() - 0.5F) * this.shakeMagnitude;
                player.rotationPitch += (player.getRNG().nextFloat() - 0.5F) * this.shakeMagnitude;
            }
        }
        if (this.countdownTicks > 0)//Decrease countdown ticks on common to allow for sanity checks to be made in other objects
            --this.countdownTicks;

        if (this.shakingTicks > 0)
            --this.shakingTicks;

    }

    @Override
    public double getDisplacement() {
        return this.displacement;
    }

    @Override
    public void calcDisplacement(BlockPos start, BlockPos finish) {
        this.displacement = Math.sqrt(start.distanceSq(finish));
    }

    @Override
    public void setShaking(int shakingTicks, float magnitude) {
        this.shakingTicks = shakingTicks;
        this.shakeMagnitude = magnitude;
    }

    @Override
    public int getShaking() {
        return this.shakingTicks;
    }

    @Override
    public void setShaking(int shakingTicks) {
        this.setShaking(shakingTicks, 1F);
    }

    @Override
    public void update() {
        if (!player.world.isRemote)
            Network.sendTo(new SyncPlayerMessage(player.getEntityId(), this.serializeNBT()), (ServerPlayerEntity) player);
    }

    @Override
    public void startCountdown(int time) {
        this.countdownTicks = time;
    }

    @Override
    public int getCountdown() {
        return this.countdownTicks;
    }
}
