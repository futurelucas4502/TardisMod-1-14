package net.tardis.mod.client.guis.manual;

import com.google.common.collect.Lists;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
import org.apache.logging.log4j.Level;

import java.util.List;

public class Chapter {

    private List<Page> pages = Lists.newArrayList();
    private String displayName;

    public Chapter(String display, List<Page> pages){
        this.displayName = display;
        this.pages.addAll(pages);
    }

    public List<Page> getPages(){
        return this.pages;
    }

    public static Chapter read(ResourceLocation id, JsonObject object){
       try{
           String display = object.get("display_name").getAsString();

           List<Page> pages = Lists.newArrayList();

            for(JsonElement e : object.get("pages").getAsJsonArray()){
                ResourceLocation pageID = getPageResourceLocation(new ResourceLocation(e.getAsString()));
                pages.addAll(Page.read(pageID));
            }

            return new Chapter(display, null);

       }
       catch(Exception e){
           Tardis.LOGGER.log(Level.ALL, "Caught error in manual chapter " + id.toString());
           return null;
       }
    }

    public static ResourceLocation getPageResourceLocation(ResourceLocation loc){
        return new ResourceLocation(loc.getNamespace(), "manual/page/" + loc.getPath() + ".json");
    }

}
