package net.tardis.mod.client.guis.widgets;

import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.widget.TextFieldWidget;
import net.minecraft.util.text.TranslationTextComponent;


/**
 * Created by 50ap5ud5
 * on 17 Mar 2020 @ 9:51:12 pm
 */

/*
 * A custom text field widget that doesn't allow user to click in the textbox
 * Mainly used to display text without needing to consider things such as :
 * - Text going over the gui texture if too long
 * - Text not contrasting enough with gui texture
 */
public class DisabledTextFieldWidget extends TextFieldWidget {

    public DisabledTextFieldWidget(FontRenderer fontIn, int xIn, int yIn, int widthIn, int heightIn, TranslationTextComponent msg) {
        super(fontIn, xIn, yIn, widthIn, heightIn, msg);
    }

    @Override
    public boolean mouseClicked(double mouseX, double mouseY, int button) {
        return false;
    }


}
