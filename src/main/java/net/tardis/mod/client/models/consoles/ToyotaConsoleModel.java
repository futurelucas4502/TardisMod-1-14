package net.tardis.mod.client.models.consoles;
// Made with Blockbench 3.7.4
// Exported for Minecraft version 1.15
// Paste this class into your mod and generate all required imports

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.tardis.mod.client.models.LightModelRenderer;
import net.tardis.mod.client.models.TileModel;
import net.tardis.mod.controls.DoorControl;
import net.tardis.mod.controls.FacingControl;
import net.tardis.mod.controls.HandbrakeControl;
import net.tardis.mod.controls.LandingTypeControl;
import net.tardis.mod.controls.LandingTypeControl.EnumLandType;
import net.tardis.mod.controls.RefuelerControl;
import net.tardis.mod.controls.ThrottleControl;
import net.tardis.mod.controls.XControl;
import net.tardis.mod.controls.YControl;
import net.tardis.mod.controls.ZControl;
import net.tardis.mod.entity.DoorEntity;
import net.tardis.mod.enums.EnumDoorState;
import net.tardis.mod.helper.ModelHelper;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.ToyotaSpinnyTile;
import net.tardis.mod.tileentities.consoles.ToyotaConsoleTile;

public class ToyotaConsoleModel extends EntityModel<Entity> implements TileModel<ToyotaConsoleTile> {
	private final ModelRenderer base_console;
	private final ModelRenderer centre_column;
	private final LightModelRenderer rotorbeam_glow;
	private final ModelRenderer bone176;
	private final ModelRenderer bone192;
	private final ModelRenderer bone197;
	private final ModelRenderer bone198;
	private final ModelRenderer bone199;
	private final ModelRenderer bone130;
	private final ModelRenderer bone131;
	private final ModelRenderer bone42;
	private final ModelRenderer bone44;
	private final ModelRenderer bone45;
	private final ModelRenderer bone46;
	private final ModelRenderer bone47;
	private final ModelRenderer bone137;
	private final ModelRenderer bone36;
	private final ModelRenderer bone38;
	private final ModelRenderer bone39;
	private final ModelRenderer bone40;
	private final ModelRenderer bone41;
	private final ModelRenderer top_column;
	private final ModelRenderer bone145;
	private final ModelRenderer bone146;
	private final ModelRenderer bone30;
	private final ModelRenderer bone32;
	private final ModelRenderer bone33;
	private final ModelRenderer bone34;
	private final ModelRenderer bone35;
	private final ModelRenderer bone152;
	private final ModelRenderer bone25;
	private final ModelRenderer bone26;
	private final ModelRenderer bone27;
	private final ModelRenderer bone28;
	private final ModelRenderer bone29;
	private final ModelRenderer bone158;
	private final ModelRenderer bone159;
	private final ModelRenderer bone2;
	private final ModelRenderer bone3;
	private final ModelRenderer bone4;
	private final ModelRenderer bone5;
	private final ModelRenderer bone6;
	private final ModelRenderer bone165;
	private final ModelRenderer bone8;
	private final ModelRenderer bone9;
	private final ModelRenderer bone10;
	private final ModelRenderer bone11;
	private final ModelRenderer bone12;
	private final ModelRenderer bone171;
	private final ModelRenderer bone172;
	private final ModelRenderer bone20;
	private final ModelRenderer bone21;
	private final ModelRenderer bone22;
	private final ModelRenderer bone23;
	private final ModelRenderer bone24;
	private final ModelRenderer bone178;
	private final ModelRenderer bone14;
	private final ModelRenderer bone15;
	private final ModelRenderer bone16;
	private final ModelRenderer bone17;
	private final ModelRenderer bone18;
	private final ModelRenderer bone206;
	private final ModelRenderer bone207;
	private final ModelRenderer bone208;
	private final ModelRenderer bone209;
	private final ModelRenderer bone210;
	private final ModelRenderer bone211;
	private final ModelRenderer bone200;
	private final ModelRenderer bone201;
	private final ModelRenderer bone202;
	private final ModelRenderer bone203;
	private final ModelRenderer bone204;
	private final ModelRenderer bone205;
	private final ModelRenderer base;
	private final LightModelRenderer base_glow;
	private final ModelRenderer bone153;
	private final ModelRenderer bone154;
	private final ModelRenderer bone155;
	private final ModelRenderer bone156;
	private final ModelRenderer bone157;
	private final ModelRenderer bone13;
	private final ModelRenderer bone140;
	private final ModelRenderer bone141;
	private final ModelRenderer bone142;
	private final ModelRenderer bone143;
	private final ModelRenderer bone144;
	private final ModelRenderer bone7;
	private final ModelRenderer bone134;
	private final ModelRenderer bone135;
	private final ModelRenderer bone136;
	private final ModelRenderer bone138;
	private final ModelRenderer bone139;
	private final ModelRenderer bone;
	private final ModelRenderer bone147;
	private final ModelRenderer bone148;
	private final ModelRenderer bone149;
	private final ModelRenderer bone150;
	private final ModelRenderer bone151;
	private final ModelRenderer bone129;
	private final ModelRenderer bone123;
	private final ModelRenderer bone54;
	private final ModelRenderer bone57;
	private final ModelRenderer bone58;
	private final ModelRenderer bone59;
	private final ModelRenderer bone60;
	private final ModelRenderer bone117;
	private final ModelRenderer bone48;
	private final ModelRenderer bone50;
	private final ModelRenderer bone51;
	private final ModelRenderer bone52;
	private final ModelRenderer bone53;
	private final ModelRenderer bone85;
	private final ModelRenderer bone86;
	private final ModelRenderer bone109;
	private final ModelRenderer bone110;
	private final ModelRenderer bone111;
	private final ModelRenderer bone112;
	private final ModelRenderer bone113;
	private final ModelRenderer bone114;
	private final ModelRenderer bone115;
	private final ModelRenderer bone116;
	private final ModelRenderer bone118;
	private final ModelRenderer bone119;
	private final ModelRenderer bone81;
	private final ModelRenderer bone82;
	private final ModelRenderer bone83;
	private final ModelRenderer bone84;
	private final ModelRenderer bone61;
	private final ModelRenderer bone62;
	private final ModelRenderer bone63;
	private final ModelRenderer bone64;
	private final ModelRenderer bone65;
	private final ModelRenderer bone66;
	private final ModelRenderer bone69;
	private final ModelRenderer bone70;
	private final ModelRenderer bone71;
	private final ModelRenderer bone72;
	private final ModelRenderer bone73;
	private final ModelRenderer bone74;
	private final ModelRenderer bone75;
	private final ModelRenderer bone76;
	private final ModelRenderer bone77;
	private final ModelRenderer bone78;
	private final ModelRenderer bone79;
	private final ModelRenderer bone80;
	private final ModelRenderer bone87;
	private final ModelRenderer bone88;
	private final ModelRenderer bone67;
	private final ModelRenderer bone68;
	private final ModelRenderer bone89;
	private final ModelRenderer bone90;
	private final ModelRenderer bone91;
	private final ModelRenderer bone92;
	private final ModelRenderer bone93;
	private final ModelRenderer bone94;
	private final ModelRenderer bone95;
	private final ModelRenderer bone96;
	private final ModelRenderer bone97;
	private final ModelRenderer bone98;
	private final ModelRenderer bone55;
	private final ModelRenderer bone56;
	private final ModelRenderer bone160;
	private final ModelRenderer bone161;
	private final ModelRenderer bone162;
	private final ModelRenderer bone163;
	private final ModelRenderer bone164;
	private final ModelRenderer bone166;
	private final ModelRenderer bone167;
	private final ModelRenderer bone168;
	private final ModelRenderer bone169;
	private final ModelRenderer bone170;
	private final ModelRenderer bone43;
	private final ModelRenderer bone49;
	private final ModelRenderer bone120;
	private final ModelRenderer bone121;
	private final ModelRenderer bone122;
	private final ModelRenderer bone124;
	private final ModelRenderer bone125;
	private final ModelRenderer bone126;
	private final ModelRenderer bone127;
	private final ModelRenderer bone128;
	private final ModelRenderer bone132;
	private final ModelRenderer bone133;
	private final ModelRenderer bone37;
	private final ModelRenderer bone104;
	private final ModelRenderer bone105;
	private final ModelRenderer bone106;
	private final ModelRenderer bone107;
	private final ModelRenderer bone108;
	private final ModelRenderer bone31;
	private final ModelRenderer bone99;
	private final ModelRenderer bone100;
	private final ModelRenderer bone101;
	private final ModelRenderer bone102;
	private final ModelRenderer bone103;
	private final ModelRenderer components;
	private final ModelRenderer south_west;
	private final ModelRenderer bone174;
	private final ModelRenderer incrementmodifier_rotate_Y60;
	private final ModelRenderer randomiser;
	private final LightModelRenderer randomiser_glow;
	private final ModelRenderer communicator;
	private final ModelRenderer landtype_selector;
	private final ModelRenderer sonicport;
	private final LightModelRenderer southwest_glow;
	private final ModelRenderer bone190;
	private final ModelRenderer bone212;
	private final ModelRenderer bone239;
	private final ModelRenderer bone215;
	private final ModelRenderer north_west;
	private final ModelRenderer bone173;
	private final ModelRenderer dimensioncontrol;
	private final LightModelRenderer dimensioncontrol_glow;
	private final ModelRenderer dimensionrotate_Y35;
	private final ModelRenderer bone228;
	private final ModelRenderer dimensionrotate_Y65;
	private final ModelRenderer dimensionrotate_Y50;
	private final ModelRenderer directioncontrol_rotate_Y90;
	private final ModelRenderer bone177;
	private final ModelRenderer bone227;
	private final ModelRenderer north;
	private final ModelRenderer bone179;
	private final ModelRenderer door_rotate_x_100;
	private final ModelRenderer fastreturn_rotate_X65;
	private final ModelRenderer landtype_rotate_Y70;
	private final ModelRenderer refuel;
	private final LightModelRenderer north_glow;
	private final ModelRenderer bone233;
	private final ModelRenderer bone234;
	private final ModelRenderer bone194;
	private final ModelRenderer bone183;
	private final ModelRenderer bone182;
	private final ModelRenderer bone225;
	private final ModelRenderer door2_rotate_x_100;
	private final ModelRenderer north_east;
	private final ModelRenderer bone180;
	private final ModelRenderer bone181;
	private final ModelRenderer bone184;
	private final LightModelRenderer monitor_glow_optional;
	private final LightModelRenderer monitor_glow;
	private final ModelRenderer bone186;
	private final ModelRenderer bone185;
	private final ModelRenderer south_east;
	private final ModelRenderer bone187;
	private final LightModelRenderer telepathic_glow;
	private final ModelRenderer south;
	private final ModelRenderer bone188;
	private final ModelRenderer throttle_rotate_X80;
	private final ModelRenderer bone220;
	private final ModelRenderer bone221;
	private final ModelRenderer handbrake_rotate_Y130;
	private final ModelRenderer bone196;
	private final ModelRenderer stabiliser;
	private final LightModelRenderer stabiliser_glow;
	private final LightModelRenderer Xincrement;
	private final ModelRenderer bone222;
	private final LightModelRenderer Yincrement;
	private final ModelRenderer bone223;
	private final LightModelRenderer Zincrement;
	private final ModelRenderer bone224;
	private final LightModelRenderer south_glow;
	private final ModelRenderer bone235;
	private final ModelRenderer bone236;
	private final ModelRenderer bone237;
	private final ModelRenderer bone238;
	private final ModelRenderer bone219;
	private final ModelRenderer rotor_top_translate_8;
	private final LightModelRenderer rotortop_glow;
	private final ModelRenderer rotor_bottom_translate_14;
	private final LightModelRenderer rotorbottom_glow;

	public ToyotaConsoleModel() {
		textureWidth = 256;
		textureHeight = 256;

		base_console = new ModelRenderer(this);
		base_console.setRotationPoint(0.0F, 24.0F, 0.0F);
		

		centre_column = new ModelRenderer(this);
		centre_column.setRotationPoint(0.0F, 0.0F, 0.0F);
		base_console.addChild(centre_column);
		centre_column.setTextureOffset(0, 68).addBox(-2.0F, -53.55F, -2.0F, 4.0F, 16.0F, 4.0F, 0.0F, false);
		centre_column.setTextureOffset(0, 18).addBox(-2.0F, -86.55F, -2.0F, 4.0F, 16.0F, 4.0F, 0.0F, false);

		rotorbeam_glow = new LightModelRenderer(this);
		rotorbeam_glow.setRotationPoint(0.0F, -37.5F, 0.0F);
		centre_column.addChild(rotorbeam_glow);
		setRotationAngle(rotorbeam_glow, 0.0F, -0.5236F, 0.0F);
		rotorbeam_glow.setTextureOffset(88, 124).addBox(-1.0F, -38.05F, 5.25F, 2.0F, 26.0F, 2.0F, 0.0F, false);

		bone176 = new ModelRenderer(this);
		bone176.setRotationPoint(0.0F, 0.0F, 0.0F);
		rotorbeam_glow.addChild(bone176);
		setRotationAngle(bone176, 0.0F, -1.0472F, 0.0F);
		bone176.setTextureOffset(88, 124).addBox(-1.0F, -38.05F, 5.25F, 2.0F, 26.0F, 2.0F, 0.0F, false);

		bone192 = new ModelRenderer(this);
		bone192.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone176.addChild(bone192);
		setRotationAngle(bone192, 0.0F, -1.0472F, 0.0F);
		bone192.setTextureOffset(88, 124).addBox(-1.0F, -38.05F, 5.25F, 2.0F, 26.0F, 2.0F, 0.0F, false);

		bone197 = new ModelRenderer(this);
		bone197.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone192.addChild(bone197);
		setRotationAngle(bone197, 0.0F, -1.0472F, 0.0F);
		bone197.setTextureOffset(88, 124).addBox(-1.0F, -38.05F, 5.25F, 2.0F, 26.0F, 2.0F, 0.0F, false);

		bone198 = new ModelRenderer(this);
		bone198.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone197.addChild(bone198);
		setRotationAngle(bone198, 0.0F, -1.0472F, 0.0F);
		bone198.setTextureOffset(88, 124).addBox(-1.0F, -38.05F, 5.25F, 2.0F, 26.0F, 2.0F, 0.0F, false);

		bone199 = new ModelRenderer(this);
		bone199.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone198.addChild(bone199);
		setRotationAngle(bone199, 0.0F, -1.0472F, 0.0F);
		bone199.setTextureOffset(88, 124).addBox(-1.0F, -38.05F, 5.25F, 2.0F, 26.0F, 2.0F, 0.0F, false);

		bone130 = new ModelRenderer(this);
		bone130.setRotationPoint(0.0F, -1.5F, 0.0F);
		centre_column.addChild(bone130);
		

		bone131 = new ModelRenderer(this);
		bone131.setRotationPoint(0.0F, -36.0F, 0.0F);
		bone130.addChild(bone131);
		bone131.setTextureOffset(50, 79).addBox(-4.0F, -4.0F, 6.95F, 8.0F, 4.0F, 2.0F, 0.0F, false);

		bone42 = new ModelRenderer(this);
		bone42.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone131.addChild(bone42);
		setRotationAngle(bone42, 0.0F, -1.0472F, 0.0F);
		bone42.setTextureOffset(50, 79).addBox(-4.0F, -4.0F, 6.95F, 8.0F, 4.0F, 2.0F, 0.0F, false);

		bone44 = new ModelRenderer(this);
		bone44.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone42.addChild(bone44);
		setRotationAngle(bone44, 0.0F, -1.0472F, 0.0F);
		bone44.setTextureOffset(50, 79).addBox(-4.0F, -4.0F, 6.95F, 8.0F, 4.0F, 2.0F, 0.0F, false);

		bone45 = new ModelRenderer(this);
		bone45.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone44.addChild(bone45);
		setRotationAngle(bone45, 0.0F, -1.0472F, 0.0F);
		bone45.setTextureOffset(50, 79).addBox(-4.0F, -4.0F, 6.95F, 8.0F, 4.0F, 2.0F, 0.0F, false);

		bone46 = new ModelRenderer(this);
		bone46.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone45.addChild(bone46);
		setRotationAngle(bone46, 0.0F, -1.0472F, 0.0F);
		bone46.setTextureOffset(50, 79).addBox(-4.0F, -4.0F, 6.95F, 8.0F, 4.0F, 2.0F, 0.0F, false);

		bone47 = new ModelRenderer(this);
		bone47.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone46.addChild(bone47);
		setRotationAngle(bone47, 0.0F, -1.0472F, 0.0F);
		bone47.setTextureOffset(50, 79).addBox(-4.0F, -4.0F, 6.95F, 8.0F, 4.0F, 2.0F, 0.0F, false);

		bone137 = new ModelRenderer(this);
		bone137.setRotationPoint(0.0F, -36.0F, 0.0F);
		bone130.addChild(bone137);
		setRotationAngle(bone137, 0.0F, -0.5236F, 0.0F);
		bone137.setTextureOffset(74, 100).addBox(-1.0F, -4.05F, 7.75F, 2.0F, 4.0F, 2.0F, 0.0F, false);

		bone36 = new ModelRenderer(this);
		bone36.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone137.addChild(bone36);
		setRotationAngle(bone36, 0.0F, -1.0472F, 0.0F);
		bone36.setTextureOffset(74, 100).addBox(-1.0F, -4.05F, 7.75F, 2.0F, 4.0F, 2.0F, 0.0F, false);

		bone38 = new ModelRenderer(this);
		bone38.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone36.addChild(bone38);
		setRotationAngle(bone38, 0.0F, -1.0472F, 0.0F);
		bone38.setTextureOffset(74, 100).addBox(-1.0F, -4.05F, 7.75F, 2.0F, 4.0F, 2.0F, 0.0F, false);

		bone39 = new ModelRenderer(this);
		bone39.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone38.addChild(bone39);
		setRotationAngle(bone39, 0.0F, -1.0472F, 0.0F);
		bone39.setTextureOffset(74, 100).addBox(-1.0F, -4.05F, 7.75F, 2.0F, 4.0F, 2.0F, 0.0F, false);

		bone40 = new ModelRenderer(this);
		bone40.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone39.addChild(bone40);
		setRotationAngle(bone40, 0.0F, -1.0472F, 0.0F);
		bone40.setTextureOffset(74, 100).addBox(-1.0F, -4.05F, 7.75F, 2.0F, 4.0F, 2.0F, 0.0F, false);

		bone41 = new ModelRenderer(this);
		bone41.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone40.addChild(bone41);
		setRotationAngle(bone41, 0.0F, -1.0472F, 0.0F);
		bone41.setTextureOffset(74, 100).addBox(-1.0F, -4.05F, 7.75F, 2.0F, 4.0F, 2.0F, 0.0F, false);

		top_column = new ModelRenderer(this);
		top_column.setRotationPoint(0.0F, 0.0F, 0.0F);
		centre_column.addChild(top_column);
		

		bone145 = new ModelRenderer(this);
		bone145.setRotationPoint(0.0F, -46.5F, 0.0F);
		top_column.addChild(bone145);
		

		bone146 = new ModelRenderer(this);
		bone146.setRotationPoint(0.0F, -36.0F, 0.0F);
		bone145.addChild(bone146);
		bone146.setTextureOffset(32, 93).addBox(-4.0F, -4.05F, 6.95F, 8.0F, 4.0F, 2.0F, 0.0F, false);

		bone30 = new ModelRenderer(this);
		bone30.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone146.addChild(bone30);
		setRotationAngle(bone30, 0.0F, -1.0472F, 0.0F);
		bone30.setTextureOffset(32, 93).addBox(-4.0F, -4.05F, 6.95F, 8.0F, 4.0F, 2.0F, 0.0F, false);

		bone32 = new ModelRenderer(this);
		bone32.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone30.addChild(bone32);
		setRotationAngle(bone32, 0.0F, -1.0472F, 0.0F);
		bone32.setTextureOffset(32, 93).addBox(-4.0F, -4.05F, 6.95F, 8.0F, 4.0F, 2.0F, 0.0F, false);

		bone33 = new ModelRenderer(this);
		bone33.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone32.addChild(bone33);
		setRotationAngle(bone33, 0.0F, -1.0472F, 0.0F);
		bone33.setTextureOffset(32, 93).addBox(-4.0F, -4.05F, 6.95F, 8.0F, 4.0F, 2.0F, 0.0F, false);

		bone34 = new ModelRenderer(this);
		bone34.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone33.addChild(bone34);
		setRotationAngle(bone34, 0.0F, -1.0472F, 0.0F);
		bone34.setTextureOffset(32, 93).addBox(-4.0F, -4.05F, 6.95F, 8.0F, 4.0F, 2.0F, 0.0F, false);

		bone35 = new ModelRenderer(this);
		bone35.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone34.addChild(bone35);
		setRotationAngle(bone35, 0.0F, -1.0472F, 0.0F);
		bone35.setTextureOffset(32, 93).addBox(-4.0F, -4.05F, 6.95F, 8.0F, 4.0F, 2.0F, 0.0F, false);

		bone152 = new ModelRenderer(this);
		bone152.setRotationPoint(0.0F, -36.0F, 0.0F);
		bone145.addChild(bone152);
		setRotationAngle(bone152, 0.0F, -0.5236F, 0.0F);
		bone152.setTextureOffset(0, 110).addBox(-1.0F, -4.05F, 7.75F, 2.0F, 4.0F, 2.0F, 0.0F, false);

		bone25 = new ModelRenderer(this);
		bone25.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone152.addChild(bone25);
		setRotationAngle(bone25, 0.0F, -1.0472F, 0.0F);
		bone25.setTextureOffset(0, 110).addBox(-1.0F, -4.05F, 7.75F, 2.0F, 4.0F, 2.0F, 0.0F, false);

		bone26 = new ModelRenderer(this);
		bone26.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone25.addChild(bone26);
		setRotationAngle(bone26, 0.0F, -1.0472F, 0.0F);
		bone26.setTextureOffset(0, 110).addBox(-1.0F, -4.05F, 7.75F, 2.0F, 4.0F, 2.0F, 0.0F, false);

		bone27 = new ModelRenderer(this);
		bone27.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone26.addChild(bone27);
		setRotationAngle(bone27, 0.0F, -1.0472F, 0.0F);
		bone27.setTextureOffset(0, 110).addBox(-1.0F, -4.05F, 7.75F, 2.0F, 4.0F, 2.0F, 0.0F, false);

		bone28 = new ModelRenderer(this);
		bone28.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone27.addChild(bone28);
		setRotationAngle(bone28, 0.0F, -1.0472F, 0.0F);
		bone28.setTextureOffset(0, 110).addBox(-1.0F, -4.05F, 7.75F, 2.0F, 4.0F, 2.0F, 0.0F, false);

		bone29 = new ModelRenderer(this);
		bone29.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone28.addChild(bone29);
		setRotationAngle(bone29, 0.0F, -1.0472F, 0.0F);
		bone29.setTextureOffset(0, 110).addBox(-1.0F, -4.05F, 7.75F, 2.0F, 4.0F, 2.0F, 0.0F, false);

		bone158 = new ModelRenderer(this);
		bone158.setRotationPoint(0.0F, -49.5F, 0.0F);
		top_column.addChild(bone158);
		

		bone159 = new ModelRenderer(this);
		bone159.setRotationPoint(0.0F, -36.0F, 0.0F);
		bone158.addChild(bone159);
		bone159.setTextureOffset(68, 35).addBox(-6.0F, -11.05F, -1.6F, 12.0F, 4.0F, 14.0F, 0.0F, false);

		bone2 = new ModelRenderer(this);
		bone2.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone159.addChild(bone2);
		setRotationAngle(bone2, 0.0F, -1.0472F, 0.0F);
		bone2.setTextureOffset(68, 35).addBox(-6.0F, -11.05F, -1.6F, 12.0F, 4.0F, 14.0F, 0.0F, false);

		bone3 = new ModelRenderer(this);
		bone3.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone2.addChild(bone3);
		setRotationAngle(bone3, 0.0F, -1.0472F, 0.0F);
		bone3.setTextureOffset(68, 35).addBox(-6.0F, -11.05F, -1.6F, 12.0F, 4.0F, 14.0F, 0.0F, false);

		bone4 = new ModelRenderer(this);
		bone4.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone3.addChild(bone4);
		setRotationAngle(bone4, 0.0F, -1.0472F, 0.0F);
		bone4.setTextureOffset(68, 35).addBox(-6.0F, -11.05F, -1.6F, 12.0F, 4.0F, 14.0F, 0.0F, false);

		bone5 = new ModelRenderer(this);
		bone5.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone4.addChild(bone5);
		setRotationAngle(bone5, 0.0F, -1.0472F, 0.0F);
		bone5.setTextureOffset(68, 35).addBox(-6.0F, -11.05F, -1.6F, 12.0F, 4.0F, 14.0F, 0.0F, false);

		bone6 = new ModelRenderer(this);
		bone6.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone5.addChild(bone6);
		setRotationAngle(bone6, 0.0F, -1.0472F, 0.0F);
		bone6.setTextureOffset(68, 35).addBox(-6.0F, -11.05F, -1.6F, 12.0F, 4.0F, 14.0F, 0.0F, false);

		bone165 = new ModelRenderer(this);
		bone165.setRotationPoint(0.0F, -36.0F, 0.0F);
		bone158.addChild(bone165);
		setRotationAngle(bone165, 0.0F, -0.5236F, 0.0F);
		bone165.setTextureOffset(110, 0).addBox(-1.0F, -11.05F, 11.75F, 2.0F, 4.0F, 2.0F, 0.0F, false);

		bone8 = new ModelRenderer(this);
		bone8.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone165.addChild(bone8);
		setRotationAngle(bone8, 0.0F, -1.0472F, 0.0F);
		bone8.setTextureOffset(110, 0).addBox(-1.0F, -11.05F, 11.75F, 2.0F, 4.0F, 2.0F, 0.0F, false);

		bone9 = new ModelRenderer(this);
		bone9.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone8.addChild(bone9);
		setRotationAngle(bone9, 0.0F, -1.0472F, 0.0F);
		bone9.setTextureOffset(110, 0).addBox(-1.0F, -11.05F, 11.75F, 2.0F, 4.0F, 2.0F, 0.0F, false);

		bone10 = new ModelRenderer(this);
		bone10.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone9.addChild(bone10);
		setRotationAngle(bone10, 0.0F, -1.0472F, 0.0F);
		bone10.setTextureOffset(110, 0).addBox(-1.0F, -11.05F, 11.75F, 2.0F, 4.0F, 2.0F, 0.0F, false);

		bone11 = new ModelRenderer(this);
		bone11.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone10.addChild(bone11);
		setRotationAngle(bone11, 0.0F, -1.0472F, 0.0F);
		bone11.setTextureOffset(110, 0).addBox(-1.0F, -11.05F, 11.75F, 2.0F, 4.0F, 2.0F, 0.0F, false);

		bone12 = new ModelRenderer(this);
		bone12.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone11.addChild(bone12);
		setRotationAngle(bone12, 0.0F, -1.0472F, 0.0F);
		bone12.setTextureOffset(110, 0).addBox(-1.0F, -11.05F, 11.75F, 2.0F, 4.0F, 2.0F, 0.0F, false);

		bone171 = new ModelRenderer(this);
		bone171.setRotationPoint(0.0F, -45.5F, 0.0F);
		top_column.addChild(bone171);
		

		bone172 = new ModelRenderer(this);
		bone172.setRotationPoint(0.0F, -36.0F, 0.0F);
		bone171.addChild(bone172);
		bone172.setTextureOffset(0, 92).addBox(-5.0F, -11.05F, -1.35F, 10.0F, 6.0F, 12.0F, 0.0F, false);

		bone20 = new ModelRenderer(this);
		bone20.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone172.addChild(bone20);
		setRotationAngle(bone20, 0.0F, -1.0472F, 0.0F);
		bone20.setTextureOffset(0, 92).addBox(-5.0F, -11.05F, -1.35F, 10.0F, 6.0F, 12.0F, 0.0F, false);

		bone21 = new ModelRenderer(this);
		bone21.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone20.addChild(bone21);
		setRotationAngle(bone21, 0.0F, -1.0472F, 0.0F);
		bone21.setTextureOffset(0, 92).addBox(-5.0F, -11.05F, -1.35F, 10.0F, 6.0F, 12.0F, 0.0F, false);

		bone22 = new ModelRenderer(this);
		bone22.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone21.addChild(bone22);
		setRotationAngle(bone22, 0.0F, -1.0472F, 0.0F);
		bone22.setTextureOffset(0, 92).addBox(-5.0F, -11.05F, -1.35F, 10.0F, 6.0F, 12.0F, 0.0F, false);

		bone23 = new ModelRenderer(this);
		bone23.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone22.addChild(bone23);
		setRotationAngle(bone23, 0.0F, -1.0472F, 0.0F);
		bone23.setTextureOffset(0, 92).addBox(-5.0F, -11.05F, -1.35F, 10.0F, 6.0F, 12.0F, 0.0F, false);

		bone24 = new ModelRenderer(this);
		bone24.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone23.addChild(bone24);
		setRotationAngle(bone24, 0.0F, -1.0472F, 0.0F);
		bone24.setTextureOffset(0, 92).addBox(-5.0F, -11.05F, -1.35F, 10.0F, 6.0F, 12.0F, 0.0F, false);

		bone178 = new ModelRenderer(this);
		bone178.setRotationPoint(0.0F, -36.0F, 0.0F);
		bone171.addChild(bone178);
		setRotationAngle(bone178, 0.0F, -0.5236F, 0.0F);
		bone178.setTextureOffset(106, 35).addBox(-1.0F, -11.05F, 9.75F, 2.0F, 6.0F, 2.0F, 0.0F, false);

		bone14 = new ModelRenderer(this);
		bone14.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone178.addChild(bone14);
		setRotationAngle(bone14, 0.0F, -1.0472F, 0.0F);
		bone14.setTextureOffset(106, 35).addBox(-1.0F, -11.05F, 9.75F, 2.0F, 6.0F, 2.0F, 0.0F, false);

		bone15 = new ModelRenderer(this);
		bone15.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone14.addChild(bone15);
		setRotationAngle(bone15, 0.0F, -1.0472F, 0.0F);
		bone15.setTextureOffset(106, 35).addBox(-1.0F, -11.05F, 9.75F, 2.0F, 6.0F, 2.0F, 0.0F, false);

		bone16 = new ModelRenderer(this);
		bone16.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone15.addChild(bone16);
		setRotationAngle(bone16, 0.0F, -1.0472F, 0.0F);
		bone16.setTextureOffset(106, 35).addBox(-1.0F, -11.05F, 9.75F, 2.0F, 6.0F, 2.0F, 0.0F, false);

		bone17 = new ModelRenderer(this);
		bone17.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone16.addChild(bone17);
		setRotationAngle(bone17, 0.0F, -1.0472F, 0.0F);
		bone17.setTextureOffset(106, 35).addBox(-1.0F, -11.05F, 9.75F, 2.0F, 6.0F, 2.0F, 0.0F, false);

		bone18 = new ModelRenderer(this);
		bone18.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone17.addChild(bone18);
		setRotationAngle(bone18, 0.0F, -1.0472F, 0.0F);
		bone18.setTextureOffset(106, 35).addBox(-1.0F, -11.05F, 9.75F, 2.0F, 6.0F, 2.0F, 0.0F, false);

		bone206 = new ModelRenderer(this);
		bone206.setRotationPoint(0.0F, -74.5F, 0.0F);
		centre_column.addChild(bone206);
		setRotationAngle(bone206, 0.0F, -0.5236F, 0.0F);
		bone206.setTextureOffset(0, 0).addBox(-2.0F, -12.05F, 4.25F, 4.0F, 12.0F, 4.0F, 0.0F, false);

		bone207 = new ModelRenderer(this);
		bone207.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone206.addChild(bone207);
		setRotationAngle(bone207, 0.0F, -1.0472F, 0.0F);
		bone207.setTextureOffset(0, 0).addBox(-2.0F, -12.05F, 4.25F, 4.0F, 12.0F, 4.0F, 0.0F, false);

		bone208 = new ModelRenderer(this);
		bone208.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone207.addChild(bone208);
		setRotationAngle(bone208, 0.0F, -1.0472F, 0.0F);
		bone208.setTextureOffset(0, 0).addBox(-2.0F, -12.05F, 4.25F, 4.0F, 12.0F, 4.0F, 0.0F, false);

		bone209 = new ModelRenderer(this);
		bone209.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone208.addChild(bone209);
		setRotationAngle(bone209, 0.0F, -1.0472F, 0.0F);
		bone209.setTextureOffset(0, 0).addBox(-2.0F, -12.05F, 4.25F, 4.0F, 12.0F, 4.0F, 0.0F, false);

		bone210 = new ModelRenderer(this);
		bone210.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone209.addChild(bone210);
		setRotationAngle(bone210, 0.0F, -1.0472F, 0.0F);
		bone210.setTextureOffset(0, 0).addBox(-2.0F, -12.05F, 4.25F, 4.0F, 12.0F, 4.0F, 0.0F, false);

		bone211 = new ModelRenderer(this);
		bone211.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone210.addChild(bone211);
		setRotationAngle(bone211, 0.0F, -1.0472F, 0.0F);
		bone211.setTextureOffset(0, 0).addBox(-2.0F, -12.05F, 4.25F, 4.0F, 12.0F, 4.0F, 0.0F, false);

		bone200 = new ModelRenderer(this);
		bone200.setRotationPoint(0.0F, -37.5F, 0.0F);
		centre_column.addChild(bone200);
		setRotationAngle(bone200, 0.0F, -0.5236F, 0.0F);
		bone200.setTextureOffset(0, 40).addBox(-2.0F, -12.05F, 4.25F, 4.0F, 12.0F, 4.0F, 0.0F, false);

		bone201 = new ModelRenderer(this);
		bone201.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone200.addChild(bone201);
		setRotationAngle(bone201, 0.0F, -1.0472F, 0.0F);
		bone201.setTextureOffset(0, 40).addBox(-2.0F, -12.05F, 4.25F, 4.0F, 12.0F, 4.0F, 0.0F, false);

		bone202 = new ModelRenderer(this);
		bone202.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone201.addChild(bone202);
		setRotationAngle(bone202, 0.0F, -1.0472F, 0.0F);
		bone202.setTextureOffset(0, 40).addBox(-2.0F, -12.05F, 4.25F, 4.0F, 12.0F, 4.0F, 0.0F, false);

		bone203 = new ModelRenderer(this);
		bone203.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone202.addChild(bone203);
		setRotationAngle(bone203, 0.0F, -1.0472F, 0.0F);
		bone203.setTextureOffset(0, 40).addBox(-2.0F, -12.05F, 4.25F, 4.0F, 12.0F, 4.0F, 0.0F, false);

		bone204 = new ModelRenderer(this);
		bone204.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone203.addChild(bone204);
		setRotationAngle(bone204, 0.0F, -1.0472F, 0.0F);
		bone204.setTextureOffset(0, 40).addBox(-2.0F, -12.05F, 4.25F, 4.0F, 12.0F, 4.0F, 0.0F, false);

		bone205 = new ModelRenderer(this);
		bone205.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone204.addChild(bone205);
		setRotationAngle(bone205, 0.0F, -1.0472F, 0.0F);
		bone205.setTextureOffset(0, 40).addBox(-2.0F, -12.05F, 4.25F, 4.0F, 12.0F, 4.0F, 0.0F, false);

		base = new ModelRenderer(this);
		base.setRotationPoint(0.0F, 0.0F, 0.0F);
		base_console.addChild(base);
		

		base_glow = new LightModelRenderer(this);
		base_glow.setRotationPoint(0.0F, -6.0F, 0.0F);
		base.addChild(base_glow);
		base_glow.setTextureOffset(114, 116).addBox(-6.0F, -14.0F, 9.9F, 12.0F, 14.0F, 2.0F, 0.0F, false);

		bone153 = new ModelRenderer(this);
		bone153.setRotationPoint(0.0F, 0.0F, 0.0F);
		base_glow.addChild(bone153);
		setRotationAngle(bone153, 0.0F, -1.0472F, 0.0F);
		bone153.setTextureOffset(114, 116).addBox(-6.0F, -14.0F, 9.9F, 12.0F, 14.0F, 2.0F, 0.0F, false);

		bone154 = new ModelRenderer(this);
		bone154.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone153.addChild(bone154);
		setRotationAngle(bone154, 0.0F, -1.0472F, 0.0F);
		bone154.setTextureOffset(114, 116).addBox(-6.0F, -14.0F, 9.9F, 12.0F, 14.0F, 2.0F, 0.0F, false);

		bone155 = new ModelRenderer(this);
		bone155.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone154.addChild(bone155);
		setRotationAngle(bone155, 0.0F, -1.0472F, 0.0F);
		bone155.setTextureOffset(114, 116).addBox(-6.0F, -14.0F, 9.9F, 12.0F, 14.0F, 2.0F, 0.0F, false);

		bone156 = new ModelRenderer(this);
		bone156.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone155.addChild(bone156);
		setRotationAngle(bone156, 0.0F, -1.0472F, 0.0F);
		bone156.setTextureOffset(114, 116).addBox(-6.0F, -14.0F, 9.9F, 12.0F, 14.0F, 2.0F, 0.0F, false);

		bone157 = new ModelRenderer(this);
		bone157.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone156.addChild(bone157);
		setRotationAngle(bone157, 0.0F, -1.0472F, 0.0F);
		bone157.setTextureOffset(114, 116).addBox(-6.0F, -14.0F, 9.9F, 12.0F, 14.0F, 2.0F, 0.0F, false);

		bone13 = new ModelRenderer(this);
		bone13.setRotationPoint(0.0F, -6.0F, 0.0F);
		base.addChild(bone13);
		setRotationAngle(bone13, 0.0F, -0.5236F, 0.0F);
		bone13.setTextureOffset(26, 68).addBox(-2.0F, -14.0F, 12.6F, 4.0F, 14.0F, 2.0F, 0.0F, false);

		bone140 = new ModelRenderer(this);
		bone140.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone13.addChild(bone140);
		setRotationAngle(bone140, 0.0F, -1.0472F, 0.0F);
		bone140.setTextureOffset(26, 68).addBox(-2.0F, -14.0F, 12.6F, 4.0F, 14.0F, 2.0F, 0.0F, false);

		bone141 = new ModelRenderer(this);
		bone141.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone140.addChild(bone141);
		setRotationAngle(bone141, 0.0F, -1.0472F, 0.0F);
		bone141.setTextureOffset(26, 68).addBox(-2.0F, -14.0F, 12.6F, 4.0F, 14.0F, 2.0F, 0.0F, false);

		bone142 = new ModelRenderer(this);
		bone142.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone141.addChild(bone142);
		setRotationAngle(bone142, 0.0F, -1.0472F, 0.0F);
		bone142.setTextureOffset(26, 68).addBox(-2.0F, -14.0F, 12.6F, 4.0F, 14.0F, 2.0F, 0.0F, false);

		bone143 = new ModelRenderer(this);
		bone143.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone142.addChild(bone143);
		setRotationAngle(bone143, 0.0F, -1.0472F, 0.0F);
		bone143.setTextureOffset(26, 68).addBox(-2.0F, -14.0F, 12.6F, 4.0F, 14.0F, 2.0F, 0.0F, false);

		bone144 = new ModelRenderer(this);
		bone144.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone143.addChild(bone144);
		setRotationAngle(bone144, 0.0F, -1.0472F, 0.0F);
		bone144.setTextureOffset(26, 68).addBox(-2.0F, -14.0F, 12.6F, 4.0F, 14.0F, 2.0F, 0.0F, false);

		bone7 = new ModelRenderer(this);
		bone7.setRotationPoint(0.0F, -2.0F, 14.0F);
		base.addChild(bone7);
		bone7.setTextureOffset(112, 24).addBox(-8.0F, -4.0F, -2.1F, 16.0F, 4.0F, 2.0F, 0.0F, false);

		bone134 = new ModelRenderer(this);
		bone134.setRotationPoint(0.0F, 0.0F, -14.0F);
		bone7.addChild(bone134);
		setRotationAngle(bone134, 0.0F, -1.0472F, 0.0F);
		bone134.setTextureOffset(112, 24).addBox(-8.0F, -4.0F, 11.9F, 16.0F, 4.0F, 2.0F, 0.0F, false);

		bone135 = new ModelRenderer(this);
		bone135.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone134.addChild(bone135);
		setRotationAngle(bone135, 0.0F, -1.0472F, 0.0F);
		bone135.setTextureOffset(112, 24).addBox(-8.0F, -4.0F, 11.9F, 16.0F, 4.0F, 2.0F, 0.0F, false);

		bone136 = new ModelRenderer(this);
		bone136.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone135.addChild(bone136);
		setRotationAngle(bone136, 0.0F, -1.0472F, 0.0F);
		bone136.setTextureOffset(112, 24).addBox(-8.0F, -4.0F, 11.9F, 16.0F, 4.0F, 2.0F, 0.0F, false);

		bone138 = new ModelRenderer(this);
		bone138.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone136.addChild(bone138);
		setRotationAngle(bone138, 0.0F, -1.0472F, 0.0F);
		bone138.setTextureOffset(112, 24).addBox(-8.0F, -4.0F, 11.9F, 16.0F, 4.0F, 2.0F, 0.0F, false);

		bone139 = new ModelRenderer(this);
		bone139.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone138.addChild(bone139);
		setRotationAngle(bone139, 0.0F, -1.0472F, 0.0F);
		bone139.setTextureOffset(112, 24).addBox(-8.0F, -4.0F, 11.9F, 16.0F, 4.0F, 2.0F, 0.0F, false);

		bone = new ModelRenderer(this);
		bone.setRotationPoint(0.0F, 0.0F, 14.0F);
		base.addChild(bone);
		bone.setTextureOffset(0, 18).addBox(-10.0F, -2.0F, -16.65F, 20.0F, 2.0F, 20.0F, 0.0F, false);

		bone147 = new ModelRenderer(this);
		bone147.setRotationPoint(0.0F, 0.0F, -14.0F);
		bone.addChild(bone147);
		setRotationAngle(bone147, 0.0F, -1.0472F, 0.0F);
		bone147.setTextureOffset(0, 18).addBox(-10.0F, -2.0F, -2.65F, 20.0F, 2.0F, 20.0F, 0.0F, false);

		bone148 = new ModelRenderer(this);
		bone148.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone147.addChild(bone148);
		setRotationAngle(bone148, 0.0F, -1.0472F, 0.0F);
		bone148.setTextureOffset(0, 18).addBox(-10.0F, -2.0F, -2.65F, 20.0F, 2.0F, 20.0F, 0.0F, false);

		bone149 = new ModelRenderer(this);
		bone149.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone148.addChild(bone149);
		setRotationAngle(bone149, 0.0F, -1.0472F, 0.0F);
		bone149.setTextureOffset(0, 18).addBox(-10.0F, -2.0F, -2.65F, 20.0F, 2.0F, 20.0F, 0.0F, false);

		bone150 = new ModelRenderer(this);
		bone150.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone149.addChild(bone150);
		setRotationAngle(bone150, 0.0F, -1.0472F, 0.0F);
		bone150.setTextureOffset(0, 18).addBox(-10.0F, -2.0F, -2.65F, 20.0F, 2.0F, 20.0F, 0.0F, false);

		bone151 = new ModelRenderer(this);
		bone151.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone150.addChild(bone151);
		setRotationAngle(bone151, 0.0F, -1.0472F, 0.0F);
		bone151.setTextureOffset(0, 18).addBox(-10.0F, -2.0F, -2.65F, 20.0F, 2.0F, 20.0F, 0.0F, false);

		bone129 = new ModelRenderer(this);
		bone129.setRotationPoint(0.0F, 0.5F, 0.0F);
		base_console.addChild(bone129);
		

		bone123 = new ModelRenderer(this);
		bone123.setRotationPoint(0.0F, -36.0F, 0.0F);
		bone129.addChild(bone123);
		bone123.setTextureOffset(72, 0).addBox(-6.0F, -3.0F, -1.6F, 12.0F, 2.0F, 14.0F, 0.0F, false);

		bone54 = new ModelRenderer(this);
		bone54.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone123.addChild(bone54);
		setRotationAngle(bone54, 0.0F, -1.0472F, 0.0F);
		bone54.setTextureOffset(72, 0).addBox(-6.0F, -3.0F, -1.6F, 12.0F, 2.0F, 14.0F, 0.0F, false);

		bone57 = new ModelRenderer(this);
		bone57.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone54.addChild(bone57);
		setRotationAngle(bone57, 0.0F, -1.0472F, 0.0F);
		bone57.setTextureOffset(72, 0).addBox(-6.0F, -3.0F, -1.6F, 12.0F, 2.0F, 14.0F, 0.0F, false);

		bone58 = new ModelRenderer(this);
		bone58.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone57.addChild(bone58);
		setRotationAngle(bone58, 0.0F, -1.0472F, 0.0F);
		bone58.setTextureOffset(72, 0).addBox(-6.0F, -3.0F, -1.6F, 12.0F, 2.0F, 14.0F, 0.0F, false);

		bone59 = new ModelRenderer(this);
		bone59.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone58.addChild(bone59);
		setRotationAngle(bone59, 0.0F, -1.0472F, 0.0F);
		bone59.setTextureOffset(72, 0).addBox(-6.0F, -3.0F, -1.6F, 12.0F, 2.0F, 14.0F, 0.0F, false);

		bone60 = new ModelRenderer(this);
		bone60.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone59.addChild(bone60);
		setRotationAngle(bone60, 0.0F, -1.0472F, 0.0F);
		bone60.setTextureOffset(72, 0).addBox(-6.0F, -3.0F, -1.6F, 12.0F, 2.0F, 14.0F, 0.0F, false);

		bone117 = new ModelRenderer(this);
		bone117.setRotationPoint(0.0F, -36.0F, 0.0F);
		bone129.addChild(bone117);
		setRotationAngle(bone117, 0.0F, -0.5236F, 0.0F);
		bone117.setTextureOffset(0, 130).addBox(-1.0F, -2.95F, 11.75F, 2.0F, 2.0F, 2.0F, 0.0F, false);

		bone48 = new ModelRenderer(this);
		bone48.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone117.addChild(bone48);
		setRotationAngle(bone48, 0.0F, -1.0472F, 0.0F);
		bone48.setTextureOffset(0, 130).addBox(-1.0F, -2.95F, 11.75F, 2.0F, 2.0F, 2.0F, 0.0F, false);

		bone50 = new ModelRenderer(this);
		bone50.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone48.addChild(bone50);
		setRotationAngle(bone50, 0.0F, -1.0472F, 0.0F);
		bone50.setTextureOffset(0, 130).addBox(-1.0F, -2.95F, 11.75F, 2.0F, 2.0F, 2.0F, 0.0F, false);

		bone51 = new ModelRenderer(this);
		bone51.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone50.addChild(bone51);
		setRotationAngle(bone51, 0.0F, -1.0472F, 0.0F);
		bone51.setTextureOffset(0, 130).addBox(-1.0F, -2.95F, 11.75F, 2.0F, 2.0F, 2.0F, 0.0F, false);

		bone52 = new ModelRenderer(this);
		bone52.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone51.addChild(bone52);
		setRotationAngle(bone52, 0.0F, -1.0472F, 0.0F);
		bone52.setTextureOffset(0, 130).addBox(-1.0F, -2.95F, 11.75F, 2.0F, 2.0F, 2.0F, 0.0F, false);

		bone53 = new ModelRenderer(this);
		bone53.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone52.addChild(bone53);
		setRotationAngle(bone53, 0.0F, -1.0472F, 0.0F);
		bone53.setTextureOffset(0, 130).addBox(-1.0F, -2.95F, 11.75F, 2.0F, 2.0F, 2.0F, 0.0F, false);

		bone85 = new ModelRenderer(this);
		bone85.setRotationPoint(0.0F, -27.0F, 0.0F);
		base_console.addChild(bone85);
		

		bone86 = new ModelRenderer(this);
		bone86.setRotationPoint(0.0F, -1.0F, 29.75F);
		bone85.addChild(bone86);
		setRotationAngle(bone86, -0.4974F, 0.0F, 0.0F);
		bone86.setTextureOffset(0, 40).addBox(-13.0F, 1.25F, -18.0F, 26.0F, 2.0F, 16.0F, 0.0F, false);

		bone109 = new ModelRenderer(this);
		bone109.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone85.addChild(bone109);
		setRotationAngle(bone109, 0.0F, -1.0472F, 0.0F);
		

		bone110 = new ModelRenderer(this);
		bone110.setRotationPoint(0.0F, -1.0F, 29.75F);
		bone109.addChild(bone110);
		setRotationAngle(bone110, -0.4974F, 0.0F, 0.0F);
		bone110.setTextureOffset(0, 40).addBox(-13.0F, 1.25F, -18.0F, 26.0F, 2.0F, 16.0F, 0.0F, false);

		bone111 = new ModelRenderer(this);
		bone111.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone109.addChild(bone111);
		setRotationAngle(bone111, 0.0F, -1.0472F, 0.0F);
		

		bone112 = new ModelRenderer(this);
		bone112.setRotationPoint(0.0F, -1.0F, 29.75F);
		bone111.addChild(bone112);
		setRotationAngle(bone112, -0.4974F, 0.0F, 0.0F);
		bone112.setTextureOffset(0, 40).addBox(-13.0F, 1.25F, -18.0F, 26.0F, 2.0F, 16.0F, 0.0F, false);

		bone113 = new ModelRenderer(this);
		bone113.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone111.addChild(bone113);
		setRotationAngle(bone113, 0.0F, -1.0472F, 0.0F);
		

		bone114 = new ModelRenderer(this);
		bone114.setRotationPoint(0.0F, -1.0F, 29.75F);
		bone113.addChild(bone114);
		setRotationAngle(bone114, -0.4974F, 0.0F, 0.0F);
		bone114.setTextureOffset(0, 40).addBox(-13.0F, 1.25F, -18.0F, 26.0F, 2.0F, 16.0F, 0.0F, false);

		bone115 = new ModelRenderer(this);
		bone115.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone113.addChild(bone115);
		setRotationAngle(bone115, 0.0F, -1.0472F, 0.0F);
		

		bone116 = new ModelRenderer(this);
		bone116.setRotationPoint(0.0F, -1.0F, 29.75F);
		bone115.addChild(bone116);
		setRotationAngle(bone116, -0.4974F, 0.0F, 0.0F);
		bone116.setTextureOffset(0, 40).addBox(-13.0F, 1.25F, -18.0F, 26.0F, 2.0F, 16.0F, 0.0F, false);

		bone118 = new ModelRenderer(this);
		bone118.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone115.addChild(bone118);
		setRotationAngle(bone118, 0.0F, -1.0472F, 0.0F);
		

		bone119 = new ModelRenderer(this);
		bone119.setRotationPoint(0.0F, -1.0F, 29.75F);
		bone118.addChild(bone119);
		setRotationAngle(bone119, -0.4974F, 0.0F, 0.0F);
		bone119.setTextureOffset(0, 40).addBox(-13.0F, 1.25F, -18.0F, 26.0F, 2.0F, 16.0F, 0.0F, false);

		bone81 = new ModelRenderer(this);
		bone81.setRotationPoint(0.0F, -27.0F, 0.0F);
		base_console.addChild(bone81);
		

		bone82 = new ModelRenderer(this);
		bone82.setRotationPoint(0.0F, -1.0F, 29.75F);
		bone81.addChild(bone82);
		setRotationAngle(bone82, -0.4974F, 0.0F, 0.0F);
		bone82.setTextureOffset(60, 31).addBox(-16.0F, 0.275F, -2.0F, 32.0F, 2.0F, 2.0F, 0.0F, false);
		bone82.setTextureOffset(84, 53).addBox(-7.0F, 0.275F, -19.5F, 14.0F, 2.0F, 2.0F, 0.0F, false);

		bone83 = new ModelRenderer(this);
		bone83.setRotationPoint(-7.0F, 0.2F, -17.5F);
		bone82.addChild(bone83);
		setRotationAngle(bone83, 0.0F, -0.48F, 0.0F);
		bone83.setTextureOffset(50, 73).addBox(0.0F, 0.075F, -2.0F, 2.0F, 2.0F, 20.0F, 0.0F, false);

		bone84 = new ModelRenderer(this);
		bone84.setRotationPoint(7.0F, 0.2F, -17.5F);
		bone82.addChild(bone84);
		setRotationAngle(bone84, 0.0F, 0.48F, 0.0F);
		bone84.setTextureOffset(50, 73).addBox(-2.0F, 0.075F, -2.0F, 2.0F, 2.0F, 20.0F, 0.0F, false);

		bone61 = new ModelRenderer(this);
		bone61.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone81.addChild(bone61);
		setRotationAngle(bone61, 0.0F, -1.0472F, 0.0F);
		

		bone62 = new ModelRenderer(this);
		bone62.setRotationPoint(0.0F, -1.0F, 29.75F);
		bone61.addChild(bone62);
		setRotationAngle(bone62, -0.4974F, 0.0F, 0.0F);
		bone62.setTextureOffset(60, 31).addBox(-16.0F, 0.275F, -2.0F, 32.0F, 2.0F, 2.0F, 0.0F, false);
		bone62.setTextureOffset(84, 53).addBox(-7.0F, 0.275F, -19.5F, 14.0F, 2.0F, 2.0F, 0.0F, false);

		bone63 = new ModelRenderer(this);
		bone63.setRotationPoint(-7.0F, 0.2F, -17.5F);
		bone62.addChild(bone63);
		setRotationAngle(bone63, 0.0F, -0.48F, 0.0F);
		bone63.setTextureOffset(50, 73).addBox(0.0F, 0.075F, -2.0F, 2.0F, 2.0F, 20.0F, 0.0F, false);

		bone64 = new ModelRenderer(this);
		bone64.setRotationPoint(7.0F, 0.2F, -17.5F);
		bone62.addChild(bone64);
		setRotationAngle(bone64, 0.0F, 0.48F, 0.0F);
		bone64.setTextureOffset(50, 73).addBox(-2.0F, 0.075F, -2.0F, 2.0F, 2.0F, 20.0F, 0.0F, false);

		bone65 = new ModelRenderer(this);
		bone65.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone61.addChild(bone65);
		setRotationAngle(bone65, 0.0F, -1.0472F, 0.0F);
		

		bone66 = new ModelRenderer(this);
		bone66.setRotationPoint(0.0F, -1.0F, 29.75F);
		bone65.addChild(bone66);
		setRotationAngle(bone66, -0.4974F, 0.0F, 0.0F);
		bone66.setTextureOffset(60, 31).addBox(-16.0F, 0.275F, -2.0F, 32.0F, 2.0F, 2.0F, 0.0F, false);
		bone66.setTextureOffset(84, 53).addBox(-7.0F, 0.275F, -19.5F, 14.0F, 2.0F, 2.0F, 0.0F, false);

		bone69 = new ModelRenderer(this);
		bone69.setRotationPoint(-7.0F, 0.2F, -17.5F);
		bone66.addChild(bone69);
		setRotationAngle(bone69, 0.0F, -0.48F, 0.0F);
		bone69.setTextureOffset(50, 73).addBox(0.0F, 0.075F, -2.0F, 2.0F, 2.0F, 20.0F, 0.0F, false);

		bone70 = new ModelRenderer(this);
		bone70.setRotationPoint(7.0F, 0.2F, -17.5F);
		bone66.addChild(bone70);
		setRotationAngle(bone70, 0.0F, 0.48F, 0.0F);
		bone70.setTextureOffset(50, 73).addBox(-2.0F, 0.075F, -2.0F, 2.0F, 2.0F, 20.0F, 0.0F, false);

		bone71 = new ModelRenderer(this);
		bone71.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone65.addChild(bone71);
		setRotationAngle(bone71, 0.0F, -1.0472F, 0.0F);
		

		bone72 = new ModelRenderer(this);
		bone72.setRotationPoint(0.0F, -1.0F, 29.75F);
		bone71.addChild(bone72);
		setRotationAngle(bone72, -0.4974F, 0.0F, 0.0F);
		bone72.setTextureOffset(60, 31).addBox(-16.0F, 0.275F, -2.0F, 32.0F, 2.0F, 2.0F, 0.0F, false);
		bone72.setTextureOffset(84, 53).addBox(-7.0F, 0.275F, -19.5F, 14.0F, 2.0F, 2.0F, 0.0F, false);

		bone73 = new ModelRenderer(this);
		bone73.setRotationPoint(-7.0F, 0.2F, -17.5F);
		bone72.addChild(bone73);
		setRotationAngle(bone73, 0.0F, -0.48F, 0.0F);
		bone73.setTextureOffset(50, 73).addBox(0.0F, 0.075F, -2.0F, 2.0F, 2.0F, 20.0F, 0.0F, false);

		bone74 = new ModelRenderer(this);
		bone74.setRotationPoint(7.0F, 0.2F, -17.5F);
		bone72.addChild(bone74);
		setRotationAngle(bone74, 0.0F, 0.48F, 0.0F);
		bone74.setTextureOffset(50, 73).addBox(-2.0F, 0.075F, -2.0F, 2.0F, 2.0F, 20.0F, 0.0F, false);

		bone75 = new ModelRenderer(this);
		bone75.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone71.addChild(bone75);
		setRotationAngle(bone75, 0.0F, -1.0472F, 0.0F);
		

		bone76 = new ModelRenderer(this);
		bone76.setRotationPoint(0.0F, -1.0F, 29.75F);
		bone75.addChild(bone76);
		setRotationAngle(bone76, -0.4974F, 0.0F, 0.0F);
		bone76.setTextureOffset(60, 31).addBox(-16.0F, 0.275F, -2.0F, 32.0F, 2.0F, 2.0F, 0.0F, false);
		bone76.setTextureOffset(84, 53).addBox(-7.0F, 0.275F, -19.5F, 14.0F, 2.0F, 2.0F, 0.0F, false);

		bone77 = new ModelRenderer(this);
		bone77.setRotationPoint(-7.0F, 0.2F, -17.5F);
		bone76.addChild(bone77);
		setRotationAngle(bone77, 0.0F, -0.48F, 0.0F);
		bone77.setTextureOffset(50, 73).addBox(0.0F, 0.075F, -2.0F, 2.0F, 2.0F, 20.0F, 0.0F, false);

		bone78 = new ModelRenderer(this);
		bone78.setRotationPoint(7.0F, 0.2F, -17.5F);
		bone76.addChild(bone78);
		setRotationAngle(bone78, 0.0F, 0.48F, 0.0F);
		bone78.setTextureOffset(50, 73).addBox(-2.0F, 0.075F, -2.0F, 2.0F, 2.0F, 20.0F, 0.0F, false);

		bone79 = new ModelRenderer(this);
		bone79.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone75.addChild(bone79);
		setRotationAngle(bone79, 0.0F, -1.0472F, 0.0F);
		

		bone80 = new ModelRenderer(this);
		bone80.setRotationPoint(0.0F, -1.0F, 29.75F);
		bone79.addChild(bone80);
		setRotationAngle(bone80, -0.4974F, 0.0F, 0.0F);
		bone80.setTextureOffset(60, 31).addBox(-16.0F, 0.275F, -2.0F, 32.0F, 2.0F, 2.0F, 0.0F, false);
		bone80.setTextureOffset(84, 53).addBox(-7.0F, 0.275F, -19.5F, 14.0F, 2.0F, 2.0F, 0.0F, false);

		bone87 = new ModelRenderer(this);
		bone87.setRotationPoint(-7.0F, 0.2F, -17.5F);
		bone80.addChild(bone87);
		setRotationAngle(bone87, 0.0F, -0.48F, 0.0F);
		bone87.setTextureOffset(50, 73).addBox(0.0F, 0.075F, -2.0F, 2.0F, 2.0F, 20.0F, 0.0F, false);

		bone88 = new ModelRenderer(this);
		bone88.setRotationPoint(7.0F, 0.2F, -17.5F);
		bone80.addChild(bone88);
		setRotationAngle(bone88, 0.0F, 0.48F, 0.0F);
		bone88.setTextureOffset(50, 73).addBox(-2.0F, 0.075F, -2.0F, 2.0F, 2.0F, 20.0F, 0.0F, false);

		bone67 = new ModelRenderer(this);
		bone67.setRotationPoint(0.0F, -28.0F, 0.0F);
		base_console.addChild(bone67);
		setRotationAngle(bone67, 0.0F, -0.5236F, 0.0F);
		

		bone68 = new ModelRenderer(this);
		bone68.setRotationPoint(0.0F, 0.0F, 33.75F);
		bone67.addChild(bone68);
		setRotationAngle(bone68, -0.4363F, 0.0F, 0.0F);
		bone68.setTextureOffset(0, 68).addBox(-1.0F, 0.0F, -22.0F, 2.0F, 2.0F, 22.0F, 0.0F, false);

		bone89 = new ModelRenderer(this);
		bone89.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone67.addChild(bone89);
		setRotationAngle(bone89, 0.0F, -1.0472F, 0.0F);
		

		bone90 = new ModelRenderer(this);
		bone90.setRotationPoint(0.0F, 0.0F, 33.75F);
		bone89.addChild(bone90);
		setRotationAngle(bone90, -0.4363F, 0.0F, 0.0F);
		bone90.setTextureOffset(0, 68).addBox(-1.0F, 0.0F, -22.0F, 2.0F, 2.0F, 22.0F, 0.0F, false);

		bone91 = new ModelRenderer(this);
		bone91.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone89.addChild(bone91);
		setRotationAngle(bone91, 0.0F, -1.0472F, 0.0F);
		

		bone92 = new ModelRenderer(this);
		bone92.setRotationPoint(0.0F, 0.0F, 33.75F);
		bone91.addChild(bone92);
		setRotationAngle(bone92, -0.4363F, 0.0F, 0.0F);
		bone92.setTextureOffset(0, 68).addBox(-1.0F, 0.0F, -22.0F, 2.0F, 2.0F, 22.0F, 0.0F, false);

		bone93 = new ModelRenderer(this);
		bone93.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone91.addChild(bone93);
		setRotationAngle(bone93, 0.0F, -1.0472F, 0.0F);
		

		bone94 = new ModelRenderer(this);
		bone94.setRotationPoint(0.0F, 0.0F, 33.75F);
		bone93.addChild(bone94);
		setRotationAngle(bone94, -0.4363F, 0.0F, 0.0F);
		bone94.setTextureOffset(0, 68).addBox(-1.0F, 0.0F, -22.0F, 2.0F, 2.0F, 22.0F, 0.0F, false);

		bone95 = new ModelRenderer(this);
		bone95.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone93.addChild(bone95);
		setRotationAngle(bone95, 0.0F, -1.0472F, 0.0F);
		

		bone96 = new ModelRenderer(this);
		bone96.setRotationPoint(0.0F, 0.0F, 33.75F);
		bone95.addChild(bone96);
		setRotationAngle(bone96, -0.4363F, 0.0F, 0.0F);
		bone96.setTextureOffset(0, 68).addBox(-1.0F, 0.0F, -22.0F, 2.0F, 2.0F, 22.0F, 0.0F, false);

		bone97 = new ModelRenderer(this);
		bone97.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone95.addChild(bone97);
		setRotationAngle(bone97, 0.0F, -1.0472F, 0.0F);
		

		bone98 = new ModelRenderer(this);
		bone98.setRotationPoint(0.0F, 0.0F, 33.75F);
		bone97.addChild(bone98);
		setRotationAngle(bone98, -0.4363F, 0.0F, 0.0F);
		bone98.setTextureOffset(0, 68).addBox(-1.0F, 0.0F, -22.0F, 2.0F, 2.0F, 22.0F, 0.0F, false);

		bone55 = new ModelRenderer(this);
		bone55.setRotationPoint(0.0F, -22.0F, 0.0F);
		base_console.addChild(bone55);
		

		bone56 = new ModelRenderer(this);
		bone56.setRotationPoint(0.0F, 0.0F, 25.75F);
		bone55.addChild(bone56);
		setRotationAngle(bone56, 0.3054F, 0.0F, 0.0F);
		bone56.setTextureOffset(0, 0).addBox(-14.0F, -2.0F, -16.0F, 28.0F, 2.0F, 16.0F, 0.0F, false);

		bone160 = new ModelRenderer(this);
		bone160.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone55.addChild(bone160);
		setRotationAngle(bone160, 0.0F, -1.0472F, 0.0F);
		

		bone161 = new ModelRenderer(this);
		bone161.setRotationPoint(0.0F, 0.0F, 25.75F);
		bone160.addChild(bone161);
		setRotationAngle(bone161, 0.3054F, 0.0F, 0.0F);
		bone161.setTextureOffset(0, 0).addBox(-14.0F, -2.0F, -16.0F, 28.0F, 2.0F, 16.0F, 0.0F, false);

		bone162 = new ModelRenderer(this);
		bone162.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone160.addChild(bone162);
		setRotationAngle(bone162, 0.0F, -1.0472F, 0.0F);
		

		bone163 = new ModelRenderer(this);
		bone163.setRotationPoint(0.0F, 0.0F, 25.75F);
		bone162.addChild(bone163);
		setRotationAngle(bone163, 0.3054F, 0.0F, 0.0F);
		bone163.setTextureOffset(0, 0).addBox(-14.0F, -2.0F, -16.0F, 28.0F, 2.0F, 16.0F, 0.0F, false);

		bone164 = new ModelRenderer(this);
		bone164.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone162.addChild(bone164);
		setRotationAngle(bone164, 0.0F, -1.0472F, 0.0F);
		

		bone166 = new ModelRenderer(this);
		bone166.setRotationPoint(0.0F, 0.0F, 25.75F);
		bone164.addChild(bone166);
		setRotationAngle(bone166, 0.3054F, 0.0F, 0.0F);
		bone166.setTextureOffset(0, 0).addBox(-14.0F, -2.0F, -16.0F, 28.0F, 2.0F, 16.0F, 0.0F, false);

		bone167 = new ModelRenderer(this);
		bone167.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone164.addChild(bone167);
		setRotationAngle(bone167, 0.0F, -1.0472F, 0.0F);
		

		bone168 = new ModelRenderer(this);
		bone168.setRotationPoint(0.0F, 0.0F, 25.75F);
		bone167.addChild(bone168);
		setRotationAngle(bone168, 0.3054F, 0.0F, 0.0F);
		bone168.setTextureOffset(0, 0).addBox(-14.0F, -2.0F, -16.0F, 28.0F, 2.0F, 16.0F, 0.0F, false);

		bone169 = new ModelRenderer(this);
		bone169.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone167.addChild(bone169);
		setRotationAngle(bone169, 0.0F, -1.0472F, 0.0F);
		

		bone170 = new ModelRenderer(this);
		bone170.setRotationPoint(0.0F, 0.0F, 25.75F);
		bone169.addChild(bone170);
		setRotationAngle(bone170, 0.3054F, 0.0F, 0.0F);
		bone170.setTextureOffset(0, 0).addBox(-14.0F, -2.0F, -16.0F, 28.0F, 2.0F, 16.0F, 0.0F, false);

		bone43 = new ModelRenderer(this);
		bone43.setRotationPoint(0.0F, -22.0F, 0.0F);
		base_console.addChild(bone43);
		setRotationAngle(bone43, 0.0F, -0.5236F, 0.0F);
		

		bone49 = new ModelRenderer(this);
		bone49.setRotationPoint(0.0F, 0.0F, 33.75F);
		bone43.addChild(bone49);
		setRotationAngle(bone49, 0.2618F, 0.0F, 0.0F);
		bone49.setTextureOffset(26, 68).addBox(-1.0F, -2.0F, -20.0F, 2.0F, 2.0F, 20.0F, 0.0F, false);

		bone120 = new ModelRenderer(this);
		bone120.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone43.addChild(bone120);
		setRotationAngle(bone120, 0.0F, -1.0472F, 0.0F);
		

		bone121 = new ModelRenderer(this);
		bone121.setRotationPoint(0.0F, 0.0F, 33.75F);
		bone120.addChild(bone121);
		setRotationAngle(bone121, 0.2618F, 0.0F, 0.0F);
		bone121.setTextureOffset(26, 68).addBox(-1.0F, -2.0F, -20.0F, 2.0F, 2.0F, 20.0F, 0.0F, false);

		bone122 = new ModelRenderer(this);
		bone122.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone120.addChild(bone122);
		setRotationAngle(bone122, 0.0F, -1.0472F, 0.0F);
		

		bone124 = new ModelRenderer(this);
		bone124.setRotationPoint(0.0F, 0.0F, 33.75F);
		bone122.addChild(bone124);
		setRotationAngle(bone124, 0.2618F, 0.0F, 0.0F);
		bone124.setTextureOffset(26, 68).addBox(-1.0F, -2.0F, -20.0F, 2.0F, 2.0F, 20.0F, 0.0F, false);

		bone125 = new ModelRenderer(this);
		bone125.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone122.addChild(bone125);
		setRotationAngle(bone125, 0.0F, -1.0472F, 0.0F);
		

		bone126 = new ModelRenderer(this);
		bone126.setRotationPoint(0.0F, 0.0F, 33.75F);
		bone125.addChild(bone126);
		setRotationAngle(bone126, 0.2618F, 0.0F, 0.0F);
		bone126.setTextureOffset(26, 68).addBox(-1.0F, -2.0F, -20.0F, 2.0F, 2.0F, 20.0F, 0.0F, false);

		bone127 = new ModelRenderer(this);
		bone127.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone125.addChild(bone127);
		setRotationAngle(bone127, 0.0F, -1.0472F, 0.0F);
		

		bone128 = new ModelRenderer(this);
		bone128.setRotationPoint(0.0F, 0.0F, 33.75F);
		bone127.addChild(bone128);
		setRotationAngle(bone128, 0.2618F, 0.0F, 0.0F);
		bone128.setTextureOffset(26, 68).addBox(-1.0F, -2.0F, -20.0F, 2.0F, 2.0F, 20.0F, 0.0F, false);

		bone132 = new ModelRenderer(this);
		bone132.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone127.addChild(bone132);
		setRotationAngle(bone132, 0.0F, -1.0472F, 0.0F);
		

		bone133 = new ModelRenderer(this);
		bone133.setRotationPoint(0.0F, 0.0F, 33.75F);
		bone132.addChild(bone133);
		setRotationAngle(bone133, 0.2618F, 0.0F, 0.0F);
		bone133.setTextureOffset(26, 68).addBox(-1.0F, -2.0F, -20.0F, 2.0F, 2.0F, 20.0F, 0.0F, false);

		bone37 = new ModelRenderer(this);
		bone37.setRotationPoint(0.0F, -22.0F, 0.0F);
		base_console.addChild(bone37);
		setRotationAngle(bone37, 0.0F, -0.5236F, 0.0F);
		bone37.setTextureOffset(104, 106).addBox(-1.0F, -6.0F, 31.75F, 2.0F, 6.0F, 2.0F, 0.0F, false);

		bone104 = new ModelRenderer(this);
		bone104.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone37.addChild(bone104);
		setRotationAngle(bone104, 0.0F, -1.0472F, 0.0F);
		bone104.setTextureOffset(104, 106).addBox(-1.0F, -6.0F, 31.75F, 2.0F, 6.0F, 2.0F, 0.0F, false);

		bone105 = new ModelRenderer(this);
		bone105.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone104.addChild(bone105);
		setRotationAngle(bone105, 0.0F, -1.0472F, 0.0F);
		bone105.setTextureOffset(104, 106).addBox(-1.0F, -6.0F, 31.75F, 2.0F, 6.0F, 2.0F, 0.0F, false);

		bone106 = new ModelRenderer(this);
		bone106.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone105.addChild(bone106);
		setRotationAngle(bone106, 0.0F, -1.0472F, 0.0F);
		bone106.setTextureOffset(104, 106).addBox(-1.0F, -6.0F, 31.75F, 2.0F, 6.0F, 2.0F, 0.0F, false);

		bone107 = new ModelRenderer(this);
		bone107.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone106.addChild(bone107);
		setRotationAngle(bone107, 0.0F, -1.0472F, 0.0F);
		bone107.setTextureOffset(104, 106).addBox(-1.0F, -6.0F, 31.75F, 2.0F, 6.0F, 2.0F, 0.0F, false);

		bone108 = new ModelRenderer(this);
		bone108.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone107.addChild(bone108);
		setRotationAngle(bone108, 0.0F, -1.0472F, 0.0F);
		bone108.setTextureOffset(104, 106).addBox(-1.0F, -6.0F, 31.75F, 2.0F, 6.0F, 2.0F, 0.0F, false);

		bone31 = new ModelRenderer(this);
		bone31.setRotationPoint(0.0F, -22.0F, 14.0F);
		base_console.addChild(bone31);
		bone31.setTextureOffset(0, 58).addBox(-16.0F, -6.0F, 11.75F, 32.0F, 6.0F, 4.0F, 0.0F, false);

		bone99 = new ModelRenderer(this);
		bone99.setRotationPoint(0.0F, 0.0F, -14.0F);
		bone31.addChild(bone99);
		setRotationAngle(bone99, 0.0F, -1.0472F, 0.0F);
		bone99.setTextureOffset(0, 58).addBox(-16.0F, -6.0F, 25.75F, 32.0F, 6.0F, 4.0F, 0.0F, false);

		bone100 = new ModelRenderer(this);
		bone100.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone99.addChild(bone100);
		setRotationAngle(bone100, 0.0F, -1.0472F, 0.0F);
		bone100.setTextureOffset(0, 58).addBox(-16.0F, -6.0F, 25.75F, 32.0F, 6.0F, 4.0F, 0.0F, false);

		bone101 = new ModelRenderer(this);
		bone101.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone100.addChild(bone101);
		setRotationAngle(bone101, 0.0F, -1.0472F, 0.0F);
		bone101.setTextureOffset(0, 58).addBox(-16.0F, -6.0F, 25.75F, 32.0F, 6.0F, 4.0F, 0.0F, false);

		bone102 = new ModelRenderer(this);
		bone102.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone101.addChild(bone102);
		setRotationAngle(bone102, 0.0F, -1.0472F, 0.0F);
		bone102.setTextureOffset(0, 58).addBox(-16.0F, -6.0F, 25.75F, 32.0F, 6.0F, 4.0F, 0.0F, false);

		bone103 = new ModelRenderer(this);
		bone103.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone102.addChild(bone103);
		setRotationAngle(bone103, 0.0F, -1.0472F, 0.0F);
		bone103.setTextureOffset(0, 58).addBox(-16.0F, -6.0F, 25.75F, 32.0F, 6.0F, 4.0F, 0.0F, false);

		components = new ModelRenderer(this);
		components.setRotationPoint(0.0F, 24.0F, 0.0F);
		

		south_west = new ModelRenderer(this);
		south_west.setRotationPoint(0.0F, -27.0F, 0.0F);
		components.addChild(south_west);
		setRotationAngle(south_west, 0.0F, 1.0472F, 0.0F);
		

		bone174 = new ModelRenderer(this);
		bone174.setRotationPoint(0.0F, -1.0F, 29.75F);
		south_west.addChild(bone174);
		setRotationAngle(bone174, -0.4974F, 0.0F, 0.0F);
		bone174.setTextureOffset(24, 110).addBox(4.0F, 0.5F, -10.5F, 4.0F, 1.0F, 4.0F, 0.0F, false);
		bone174.setTextureOffset(24, 110).addBox(-8.0F, 0.5F, -10.5F, 4.0F, 1.0F, 4.0F, 0.0F, true);
		bone174.setTextureOffset(110, 77).addBox(-7.0F, 0.25F, -9.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		bone174.setTextureOffset(50, 73).addBox(4.0F, 0.5F, -6.5F, 6.0F, 1.0F, 4.0F, 0.0F, false);
		bone174.setTextureOffset(50, 73).addBox(-10.0F, 0.5F, -6.5F, 6.0F, 1.0F, 4.0F, 0.0F, true);
		bone174.setTextureOffset(32, 116).addBox(5.0F, -0.5F, -9.5F, 2.0F, 1.0F, 2.0F, 0.0F, false);
		bone174.setTextureOffset(112, 59).addBox(-3.5F, 0.75F, -9.5F, 7.0F, 1.0F, 6.0F, 0.0F, false);
		bone174.setTextureOffset(56, 114).addBox(-1.0F, 1.0F, -15.0F, 4.0F, 1.0F, 5.0F, 0.0F, false);
		bone174.setTextureOffset(96, 129).addBox(-4.5F, 0.75F, -14.5F, 3.0F, 1.0F, 3.0F, 0.0F, false);

		incrementmodifier_rotate_Y60 = new ModelRenderer(this);
		incrementmodifier_rotate_Y60.setRotationPoint(-6.0F, 0.1667F, -8.5F);
		bone174.addChild(incrementmodifier_rotate_Y60);
		setRotationAngle(incrementmodifier_rotate_Y60, 0.0F, -0.5236F, 0.0F);
		incrementmodifier_rotate_Y60.setTextureOffset(64, 120).addBox(-0.5F, -0.1667F, -1.5F, 1.0F, 1.0F, 3.0F, 0.0F, false);
		incrementmodifier_rotate_Y60.setTextureOffset(81, 0).addBox(-0.5F, -0.6667F, -1.5F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		incrementmodifier_rotate_Y60.setTextureOffset(81, 0).addBox(-0.5F, -0.6667F, 0.5F, 1.0F, 1.0F, 1.0F, 0.0F, false);

		randomiser = new ModelRenderer(this);
		randomiser.setRotationPoint(21.9604F, 0.3226F, 1.6577F);
		bone174.addChild(randomiser);
		randomiser.setTextureOffset(82, 57).addBox(-17.4604F, -0.3226F, -7.9077F, 5.0F, 1.0F, 2.0F, 0.0F, false);

		randomiser_glow = new LightModelRenderer(this);
		randomiser_glow.setRotationPoint(-0.5F, 0.0F, 2.5F);
		randomiser.addChild(randomiser_glow);
		randomiser_glow.setTextureOffset(36, 68).addBox(-13.9604F, -0.8226F, -10.4077F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		randomiser_glow.setTextureOffset(36, 68).addBox(-15.9604F, -0.8226F, -10.4077F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		randomiser_glow.setTextureOffset(80, 84).addBox(-13.9604F, -0.8226F, -8.1577F, 2.0F, 1.0F, 1.0F, 0.0F, false);
		randomiser_glow.setTextureOffset(80, 84).addBox(-16.9604F, -0.8226F, -8.1577F, 2.0F, 1.0F, 1.0F, 0.0F, false);

		communicator = new ModelRenderer(this);
		communicator.setRotationPoint(14.4604F, 0.3226F, -1.3423F);
		bone174.addChild(communicator);
		communicator.setTextureOffset(72, 0).addBox(-17.4604F, -1.0726F, -7.6577F, 2.0F, 2.0F, 5.0F, 0.0F, false);

		landtype_selector = new ModelRenderer(this);
		landtype_selector.setRotationPoint(2.25F, 1.0F, -12.0F);
		bone174.addChild(landtype_selector);
		setRotationAngle(landtype_selector, 0.3054F, 0.0F, 0.0F);
		landtype_selector.setTextureOffset(142, 77).addBox(-0.5F, -0.5F, -1.5F, 1.0F, 1.0F, 3.0F, 0.0F, false);

		sonicport = new ModelRenderer(this);
		sonicport.setRotationPoint(24.4604F, 1.3226F, -8.8423F);
		bone174.addChild(sonicport);
		sonicport.setTextureOffset(0, 92).addBox(-20.9604F, -1.5726F, -5.1577F, 3.0F, 2.0F, 3.0F, 0.0F, false);

		southwest_glow = new LightModelRenderer(this);
		southwest_glow.setRotationPoint(14.9604F, 0.5726F, -8.3423F);
		bone174.addChild(southwest_glow);
		southwest_glow.setTextureOffset(0, 58).addBox(-20.9604F, -0.8226F, -3.6577F, 1.0F, 2.0F, 1.0F, 0.0F, false);
		southwest_glow.setTextureOffset(66, 75).addBox(-20.9604F, -0.0726F, -4.9077F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		southwest_glow.setTextureOffset(66, 75).addBox(-20.9604F, -0.0726F, -6.1577F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		southwest_glow.setTextureOffset(74, 35).addBox(-13.2104F, -0.5726F, -6.4077F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		southwest_glow.setTextureOffset(74, 35).addBox(-14.4604F, -0.5726F, -6.4077F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		southwest_glow.setTextureOffset(74, 35).addBox(-15.7104F, -0.5726F, -6.4077F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		southwest_glow.setTextureOffset(78, 58).addBox(-20.4604F, -0.5726F, 4.3423F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		southwest_glow.setTextureOffset(78, 58).addBox(-20.4604F, -0.5726F, 2.3423F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		southwest_glow.setTextureOffset(78, 58).addBox(-24.4604F, -0.5726F, 2.3423F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		southwest_glow.setTextureOffset(78, 58).addBox(-24.4604F, -0.5726F, 4.3423F, 1.0F, 1.0F, 1.0F, 0.0F, false);

		bone190 = new ModelRenderer(this);
		bone190.setRotationPoint(7.9604F, -0.1774F, -0.3423F);
		bone174.addChild(bone190);
		bone190.setTextureOffset(130, 100).addBox(-15.9604F, -0.3226F, -5.1577F, 2.0F, 1.0F, 2.0F, 0.0F, false);

		bone212 = new ModelRenderer(this);
		bone212.setRotationPoint(17.4604F, 0.3226F, -1.3423F);
		bone174.addChild(bone212);
		bone212.setTextureOffset(96, 124).addBox(-18.2104F, -0.3226F, -7.1577F, 4.0F, 1.0F, 4.0F, 0.0F, false);

		bone239 = new ModelRenderer(this);
		bone239.setRotationPoint(0.375F, 1.0F, -12.0F);
		bone174.addChild(bone239);
		setRotationAngle(bone239, -0.3054F, 0.0F, 0.0F);
		bone239.setTextureOffset(141, 5).addBox(0.125F, -0.5F, -1.5F, 1.0F, 2.0F, 3.0F, 0.0F, false);
		bone239.setTextureOffset(141, 5).addBox(-1.125F, -0.5F, -1.5F, 1.0F, 2.0F, 3.0F, 0.0F, false);

		bone215 = new ModelRenderer(this);
		bone215.setRotationPoint(-3.0F, -0.25F, -13.0F);
		bone174.addChild(bone215);
		bone215.setTextureOffset(24, 130).addBox(-1.0F, 0.0F, -1.0F, 2.0F, 1.0F, 2.0F, 0.0F, false);

		north_west = new ModelRenderer(this);
		north_west.setRotationPoint(0.0F, -27.0F, 0.0F);
		components.addChild(north_west);
		setRotationAngle(north_west, 0.0F, 2.0944F, 0.0F);
		

		bone173 = new ModelRenderer(this);
		bone173.setRotationPoint(0.0F, -1.0F, 29.75F);
		north_west.addChild(bone173);
		setRotationAngle(bone173, -0.4974F, 0.0F, 0.0F);
		bone173.setTextureOffset(124, 12).addBox(7.0F, 0.25F, -6.5F, 3.0F, 1.0F, 3.0F, 0.0F, false);
		bone173.setTextureOffset(16, 76).addBox(8.0F, -0.5F, -5.75F, 1.0F, 1.0F, 2.0F, 0.0F, false);
		bone173.setTextureOffset(12, 18).addBox(6.0F, -0.75F, -9.5F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		bone173.setTextureOffset(82, 90).addBox(-4.0F, 0.75F, -17.0F, 2.0F, 1.0F, 2.0F, 0.0F, false);
		bone173.setTextureOffset(74, 90).addBox(2.0F, 0.75F, -17.0F, 2.0F, 1.0F, 2.0F, 0.0F, false);

		dimensioncontrol = new ModelRenderer(this);
		dimensioncontrol.setRotationPoint(17.9604F, 0.8226F, -0.3423F);
		bone173.addChild(dimensioncontrol);
		

		dimensioncontrol_glow = new LightModelRenderer(this);
		dimensioncontrol_glow.setRotationPoint(0.0F, 0.0F, 0.0F);
		dimensioncontrol.addChild(dimensioncontrol_glow);
		dimensioncontrol_glow.setTextureOffset(106, 35).addBox(-22.9604F, -0.8226F, -13.6577F, 10.0F, 1.0F, 11.0F, 0.0F, false);
		dimensioncontrol_glow.setTextureOffset(110, 73).addBox(-22.9604F, -0.5726F, -13.1577F, 10.0F, 1.0F, 10.0F, 0.0F, false);

		dimensionrotate_Y35 = new ModelRenderer(this);
		dimensionrotate_Y35.setRotationPoint(-15.4604F, -0.8226F, -11.1577F);
		dimensioncontrol.addChild(dimensionrotate_Y35);
		setRotationAngle(dimensionrotate_Y35, 0.0F, -0.6109F, 0.0F);
		dimensionrotate_Y35.setTextureOffset(16, 25).addBox(-0.5F, -1.0F, -0.5F, 1.0F, 2.0F, 1.0F, 0.0F, false);

		bone228 = new ModelRenderer(this);
		bone228.setRotationPoint(0.5F, -1.0F, 0.0F);
		dimensionrotate_Y35.addChild(bone228);
		setRotationAngle(bone228, 0.0F, 0.0F, -0.1745F);
		bone228.setTextureOffset(68, 44).addBox(-1.0F, -1.0F, -0.5F, 2.0F, 1.0F, 1.0F, 0.0F, false);

		dimensionrotate_Y65 = new ModelRenderer(this);
		dimensionrotate_Y65.setRotationPoint(-19.9604F, -1.0726F, -10.6577F);
		dimensioncontrol.addChild(dimensionrotate_Y65);
		setRotationAngle(dimensionrotate_Y65, 0.0F, -0.5672F, 0.0F);
		dimensionrotate_Y65.setTextureOffset(38, 80).addBox(-0.5F, -1.25F, -1.5F, 1.0F, 1.0F, 3.0F, 0.0F, false);
		dimensionrotate_Y65.setTextureOffset(0, 18).addBox(-0.5F, -0.25F, -0.5F, 1.0F, 2.0F, 1.0F, 0.0F, false);

		dimensionrotate_Y50 = new ModelRenderer(this);
		dimensionrotate_Y50.setRotationPoint(-19.9604F, -1.5726F, -6.6577F);
		dimensioncontrol.addChild(dimensionrotate_Y50);
		setRotationAngle(dimensionrotate_Y50, 0.0F, -1.0908F, 0.0F);
		dimensionrotate_Y50.setTextureOffset(76, 35).addBox(-0.5F, -1.25F, -1.0F, 1.0F, 1.0F, 2.0F, 0.0F, false);
		dimensionrotate_Y50.setTextureOffset(16, 22).addBox(-0.5F, -0.25F, -0.5F, 1.0F, 2.0F, 1.0F, 0.0F, false);

		directioncontrol_rotate_Y90 = new ModelRenderer(this);
		directioncontrol_rotate_Y90.setRotationPoint(-8.0F, 0.75F, -5.0F);
		bone173.addChild(directioncontrol_rotate_Y90);
		setRotationAngle(directioncontrol_rotate_Y90, 0.0F, -0.5236F, 0.0F);
		directioncontrol_rotate_Y90.setTextureOffset(128, 30).addBox(-1.5F, -0.475F, -1.5F, 3.0F, 1.0F, 3.0F, 0.0F, false);
		directioncontrol_rotate_Y90.setTextureOffset(12, 0).addBox(-1.5F, -1.475F, -1.5F, 1.0F, 2.0F, 1.0F, 0.0F, false);

		bone177 = new ModelRenderer(this);
		bone177.setRotationPoint(-3.0F, 0.1667F, -16.0F);
		bone173.addChild(bone177);
		setRotationAngle(bone177, 0.0F, -0.7418F, 0.0F);
		bone177.setTextureOffset(68, 26).addBox(-0.5F, -0.1667F, 0.5F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		bone177.setTextureOffset(37, 84).addBox(-0.5F, 0.3333F, -1.5F, 1.0F, 1.0F, 3.0F, 0.0F, false);
		bone177.setTextureOffset(68, 26).addBox(-0.5F, -0.1667F, -1.5F, 1.0F, 1.0F, 1.0F, 0.0F, false);

		bone227 = new ModelRenderer(this);
		bone227.setRotationPoint(3.0F, 0.1667F, -16.0F);
		bone173.addChild(bone227);
		setRotationAngle(bone227, 0.0F, -0.7418F, 0.0F);
		bone227.setTextureOffset(76, 40).addBox(-0.5F, 0.3333F, -1.5F, 1.0F, 1.0F, 2.0F, 0.0F, false);
		bone227.setTextureOffset(68, 24).addBox(-0.5F, -0.4167F, -1.5F, 1.0F, 1.0F, 1.0F, 0.0F, false);

		north = new ModelRenderer(this);
		north.setRotationPoint(0.0F, -27.0F, 0.0F);
		components.addChild(north);
		setRotationAngle(north, 0.0F, 3.1416F, 0.0F);
		

		bone179 = new ModelRenderer(this);
		bone179.setRotationPoint(0.0F, -1.0F, 29.75F);
		north.addChild(bone179);
		setRotationAngle(bone179, -0.4974F, 0.0F, 0.0F);
		bone179.setTextureOffset(46, 90).addBox(-2.0F, 0.25F, -16.5F, 4.0F, 1.0F, 2.0F, 0.0F, false);
		bone179.setTextureOffset(50, 68).addBox(-4.0F, 0.25F, -11.0F, 8.0F, 1.0F, 3.0F, 0.0F, false);
		bone179.setTextureOffset(58, 90).addBox(-8.0F, 0.25F, -10.75F, 3.0F, 1.0F, 2.0F, 0.0F, false);
		bone179.setTextureOffset(16, 79).addBox(2.0F, -0.25F, -13.0F, 2.0F, 2.0F, 1.0F, 0.0F, false);
		bone179.setTextureOffset(76, 43).addBox(-4.0F, -0.25F, -13.0F, 2.0F, 2.0F, 1.0F, 0.0F, false);
		bone179.setTextureOffset(76, 43).addBox(-1.0F, -0.25F, -13.0F, 2.0F, 2.0F, 1.0F, 0.0F, false);
		bone179.setTextureOffset(105, 129).addBox(-1.0F, 0.25F, -7.5F, 2.0F, 1.0F, 2.0F, 0.0F, false);
		bone179.setTextureOffset(0, 88).addBox(-4.5F, 0.75F, -16.0F, 2.0F, 1.0F, 1.0F, 0.0F, false);
		bone179.setTextureOffset(62, 85).addBox(2.5F, 0.75F, -16.0F, 2.0F, 1.0F, 1.0F, 0.0F, false);
		bone179.setTextureOffset(16, 31).addBox(8.25F, -0.75F, -3.75F, 1.0F, 2.0F, 1.0F, 0.0F, false);
		bone179.setTextureOffset(16, 31).addBox(10.25F, -0.75F, -3.75F, 1.0F, 2.0F, 1.0F, 0.0F, false);
		bone179.setTextureOffset(104, 57).addBox(-10.0F, 0.25F, -7.0F, 2.0F, 1.0F, 2.0F, 0.0F, false);

		door_rotate_x_100 = new ModelRenderer(this);
		door_rotate_x_100.setRotationPoint(6.5F, 1.2F, -6.5F);
		bone179.addChild(door_rotate_x_100);
		door_rotate_x_100.setTextureOffset(60, 24).addBox(-1.0F, -2.0F, -1.0F, 2.0F, 2.0F, 4.0F, 0.0F, false);
		door_rotate_x_100.setTextureOffset(68, 40).addBox(-0.5F, -2.0F, 3.0F, 1.0F, 1.0F, 6.0F, 0.0F, false);

		fastreturn_rotate_X65 = new ModelRenderer(this);
		fastreturn_rotate_X65.setRotationPoint(-6.5F, 0.5F, -9.75F);
		bone179.addChild(fastreturn_rotate_X65);
		setRotationAngle(fastreturn_rotate_X65, -0.5672F, 0.0F, 0.0F);
		fastreturn_rotate_X65.setTextureOffset(68, 60).addBox(-0.5F, -1.75F, -0.5F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		fastreturn_rotate_X65.setTextureOffset(80, 53).addBox(-1.0F, -0.75F, -0.5F, 2.0F, 1.0F, 1.0F, 0.0F, false);

		landtype_rotate_Y70 = new ModelRenderer(this);
		landtype_rotate_Y70.setRotationPoint(6.5F, 0.5F, -10.25F);
		bone179.addChild(landtype_rotate_Y70);
		setRotationAngle(landtype_rotate_Y70, 0.0F, 0.6109F, 0.0F);
		landtype_rotate_Y70.setTextureOffset(16, 84).addBox(-1.0F, -1.0F, -0.5F, 2.0F, 1.0F, 1.0F, 0.0F, false);
		landtype_rotate_Y70.setTextureOffset(96, 57).addBox(-1.0F, -0.25F, -1.0F, 2.0F, 1.0F, 2.0F, 0.0F, false);

		refuel = new ModelRenderer(this);
		refuel.setRotationPoint(13.4604F, 0.8226F, 6.6577F);
		bone179.addChild(refuel);
		refuel.setTextureOffset(105, 129).addBox(-10.4604F, -0.4726F, -11.4077F, 2.0F, 1.0F, 2.0F, 0.0F, false);
		refuel.setTextureOffset(105, 129).addBox(-12.9604F, -0.4726F, -11.4077F, 2.0F, 1.0F, 2.0F, 0.0F, false);
		refuel.setTextureOffset(105, 129).addBox(-15.4604F, -0.5726F, -11.4077F, 2.0F, 1.0F, 2.0F, 0.0F, false);
		refuel.setTextureOffset(105, 129).addBox(-17.9604F, -0.5726F, -11.4077F, 2.0F, 1.0F, 2.0F, 0.0F, false);

		north_glow = new LightModelRenderer(this);
		north_glow.setRotationPoint(17.9604F, 1.3226F, 3.6577F);
		bone179.addChild(north_glow);
		north_glow.setTextureOffset(106, 43).addBox(-9.7104F, -1.0726F, -9.9077F, 2.0F, 1.0F, 2.0F, 0.0F, false);
		north_glow.setTextureOffset(80, 73).addBox(-28.7104F, -2.0726F, -7.6577F, 2.0F, 2.0F, 1.0F, 0.0F, false);
		north_glow.setTextureOffset(68, 58).addBox(-22.9604F, -0.5726F, -10.6577F, 4.0F, 1.0F, 1.0F, 0.0F, false);
		north_glow.setTextureOffset(72, 12).addBox(-16.9604F, -0.5726F, -10.6577F, 4.0F, 1.0F, 1.0F, 0.0F, false);

		bone233 = new ModelRenderer(this);
		bone233.setRotationPoint(0.0F, 0.25F, -9.5F);
		bone179.addChild(bone233);
		setRotationAngle(bone233, 0.6981F, 0.0F, 0.0F);
		bone233.setTextureOffset(65, 125).addBox(2.5F, -1.75F, -0.5F, 1.0F, 3.0F, 1.0F, 0.0F, false);
		bone233.setTextureOffset(50, 72).addBox(2.5F, -1.75F, 0.5F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		bone233.setTextureOffset(65, 125).addBox(0.5F, -1.75F, -0.5F, 1.0F, 3.0F, 1.0F, 0.0F, false);
		bone233.setTextureOffset(50, 72).addBox(0.5F, -1.75F, 0.5F, 1.0F, 1.0F, 1.0F, 0.0F, false);

		bone234 = new ModelRenderer(this);
		bone234.setRotationPoint(0.0F, 0.25F, -9.5F);
		bone179.addChild(bone234);
		bone234.setTextureOffset(65, 125).addBox(-1.5F, -1.75F, -0.5F, 1.0F, 3.0F, 1.0F, 0.0F, false);
		bone234.setTextureOffset(50, 72).addBox(-1.5F, -1.75F, 0.5F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		bone234.setTextureOffset(65, 125).addBox(-3.5F, -1.75F, -0.5F, 1.0F, 3.0F, 1.0F, 0.0F, false);
		bone234.setTextureOffset(50, 72).addBox(-3.5F, -1.75F, 0.5F, 1.0F, 1.0F, 1.0F, 0.0F, false);

		bone194 = new ModelRenderer(this);
		bone194.setRotationPoint(-5.5F, 0.75F, -13.0F);
		bone179.addChild(bone194);
		setRotationAngle(bone194, 0.0F, 0.5236F, 0.0F);
		bone194.setTextureOffset(0, 70).addBox(-0.5F, -0.5F, -0.5F, 1.0F, 1.0F, 1.0F, 0.0F, false);

		bone183 = new ModelRenderer(this);
		bone183.setRotationPoint(-0.5F, -0.5F, 0.0F);
		bone194.addChild(bone183);
		setRotationAngle(bone183, 0.0F, 0.0F, 0.3927F);
		bone183.setTextureOffset(16, 82).addBox(-1.0F, -1.0F, -0.5F, 2.0F, 1.0F, 1.0F, 0.0F, false);

		bone182 = new ModelRenderer(this);
		bone182.setRotationPoint(5.5F, 0.75F, -13.0F);
		bone179.addChild(bone182);
		setRotationAngle(bone182, 0.0F, -0.5236F, 0.0F);
		bone182.setTextureOffset(0, 70).addBox(-0.5F, -0.5F, -0.5F, 1.0F, 1.0F, 1.0F, 0.0F, true);

		bone225 = new ModelRenderer(this);
		bone225.setRotationPoint(0.5F, -0.5F, 0.0F);
		bone182.addChild(bone225);
		setRotationAngle(bone225, 0.0F, 0.0F, -0.3927F);
		bone225.setTextureOffset(16, 82).addBox(-1.0F, -1.0F, -0.5F, 2.0F, 1.0F, 1.0F, 0.0F, true);

		door2_rotate_x_100 = new ModelRenderer(this);
		door2_rotate_x_100.setRotationPoint(-6.5F, 1.2F, -6.5F);
		bone179.addChild(door2_rotate_x_100);
		door2_rotate_x_100.setTextureOffset(60, 24).addBox(-1.0F, -2.0F, -1.0F, 2.0F, 2.0F, 4.0F, 0.0F, false);
		door2_rotate_x_100.setTextureOffset(68, 40).addBox(-0.5F, -2.0F, 3.0F, 1.0F, 1.0F, 6.0F, 0.0F, false);

		north_east = new ModelRenderer(this);
		north_east.setRotationPoint(0.0F, -27.0F, 0.0F);
		components.addChild(north_east);
		setRotationAngle(north_east, 0.0F, -2.0944F, 0.0F);
		

		bone180 = new ModelRenderer(this);
		bone180.setRotationPoint(0.0F, -1.0F, 29.75F);
		north_east.addChild(bone180);
		setRotationAngle(bone180, -0.4974F, 0.0F, 0.0F);
		bone180.setTextureOffset(50, 85).addBox(-2.5F, 0.5F, -16.5F, 5.0F, 1.0F, 2.0F, 0.0F, false);
		bone180.setTextureOffset(74, 84).addBox(-1.0F, -0.5F, -16.0F, 2.0F, 1.0F, 1.0F, 0.0F, false);
		bone180.setTextureOffset(14, 86).addBox(3.0F, -0.25F, -16.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		bone180.setTextureOffset(14, 86).addBox(-5.0F, -0.25F, -16.0F, 2.0F, 2.0F, 2.0F, 0.0F, true);
		bone180.setTextureOffset(104, 106).addBox(-6.5F, 0.25F, -12.0F, 13.0F, 1.0F, 9.0F, 0.0F, false);

		bone181 = new ModelRenderer(this);
		bone181.setRotationPoint(-4.0F, 1.25F, -15.0F);
		bone180.addChild(bone181);
		setRotationAngle(bone181, 0.0F, -0.7854F, 0.0F);
		bone181.setTextureOffset(68, 40).addBox(-0.5F, -1.0F, 0.0607F, 1.0F, 2.0F, 2.0F, 0.0F, false);

		bone184 = new ModelRenderer(this);
		bone184.setRotationPoint(4.0F, 1.25F, -15.0F);
		bone180.addChild(bone184);
		setRotationAngle(bone184, 0.0F, 0.7854F, 0.0F);
		bone184.setTextureOffset(68, 40).addBox(-0.5F, -1.0F, 0.0607F, 1.0F, 2.0F, 2.0F, 0.0F, true);

		monitor_glow_optional = new LightModelRenderer(this);
		monitor_glow_optional.setRotationPoint(0.0F, 2.75F, -7.5F);
		bone180.addChild(monitor_glow_optional);
		setRotationAngle(monitor_glow_optional, 0.7854F, 0.0F, 0.0F);
		monitor_glow_optional.setTextureOffset(60, 12).addBox(-5.05F, -6.0F, 0.0F, 0.0F, 6.0F, 6.0F, 0.0F, false);
		monitor_glow_optional.setTextureOffset(60, 12).addBox(5.05F, -6.0F, 0.0F, 0.0F, 6.0F, 6.0F, 0.0F, false);

		monitor_glow = new LightModelRenderer(this);
		monitor_glow.setRotationPoint(15.4604F, -1.6774F, -1.8423F);
		bone180.addChild(monitor_glow);
		monitor_glow.setTextureOffset(112, 16).addBox(-20.9604F, 0.9274F, -9.1577F, 11.0F, 1.0F, 7.0F, 0.0F, false);

		bone186 = new ModelRenderer(this);
		bone186.setRotationPoint(9.0F, 1.25F, -4.5F);
		bone180.addChild(bone186);
		setRotationAngle(bone186, 0.0F, 0.48F, 0.0F);
		bone186.setTextureOffset(72, 7).addBox(-1.5F, -1.5F, -1.0F, 3.0F, 2.0F, 3.0F, 0.0F, false);
		bone186.setTextureOffset(8, 130).addBox(-0.5F, -1.0F, -4.0F, 2.0F, 1.0F, 2.0F, 0.0F, false);

		bone185 = new ModelRenderer(this);
		bone185.setRotationPoint(-9.0F, 1.25F, -4.5F);
		bone180.addChild(bone185);
		setRotationAngle(bone185, 0.0F, -0.48F, 0.0F);
		bone185.setTextureOffset(72, 7).addBox(-1.5F, -1.5F, -1.0F, 3.0F, 2.0F, 3.0F, 0.0F, true);
		bone185.setTextureOffset(8, 130).addBox(-1.5F, -1.0F, -4.0F, 2.0F, 1.0F, 2.0F, 0.0F, true);

		south_east = new ModelRenderer(this);
		south_east.setRotationPoint(0.0F, -27.0F, 0.0F);
		components.addChild(south_east);
		setRotationAngle(south_east, 0.0F, -1.0472F, 0.0F);
		

		bone187 = new ModelRenderer(this);
		bone187.setRotationPoint(0.0F, -1.0F, 29.75F);
		south_east.addChild(bone187);
		setRotationAngle(bone187, -0.4974F, 0.0F, 0.0F);
		

		telepathic_glow = new LightModelRenderer(this);
		telepathic_glow.setRotationPoint(16.9604F, 0.3226F, 0.1577F);
		bone187.addChild(telepathic_glow);
		telepathic_glow.setTextureOffset(60, 18).addBox(-26.9604F, -0.1226F, -14.6577F, 20.0F, 1.0F, 12.0F, 0.0F, false);
		telepathic_glow.setTextureOffset(60, 60).addBox(-26.9604F, -0.6226F, -15.1577F, 20.0F, 1.0F, 12.0F, 0.0F, false);

		south = new ModelRenderer(this);
		south.setRotationPoint(0.0F, -27.0F, 0.0F);
		components.addChild(south);
		

		bone188 = new ModelRenderer(this);
		bone188.setRotationPoint(0.0F, -1.0F, 29.75F);
		south.addChild(bone188);
		setRotationAngle(bone188, -0.4974F, 0.0F, 0.0F);
		bone188.setTextureOffset(112, 66).addBox(-9.25F, -0.25F, -7.5F, 5.0F, 2.0F, 4.0F, 0.0F, false);
		bone188.setTextureOffset(74, 95).addBox(-3.0F, 0.75F, -12.0F, 6.0F, 1.0F, 4.0F, 0.0F, false);
		bone188.setTextureOffset(16, 130).addBox(-6.75F, 0.25F, -12.0F, 2.0F, 1.0F, 2.0F, 0.0F, false);
		bone188.setTextureOffset(12, 68).addBox(4.5F, 0.25F, -13.0F, 2.0F, 1.0F, 3.0F, 0.0F, false);
		bone188.setTextureOffset(60, 35).addBox(-3.0F, 0.75F, -15.5F, 6.0F, 1.0F, 2.0F, 0.0F, false);

		throttle_rotate_X80 = new ModelRenderer(this);
		throttle_rotate_X80.setRotationPoint(-6.75F, 0.25F, -5.5F);
		bone188.addChild(throttle_rotate_X80);
		setRotationAngle(throttle_rotate_X80, 0.7854F, 0.0F, 0.0F);
		throttle_rotate_X80.setTextureOffset(16, 72).addBox(-2.0F, -1.5F, -1.0F, 1.0F, 2.0F, 2.0F, 0.0F, false);
		throttle_rotate_X80.setTextureOffset(16, 72).addBox(1.0F, -1.5F, -1.0F, 1.0F, 2.0F, 2.0F, 0.0F, false);
		throttle_rotate_X80.setTextureOffset(68, 53).addBox(-2.5F, -4.75F, -1.0F, 5.0F, 1.0F, 2.0F, 0.0F, false);
		throttle_rotate_X80.setTextureOffset(0, 68).addBox(-0.5F, -4.75F, 0.0F, 1.0F, 1.0F, 1.0F, 0.0F, false);

		bone220 = new ModelRenderer(this);
		bone220.setRotationPoint(-1.0F, -1.5F, 0.0F);
		throttle_rotate_X80.addChild(bone220);
		setRotationAngle(bone220, 0.0F, 0.0F, -0.1309F);
		bone220.setTextureOffset(0, 0).addBox(-1.0F, -3.0F, -0.5F, 1.0F, 3.0F, 1.0F, 0.0F, false);

		bone221 = new ModelRenderer(this);
		bone221.setRotationPoint(1.0F, -1.5F, 0.0F);
		throttle_rotate_X80.addChild(bone221);
		setRotationAngle(bone221, 0.0F, 0.0F, 0.1309F);
		bone221.setTextureOffset(0, 0).addBox(0.0F, -3.0F, -0.5F, 1.0F, 3.0F, 1.0F, 0.0F, true);

		handbrake_rotate_Y130 = new ModelRenderer(this);
		handbrake_rotate_Y130.setRotationPoint(7.0F, 1.25F, -5.5F);
		bone188.addChild(handbrake_rotate_Y130);
		setRotationAngle(handbrake_rotate_Y130, 0.0F, -2.2689F, 0.0F);
		handbrake_rotate_Y130.setTextureOffset(0, 97).addBox(-1.5F, -2.0F, -1.5F, 3.0F, 2.0F, 3.0F, 0.0F, false);

		bone196 = new ModelRenderer(this);
		bone196.setRotationPoint(0.5F, -2.0F, 0.0F);
		handbrake_rotate_Y130.addChild(bone196);
		setRotationAngle(bone196, 0.0F, 0.0F, 0.1309F);
		bone196.setTextureOffset(26, 84).addBox(-6.0F, -1.0F, -0.5F, 6.0F, 2.0F, 1.0F, 0.0F, false);

		stabiliser = new ModelRenderer(this);
		stabiliser.setRotationPoint(26.7104F, -0.1774F, 8.1577F);
		bone188.addChild(stabiliser);
		stabiliser.setTextureOffset(32, 99).addBox(-29.7104F, 0.4274F, -15.6577F, 6.0F, 1.0F, 4.0F, 0.0F, false);
		stabiliser.setTextureOffset(50, 74).addBox(-28.7104F, -0.5726F, -15.1577F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		stabiliser.setTextureOffset(50, 74).addBox(-27.2104F, -0.5726F, -15.1577F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		stabiliser.setTextureOffset(50, 74).addBox(-25.7104F, -0.5726F, -15.1577F, 1.0F, 1.0F, 1.0F, 0.0F, false);

		stabiliser_glow = new LightModelRenderer(this);
		stabiliser_glow.setRotationPoint(1.25F, 0.5F, 2.0F);
		stabiliser.addChild(stabiliser_glow);
		stabiliser_glow.setTextureOffset(68, 47).addBox(-30.4604F, -0.5726F, -15.1577F, 5.0F, 1.0F, 1.0F, 0.0F, false);

		Xincrement = new LightModelRenderer(this);
		Xincrement.setRotationPoint(2.0F, 1.25F, -14.5F);
		bone188.addChild(Xincrement);
		setRotationAngle(Xincrement, 0.48F, 0.0F, 0.0F);
		Xincrement.setTextureOffset(60, 24).addBox(-0.5F, -2.0F, -0.5F, 1.0F, 2.0F, 1.0F, 0.0F, false);

		bone222 = new ModelRenderer(this);
		bone222.setRotationPoint(0.0F, -2.0F, 0.5F);
		Xincrement.addChild(bone222);
		setRotationAngle(bone222, 0.3927F, 0.0F, 0.0F);
		bone222.setTextureOffset(66, 73).addBox(-0.5F, -1.0F, -1.025F, 1.0F, 1.0F, 1.0F, 0.025F, false);

		Yincrement = new LightModelRenderer(this);
		Yincrement.setRotationPoint(0.0F, 1.25F, -14.5F);
		bone188.addChild(Yincrement);
		setRotationAngle(Yincrement, 0.48F, 0.0F, 0.0F);
		Yincrement.setTextureOffset(12, 40).addBox(-0.5F, -2.0F, -0.5F, 1.0F, 2.0F, 1.0F, 0.0F, false);

		bone223 = new ModelRenderer(this);
		bone223.setRotationPoint(0.0F, -2.0F, 0.5F);
		Yincrement.addChild(bone223);
		setRotationAngle(bone223, 0.3927F, 0.0F, 0.0F);
		bone223.setTextureOffset(66, 73).addBox(-0.5F, -1.0F, -1.025F, 1.0F, 1.0F, 1.0F, 0.025F, false);

		Zincrement = new LightModelRenderer(this);
		Zincrement.setRotationPoint(-2.0F, 1.25F, -14.5F);
		bone188.addChild(Zincrement);
		Zincrement.setTextureOffset(0, 40).addBox(-0.5F, -2.0F, -0.5F, 1.0F, 2.0F, 1.0F, 0.0F, false);

		bone224 = new ModelRenderer(this);
		bone224.setRotationPoint(0.0F, -2.0F, 0.5F);
		Zincrement.addChild(bone224);
		setRotationAngle(bone224, 0.3927F, 0.0F, 0.0F);
		bone224.setTextureOffset(66, 73).addBox(-0.5F, -1.0F, -1.025F, 1.0F, 1.0F, 1.0F, 0.025F, false);

		south_glow = new LightModelRenderer(this);
		south_glow.setRotationPoint(26.2104F, 1.3226F, 4.1577F);
		bone188.addChild(south_glow);
		south_glow.setTextureOffset(72, 0).addBox(-34.9604F, -0.5726F, -13.1577F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		south_glow.setTextureOffset(72, 0).addBox(-31.9604F, -0.5726F, -13.1577F, 1.0F, 1.0F, 1.0F, 0.0F, false);

		bone235 = new ModelRenderer(this);
		bone235.setRotationPoint(0.0F, 0.75F, -10.0F);
		bone188.addChild(bone235);
		setRotationAngle(bone235, -0.3054F, 0.0F, 0.0F);
		bone235.setTextureOffset(55, 135).addBox(1.0F, -1.0F, -1.5F, 1.0F, 2.0F, 3.0F, 0.0F, false);
		bone235.setTextureOffset(55, 135).addBox(-0.5F, -1.0F, -1.5F, 1.0F, 2.0F, 3.0F, 0.0F, false);

		bone236 = new ModelRenderer(this);
		bone236.setRotationPoint(0.0F, 0.75F, -10.0F);
		bone188.addChild(bone236);
		setRotationAngle(bone236, 0.3927F, 0.0F, 0.0F);
		bone236.setTextureOffset(55, 135).addBox(-2.0F, -1.0F, -1.5F, 1.0F, 2.0F, 3.0F, 0.0F, false);

		bone237 = new ModelRenderer(this);
		bone237.setRotationPoint(-5.75F, 0.25F, -11.0F);
		bone188.addChild(bone237);
		setRotationAngle(bone237, 0.5236F, 0.0F, 0.0F);
		bone237.setTextureOffset(16, 34).addBox(-0.5F, -1.5F, -0.5F, 1.0F, 2.0F, 1.0F, 0.0F, false);

		bone238 = new ModelRenderer(this);
		bone238.setRotationPoint(5.5F, 0.75F, -11.5F);
		bone188.addChild(bone238);
		setRotationAngle(bone238, 0.6109F, 0.0F, 0.0F);
		bone238.setTextureOffset(72, 2).addBox(-0.5F, -1.5F, -0.5F, 1.0F, 2.0F, 1.0F, 0.0F, false);

		bone219 = new ModelRenderer(this);
		bone219.setRotationPoint(6.5F, 1.75F, -6.0F);
		bone188.addChild(bone219);
		bone219.setTextureOffset(124, 66).addBox(-3.0F, -1.0F, -3.0F, 6.0F, 1.0F, 6.0F, 0.0F, false);

		rotor_top_translate_8 = new ModelRenderer(this);
		rotor_top_translate_8.setRotationPoint(0.0F, -103.0F, 0.0F);
		setRotationAngle(rotor_top_translate_8, 0.0F, -0.7854F, 0.0F);
		rotor_top_translate_8.setTextureOffset(110, 47).addBox(-5.0F, 57.5F, -5.0F, 10.0F, 2.0F, 10.0F, 0.0F, false);
		rotor_top_translate_8.setTextureOffset(0, 110).addBox(-4.0F, 46.5F, -4.0F, 8.0F, 12.0F, 8.0F, 0.0F, false);
		rotor_top_translate_8.setTextureOffset(74, 74).addBox(-6.0F, 49.5F, -6.0F, 12.0F, 4.0F, 12.0F, 0.0F, false);
		rotor_top_translate_8.setTextureOffset(44, 96).addBox(-5.0F, 38.5F, -5.0F, 10.0F, 8.0F, 10.0F, 0.0F, false);
		rotor_top_translate_8.setTextureOffset(74, 74).addBox(-1.0F, 56.45F, -1.0F, 2.0F, 8.0F, 2.0F, 0.0F, false);

		rotortop_glow = new LightModelRenderer(this);
		rotortop_glow.setRotationPoint(0.0F, 0.0F, 0.0F);
		rotor_top_translate_8.addChild(rotortop_glow);
		rotortop_glow.setTextureOffset(118, 84).addBox(-3.0F, 59.475F, -3.0F, 6.0F, 10.0F, 6.0F, 0.0F, false);

		rotor_bottom_translate_14 = new ModelRenderer(this);
		rotor_bottom_translate_14.setRotationPoint(0.0F, 24.0F, 0.0F);
		rotor_bottom_translate_14.setTextureOffset(110, 47).addBox(-5.0F, -59.5F, -5.0F, 10.0F, 2.0F, 10.0F, 0.0F, false);
		rotor_bottom_translate_14.setTextureOffset(32, 114).addBox(-4.0F, -58.5F, -4.0F, 8.0F, 12.0F, 8.0F, 0.0F, false);
		rotor_bottom_translate_14.setTextureOffset(82, 90).addBox(-6.0F, -53.5F, -6.0F, 12.0F, 4.0F, 12.0F, 0.0F, false);
		rotor_bottom_translate_14.setTextureOffset(74, 106).addBox(-5.0F, -46.5F, -5.0F, 10.0F, 8.0F, 10.0F, 0.0F, false);
		rotor_bottom_translate_14.setTextureOffset(38, 68).addBox(-1.0F, -63.55F, -1.0F, 2.0F, 10.0F, 2.0F, 0.0F, false);

		rotorbottom_glow = new LightModelRenderer(this);
		rotorbottom_glow.setRotationPoint(0.0F, 0.0F, 0.0F);
		rotor_bottom_translate_14.addChild(rotorbottom_glow);
		rotorbottom_glow.setTextureOffset(64, 124).addBox(-3.0F, -69.5F, -3.0F, 6.0F, 10.0F, 6.0F, 0.0F, false);
	}

	@Override
	public void setRotationAngles(Entity entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch){
		//previously the render function, render code was moved to a method below
	}

	@Override
	public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha){
		
	}

	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}

	@Override
	public void render(ToyotaConsoleTile tile, float scale, MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
		
		matrixStack.push();
		matrixStack.translate(0, (float) Math.cos(tile.flightTicks * 0.1) * 0.2F, 0);
		this.rotorbottom_glow.setBright(1F);
		this.rotor_bottom_translate_14.render(matrixStack, buffer, packedLight, packedOverlay);
		matrixStack.pop();
		
		matrixStack.push();
		matrixStack.translate(0, (float) Math.cos(tile.flightTicks * 0.1) * -0.2F, 0);
		this.rotortop_glow.setBright(1F);
		this.rotor_top_translate_8.render(matrixStack, buffer, packedLight, packedOverlay);
		matrixStack.pop();
		
		
		tile.getControl(ThrottleControl.class).ifPresent(throttle -> {
			this.throttle_rotate_X80.rotateAngleX = (float) Math.toRadians(-60 + 110 * throttle.getAmount());	
		});
        
		tile.getControl(HandbrakeControl.class).ifPresent(handbrake -> {
			this.handbrake_rotate_Y130.rotateAngleY = (float) Math.toRadians(handbrake.isFree() ? -60 : 40);	
		});
        
		tile.getControl(FacingControl.class).ifPresent(facing -> {
			this.directioncontrol_rotate_Y90.rotateAngleY = (float) Math.toRadians(facing.getAnimationTicks() * 20);	
		});
        
		tile.getControl(RefuelerControl.class).ifPresent(refueler -> {
			this.refuel.rotateAngleX = refueler.isRefueling() ? 0.0325F : 0;	
		});
        
		tile.getControl(XControl.class).ifPresent(x -> {
			this.Xincrement.rotateAngleX = x.getAnimationTicks() == 0 ? 0 : -0.025F;
			this.Xincrement.setBright(x.getAnimationTicks() == 0 ? 0 : 1F);
		}); 
        
		tile.getControl(YControl.class).ifPresent(y -> {
			this.Yincrement.rotateAngleX = y.getAnimationTicks() == 0 ? 0 : -0.025F;
			this.Yincrement.setBright(y.getAnimationTicks() == 0 ? 0 : 1F);
		});
        
		tile.getControl(ZControl.class).ifPresent(z -> {
			this.Zincrement.rotateAngleX = z.getAnimationTicks() == 0 ? 0 : -0.025F;
			this.Zincrement.setBright(z.getAnimationTicks() == 0 ? 0 : 1F);
		});
		
		tile.getControl(DoorControl.class).ifPresent(doorControl -> {
            float angle = 0;
			
			DoorEntity door = tile.getDoor().orElse(null);
			if(door != null)
				angle = door.getOpenState() == EnumDoorState.CLOSED ? 0 : 90;
			
			this.door2_rotate_x_100.rotateAngleX = (float)Math.toRadians(angle);
		});
		
		tile.getControl(LandingTypeControl.class).ifPresent(type -> {
			this.landtype_rotate_Y70.rotateAngleY = type.getLandType() == EnumLandType.UP ? 70 : -70;
		});
		
		long time = Minecraft.getInstance().world.getGameTime() % 120;
		this.base_glow.setBright(1F);
		this.dimensioncontrol_glow.setBright(1F);
		this.monitor_glow.setBright(1F);
		this.monitor_glow_optional.setBright(1F);
		this.randomiser_glow.setBright(time > 30 ? 1.0F : 0.0F);
		this.rotorbeam_glow.setBright(1F);
		this.southwest_glow.setBright(1F);
		this.telepathic_glow.setBright(0.5F);
		this.north_glow.setBright(1F);
		this.south_glow.setBright(1F);
		this.southwest_glow.setBright(1F);
		this.stabiliser_glow.setBright(time < 30 ? 1.0F : 0.0F);
        
        this.base_console.render(matrixStack, buffer, packedLight, packedOverlay);
        components.render(matrixStack, buffer, packedLight, packedOverlay);
        

    }
}