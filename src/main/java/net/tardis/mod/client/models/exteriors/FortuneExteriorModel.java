package net.tardis.mod.client.models.exteriors;
// Made with Blockbench 3.7.4
// Exported for Minecraft version 1.15
// Paste this class into your mod and generate all required imports

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.util.math.vector.Vector3f;
import net.tardis.mod.client.models.LightModelRenderer;
import net.tardis.mod.client.renderers.boti.BOTIRenderer;
import net.tardis.mod.client.renderers.boti.PortalInfo;
import net.tardis.mod.client.renderers.exteriors.ExteriorRenderer;
import net.tardis.mod.client.renderers.exteriors.FortuneExteriorRenderer;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.misc.IDoorType.EnumDoorType;
//import net.tardis.mod.entity.TardisEntity;
//import net.tardis.mod.helper.ModelHelper;
//import net.tardis.mod.misc.IDoorType.EnumDoorType;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public class FortuneExteriorModel extends ExteriorModel {
	private final LightModelRenderer glow_crystal;
	private final ModelRenderer glow_crystal1;
	private final ModelRenderer glow_crystal2;
	private final ModelRenderer glow_crystal3;
	private final ModelRenderer glow_crystal4;
	private final LightModelRenderer glow_topper;
	private final ModelRenderer door;
	private final ModelRenderer latch;
	private final ModelRenderer boti;
	private final ModelRenderer box;
	private final ModelRenderer base;
	private final ModelRenderer detailing;
	private final ModelRenderer detailing2;
	private final ModelRenderer front;
	private final ModelRenderer front_panels;
	private final ModelRenderer front_brasstrim_tilt;
	private final ModelRenderer front_brasstrim;
	private final ModelRenderer lid;
	private final ModelRenderer posts;
	private final ModelRenderer nw_post;
	private final ModelRenderer sw_post;
	private final ModelRenderer ne_post;
	private final ModelRenderer se_post;
	private final ModelRenderer curtains;
	private final ModelRenderer curtains_left;
	private final ModelRenderer curtains_right;
	private final ModelRenderer curtains_back;
	private final ModelRenderer dias;
	private final ModelRenderer coinreturn;
	private final ModelRenderer button;

	public FortuneExteriorModel() {
		textureWidth = 512;
		textureHeight = 512;

		glow_crystal = new LightModelRenderer(this);
		glow_crystal.setRotationPoint(0.0F, -76.0F, -8.0F);
		setRotationAngle(glow_crystal, -0.6109F, -0.6109F, 0.4363F);
		

		glow_crystal1 = new ModelRenderer(this);
		glow_crystal1.setRotationPoint(0.0F, 0.0F, 0.0F);
		glow_crystal.addChild(glow_crystal1);
		glow_crystal1.setTextureOffset(200, 21).addBox(-8.0F, -8.0F, -8.0F, 16.0F, 16.0F, 16.0F, 0.0F, false);

		glow_crystal2 = new ModelRenderer(this);
		glow_crystal2.setRotationPoint(0.0F, 0.0F, 0.0F);
		glow_crystal.addChild(glow_crystal2);
		setRotationAngle(glow_crystal2, -0.7854F, -0.7854F, 0.0F);
		glow_crystal2.setTextureOffset(200, 21).addBox(-8.0F, -8.0F, -8.0F, 16.0F, 16.0F, 16.0F, 0.0F, false);

		glow_crystal3 = new ModelRenderer(this);
		glow_crystal3.setRotationPoint(0.0F, 0.0F, 0.0F);
		glow_crystal.addChild(glow_crystal3);
		setRotationAngle(glow_crystal3, -0.7854F, 0.0F, -0.7854F);
		glow_crystal3.setTextureOffset(218, 68).addBox(-4.0F, -4.0F, -4.0F, 8.0F, 8.0F, 8.0F, 0.0F, false);

		glow_crystal4 = new ModelRenderer(this);
		glow_crystal4.setRotationPoint(0.0F, 0.0F, 0.0F);
		glow_crystal.addChild(glow_crystal4);
		setRotationAngle(glow_crystal4, 0.1745F, 0.5236F, -0.1745F);
		glow_crystal4.setTextureOffset(218, 68).addBox(-4.0F, -4.0F, -4.0F, 8.0F, 8.0F, 8.0F, 0.0F, false);

		glow_topper = new LightModelRenderer(this);
		glow_topper.setRotationPoint(0.0F, 24.0F, 0.0F);
		glow_topper.setTextureOffset(161, 54).addBox(-22.0F, -164.0F, -22.0F, 44.0F, 4.0F, 44.0F, 0.0F, false);

		door = new ModelRenderer(this);
		door.setRotationPoint(-24.0F, 24.0F, 28.0F);
		door.setTextureOffset(253, 106).addBox(40.0F, -154.0F, 0.0F, 8.0F, 148.0F, 4.0F, 0.0F, false);
		door.setTextureOffset(179, 110).addBox(2.0F, -151.6F, 2.0F, 44.0F, 144.0F, 1.0F, 0.0F, false);
		door.setTextureOffset(256, 108).addBox(0.0F, -154.0F, 0.0F, 8.0F, 148.0F, 4.0F, 0.0F, false);
		door.setTextureOffset(224, 241).addBox(8.0F, -18.0F, 0.0F, 32.0F, 12.0F, 4.0F, 0.0F, false);
		door.setTextureOffset(292, 125).addBox(8.0F, -154.0F, 0.0F, 32.0F, 12.0F, 4.0F, 0.0F, false);
		door.setTextureOffset(139, 105).addBox(8.0F, -112.4F, 0.0F, 32.0F, 4.0F, 4.0F, 0.0F, false);
		door.setTextureOffset(139, 105).addBox(8.0F, -54.0F, 0.0F, 32.0F, 4.0F, 4.0F, 0.0F, false);

		latch = new ModelRenderer(this);
		latch.setRotationPoint(0.0F, 0.0F, 0.0F);
		door.addChild(latch);
		latch.setTextureOffset(166, 8).addBox(42.0F, -88.0F, -2.0F, 4.0F, 8.0F, 8.0F, 0.0F, false);
		latch.setTextureOffset(110, 16).addBox(42.0F, -78.0F, 1.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);

		boti = new ModelRenderer(this);
		boti.setRotationPoint(0.0F, 24.0F, 0.0F);
		boti.setTextureOffset(0, 96).addBox(-24.0F, -156.0F, 25.0F, 48.0F, 152.0F, 4.0F, 0.0F, false);

		box = new ModelRenderer(this);
		box.setRotationPoint(0.0F, 24.0F, 0.0F);
		

		base = new ModelRenderer(this);
		base.setRotationPoint(0.0F, 0.0F, 0.0F);
		box.addChild(base);
		base.setTextureOffset(126, 180).addBox(-36.0F, -4.0F, -36.0F, 72.0F, 4.0F, 72.0F, 0.0F, false);
		base.setTextureOffset(132, 182).addBox(-34.0F, -6.0F, -34.0F, 68.0F, 4.0F, 68.0F, 0.0F, false);
		base.setTextureOffset(155, 191).addBox(-30.0F, -12.0F, -30.0F, 60.0F, 8.0F, 56.0F, 0.0F, false);
		base.setTextureOffset(122, 111).addBox(-30.0F, -76.0F, -30.0F, 60.0F, 4.0F, 56.0F, 0.0F, false);
		base.setTextureOffset(290, 52).addBox(-18.0F, -82.0F, -22.0F, 36.0F, 8.0F, 36.0F, 0.0F, false);
		base.setTextureOffset(129, 135).addBox(-28.0F, -72.0F, -28.0F, 56.0F, 60.0F, 56.0F, 0.0F, false);

		detailing = new ModelRenderer(this);
		detailing.setRotationPoint(0.0F, 0.0F, 0.0F);
		base.addChild(detailing);
		detailing.setTextureOffset(108, 30).addBox(25.0F, -66.0F, -20.0F, 4.0F, 2.0F, 40.0F, 0.0F, false);
		detailing.setTextureOffset(105, 25).addBox(25.0F, -22.0F, -20.0F, 4.0F, 2.0F, 40.0F, 0.0F, false);
		detailing.setTextureOffset(150, 30).addBox(25.0F, -64.0F, -20.0F, 4.0F, 42.0F, 2.0F, 0.0F, false);
		detailing.setTextureOffset(130, 25).addBox(25.0F, -64.0F, 18.0F, 4.0F, 42.0F, 2.0F, 0.0F, false);
		detailing.setTextureOffset(198, 168).addBox(25.0F, -62.0F, -16.0F, 4.0F, 36.0F, 32.0F, 0.0F, false);
		detailing.setTextureOffset(207, 189).addBox(26.0F, -54.0F, -8.0F, 4.0F, 20.0F, 16.0F, 0.0F, false);

		detailing2 = new ModelRenderer(this);
		detailing2.setRotationPoint(0.0F, 0.0F, 0.0F);
		base.addChild(detailing2);
		detailing2.setTextureOffset(108, 19).addBox(-29.0F, -66.0F, -20.0F, 4.0F, 2.0F, 40.0F, 0.0F, false);
		detailing2.setTextureOffset(108, 38).addBox(-29.0F, -22.0F, -20.0F, 4.0F, 2.0F, 40.0F, 0.0F, false);
		detailing2.setTextureOffset(158, 25).addBox(-29.0F, -64.0F, -20.0F, 4.0F, 42.0F, 2.0F, 0.0F, false);
		detailing2.setTextureOffset(139, 19).addBox(-29.0F, -64.0F, 18.0F, 4.0F, 42.0F, 2.0F, 0.0F, false);
		detailing2.setTextureOffset(205, 168).addBox(-29.0F, -62.0F, -16.0F, 4.0F, 36.0F, 32.0F, 0.0F, false);
		detailing2.setTextureOffset(208, 190).addBox(-30.0F, -54.0F, -8.0F, 4.0F, 20.0F, 16.0F, 0.0F, false);

		front = new ModelRenderer(this);
		front.setRotationPoint(-0.5F, -142.0F, -29.5F);
		box.addChild(front);
		

		front_panels = new ModelRenderer(this);
		front_panels.setRotationPoint(0.0F, 0.0F, 0.0F);
		front.addChild(front_panels);
		setRotationAngle(front_panels, 0.0F, 0.0F, 0.7854F);
		front_panels.setTextureOffset(306, 151).addBox(-2.5F, -21.0F, 0.5F, 28.0F, 17.0F, 1.0F, 0.0F, false);
		front_panels.setTextureOffset(264, 148).addBox(-2.5F, -4.0F, 0.5F, 8.0F, 8.0F, 1.0F, 0.0F, false);
		front_panels.setTextureOffset(268, 141).addBox(-7.5F, 25.0F, 0.5F, 9.0F, 9.0F, 1.0F, 0.0F, false);
		front_panels.setTextureOffset(268, 141).addBox(60.5F, 25.0F, 0.5F, 4.0F, 9.0F, 1.0F, 0.0F, false);
		front_panels.setTextureOffset(294, 148).addBox(25.5F, -9.0F, 0.5F, 9.0F, 9.0F, 1.0F, 0.0F, false);
		front_panels.setTextureOffset(294, 148).addBox(25.5F, 60.0F, 0.5F, 9.0F, 3.0F, 1.0F, 0.0F, false);
		front_panels.setTextureOffset(300, 161).addBox(-19.5F, -4.0F, 0.5F, 17.0F, 29.0F, 1.0F, 0.0F, false);

		front_brasstrim_tilt = new ModelRenderer(this);
		front_brasstrim_tilt.setRotationPoint(0.0F, 0.0F, 0.0F);
		front.addChild(front_brasstrim_tilt);
		setRotationAngle(front_brasstrim_tilt, 0.0F, 0.0F, 0.7854F);
		front_brasstrim_tilt.setTextureOffset(130, 61).addBox(6.5F, -4.0F, 0.0F, 18.0F, 1.0F, 2.0F, 0.0F, false);
		front_brasstrim_tilt.setTextureOffset(130, 61).addBox(-2.5F, 4.0F, 0.0F, 8.0F, 1.0F, 2.0F, 0.0F, false);
		front_brasstrim_tilt.setTextureOffset(130, 61).addBox(-2.5F, 24.0F, 0.0F, 5.0F, 1.0F, 2.0F, 0.0F, false);
		front_brasstrim_tilt.setTextureOffset(130, 61).addBox(24.5F, -4.0F, 0.0F, 1.0F, 5.0F, 2.0F, 0.0F, false);
		front_brasstrim_tilt.setTextureOffset(130, 61).addBox(5.5F, -4.0F, 0.0F, 1.0F, 9.0F, 2.0F, 0.0F, false);
		front_brasstrim_tilt.setTextureOffset(130, 61).addBox(1.5F, 25.0F, 0.0F, 1.0F, 11.0F, 2.0F, 0.0F, false);
		front_brasstrim_tilt.setTextureOffset(130, 61).addBox(59.75F, 25.0F, 0.0F, 1.0F, 11.0F, 2.0F, 0.0F, false);
		front_brasstrim_tilt.setTextureOffset(130, 61).addBox(-2.5F, 5.0F, 0.0F, 1.0F, 19.0F, 2.0F, 0.0F, false);
		front_brasstrim_tilt.setTextureOffset(130, 61).addBox(25.5F, 0.0F, 0.0F, 10.0F, 1.0F, 2.0F, 0.0F, false);
		front_brasstrim_tilt.setTextureOffset(130, 61).addBox(25.5F, 59.0F, 0.0F, 10.0F, 1.0F, 2.0F, 0.0F, false);

		front_brasstrim = new ModelRenderer(this);
		front_brasstrim.setRotationPoint(0.0F, 0.0F, 0.0F);
		front.addChild(front_brasstrim);
		front_brasstrim.setTextureOffset(130, 61).addBox(-18.5F, 65.0F, 0.1F, 38.0F, 1.0F, 2.0F, 0.0F, false);
		front_brasstrim.setTextureOffset(111, 69).addBox(-23.5F, 70.0F, 1.1F, 24.0F, 2.0F, 1.0F, 0.0F, false);
		front_brasstrim.setTextureOffset(111, 69).addBox(-23.0F, 128.0F, 1.1F, 23.0F, 2.0F, 1.0F, 0.0F, false);
		front_brasstrim.setTextureOffset(108, 13).addBox(-23.0F, 72.0F, 1.1F, 2.0F, 56.0F, 1.0F, 0.0F, false);
		front_brasstrim.setTextureOffset(108, 13).addBox(22.0F, 72.0F, 1.1F, 2.0F, 56.0F, 1.0F, 0.0F, false);
		front_brasstrim.setTextureOffset(111, 69).addBox(0.5F, 70.0F, 1.1F, 24.0F, 2.0F, 1.0F, 0.0F, false);
		front_brasstrim.setTextureOffset(111, 69).addBox(0.0F, 128.0F, 1.1F, 24.0F, 2.0F, 1.0F, 0.0F, false);
		front_brasstrim.setTextureOffset(141, 27).addBox(-23.0F, 25.0F, 0.1F, 1.0F, 37.0F, 2.0F, 0.0F, false);
		front_brasstrim.setTextureOffset(141, 27).addBox(23.25F, 24.75F, 0.1F, 1.0F, 37.0F, 2.0F, 0.0F, false);

		lid = new ModelRenderer(this);
		lid.setRotationPoint(0.0F, 0.0F, 0.0F);
		box.addChild(lid);
		lid.setTextureOffset(131, 155).addBox(-36.0F, -160.0F, -36.0F, 72.0F, 4.0F, 72.0F, 0.0F, false);
		lid.setTextureOffset(144, 168).addBox(-34.0F, -158.0F, -34.0F, 68.0F, 4.0F, 68.0F, 0.0F, false);
		lid.setTextureOffset(300, 155).addBox(26.0F, -154.0F, -26.0F, 4.0F, 8.0F, 52.0F, 0.0F, false);
		lid.setTextureOffset(288, 158).addBox(-30.0F, -154.0F, -26.0F, 4.0F, 8.0F, 52.0F, 0.0F, false);
		lid.setTextureOffset(330, 136).addBox(-25.0F, -154.0F, -30.0F, 52.0F, 8.0F, 4.0F, 0.0F, false);
		lid.setTextureOffset(122, 111).addBox(21.0F, -164.0F, -25.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
		lid.setTextureOffset(122, 111).addBox(-26.0F, -166.0F, -26.0F, 52.0F, 2.0F, 52.0F, 0.0F, false);
		lid.setTextureOffset(122, 111).addBox(-25.0F, -164.0F, -25.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
		lid.setTextureOffset(122, 111).addBox(-25.0F, -164.0F, 21.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
		lid.setTextureOffset(122, 111).addBox(21.0F, -164.0F, 21.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
		lid.setTextureOffset(122, 111).addBox(5.0F, -164.0F, -25.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
		lid.setTextureOffset(122, 111).addBox(13.0F, -164.0F, -25.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
		lid.setTextureOffset(122, 111).addBox(-9.0F, -164.0F, -25.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
		lid.setTextureOffset(122, 111).addBox(-17.0F, -164.0F, -25.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
		lid.setTextureOffset(122, 111).addBox(21.0F, -164.0F, -17.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
		lid.setTextureOffset(122, 111).addBox(21.0F, -164.0F, -9.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
		lid.setTextureOffset(122, 111).addBox(21.0F, -164.0F, 12.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
		lid.setTextureOffset(122, 111).addBox(21.0F, -164.0F, 3.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
		lid.setTextureOffset(122, 111).addBox(13.0F, -164.0F, 21.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
		lid.setTextureOffset(122, 111).addBox(5.0F, -164.0F, 21.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
		lid.setTextureOffset(122, 111).addBox(-17.0F, -164.0F, 21.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
		lid.setTextureOffset(122, 111).addBox(-9.0F, -164.0F, 21.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
		lid.setTextureOffset(122, 111).addBox(-25.0F, -164.0F, 13.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
		lid.setTextureOffset(122, 111).addBox(-25.0F, -164.0F, 5.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
		lid.setTextureOffset(122, 111).addBox(-25.0F, -164.0F, -17.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
		lid.setTextureOffset(122, 111).addBox(-25.0F, -164.0F, -9.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
		lid.setTextureOffset(122, 111).addBox(-26.0F, -154.0F, 26.0F, 52.0F, 8.0F, 4.0F, 0.0F, false);

		posts = new ModelRenderer(this);
		posts.setRotationPoint(0.0F, 0.0F, 0.0F);
		box.addChild(posts);
		

		nw_post = new ModelRenderer(this);
		nw_post.setRotationPoint(0.0F, 0.0F, 0.0F);
		posts.addChild(nw_post);
		nw_post.setTextureOffset(178, 236).addBox(24.0F, -16.0F, -32.0F, 8.0F, 12.0F, 8.0F, 0.0F, false);
		nw_post.setTextureOffset(115, 118).addBox(24.0F, -141.0F, -32.0F, 8.0F, 120.0F, 8.0F, 0.0F, false);
		nw_post.setTextureOffset(299, 118).addBox(24.0F, -156.0F, -32.0F, 8.0F, 12.0F, 8.0F, 0.0F, false);
		nw_post.setTextureOffset(168, 231).addBox(25.0F, -24.0F, -31.0F, 6.0F, 8.0F, 6.0F, 0.0F, false);
		nw_post.setTextureOffset(160, 115).addBox(25.0F, -148.0F, -31.0F, 6.0F, 8.0F, 6.0F, 0.0F, false);
		nw_post.setTextureOffset(160, 135).addBox(25.0F, -123.4F, -32.4F, 6.0F, 100.0F, 6.0F, 0.0F, false);
		nw_post.setTextureOffset(160, 135).addBox(26.6F, -123.4F, -30.8F, 6.0F, 100.0F, 6.0F, 0.0F, false);
		nw_post.setTextureOffset(160, 115).addBox(25.0F, -139.4F, -32.4F, 6.0F, 14.0F, 6.0F, 0.0F, false);
		nw_post.setTextureOffset(160, 115).addBox(26.6F, -139.4F, -30.8F, 6.0F, 14.0F, 6.0F, 0.0F, false);
		nw_post.setTextureOffset(160, 115).addBox(23.6F, -139.4F, -30.8F, 6.0F, 14.0F, 6.0F, 0.0F, false);
		nw_post.setTextureOffset(160, 131).addBox(23.6F, -123.4F, -30.8F, 6.0F, 100.0F, 6.0F, 0.0F, false);
		nw_post.setTextureOffset(160, 135).addBox(25.0F, -123.4F, -29.8F, 6.0F, 100.0F, 6.0F, 0.0F, false);
		nw_post.setTextureOffset(160, 115).addBox(25.0F, -139.4F, -29.8F, 6.0F, 14.0F, 6.0F, 0.0F, false);
		nw_post.setTextureOffset(190, 241).addBox(23.0F, -8.0F, -33.0F, 10.0F, 4.0F, 10.0F, 0.0F, false);
		nw_post.setTextureOffset(297, 118).addBox(23.0F, -156.0F, -33.0F, 10.0F, 4.0F, 10.0F, 0.0F, false);

		sw_post = new ModelRenderer(this);
		sw_post.setRotationPoint(0.0F, 0.0F, 0.0F);
		posts.addChild(sw_post);
		sw_post.setTextureOffset(169, 237).addBox(24.0F, -16.0F, 24.0F, 8.0F, 12.0F, 8.0F, 0.0F, false);
		sw_post.setTextureOffset(119, 114).addBox(24.0F, -141.0F, 24.0F, 8.0F, 120.0F, 8.0F, 0.0F, false);
		sw_post.setTextureOffset(290, 114).addBox(24.0F, -156.0F, 24.0F, 8.0F, 12.0F, 8.0F, 0.0F, false);
		sw_post.setTextureOffset(268, 232).addBox(23.0F, -24.0F, 25.0F, 8.0F, 8.0F, 6.0F, 0.0F, false);
		sw_post.setTextureOffset(160, 115).addBox(23.0F, -148.0F, 25.0F, 8.0F, 8.0F, 6.0F, 0.0F, false);
		sw_post.setTextureOffset(160, 135).addBox(25.0F, -123.4F, 23.6F, 6.0F, 100.0F, 6.0F, 0.0F, false);
		sw_post.setTextureOffset(160, 135).addBox(26.6F, -123.4F, 25.2F, 6.0F, 100.0F, 6.0F, 0.0F, false);
		sw_post.setTextureOffset(160, 115).addBox(25.0F, -139.4F, 23.6F, 6.0F, 14.0F, 6.0F, 0.0F, false);
		sw_post.setTextureOffset(160, 115).addBox(26.6F, -139.4F, 25.2F, 6.0F, 14.0F, 6.0F, 0.0F, false);
		sw_post.setTextureOffset(160, 135).addBox(25.0F, -123.4F, 26.2F, 6.0F, 100.0F, 6.0F, 0.0F, false);
		sw_post.setTextureOffset(160, 115).addBox(25.0F, -139.4F, 26.2F, 6.0F, 14.0F, 6.0F, 0.0F, false);
		sw_post.setTextureOffset(161, 240).addBox(23.0F, -8.0F, 23.0F, 10.0F, 4.0F, 10.0F, 0.0F, false);
		sw_post.setTextureOffset(371, 114).addBox(23.0F, -156.0F, 23.0F, 10.0F, 4.0F, 10.0F, 0.0F, false);
		sw_post.setTextureOffset(160, 115).addBox(23.6F, -139.4F, 25.2F, 6.0F, 14.0F, 6.0F, 0.0F, false);
		sw_post.setTextureOffset(160, 115).addBox(23.6F, -123.4F, 25.2F, 6.0F, 100.0F, 6.0F, 0.0F, false);

		ne_post = new ModelRenderer(this);
		ne_post.setRotationPoint(0.0F, 0.0F, 0.0F);
		posts.addChild(ne_post);
		ne_post.setTextureOffset(176, 232).addBox(-32.0F, -16.0F, -32.0F, 8.0F, 12.0F, 8.0F, 0.0F, false);
		ne_post.setTextureOffset(112, 109).addBox(-32.0F, -141.0F, -32.0F, 8.0F, 120.0F, 8.0F, 0.0F, false);
		ne_post.setTextureOffset(328, 155).addBox(-32.0F, -156.0F, -32.0F, 8.0F, 12.0F, 8.0F, 0.0F, false);
		ne_post.setTextureOffset(157, 233).addBox(-31.0F, -24.0F, -31.0F, 6.0F, 8.0F, 6.0F, 0.0F, false);
		ne_post.setTextureOffset(160, 115).addBox(-31.0F, -148.0F, -31.0F, 6.0F, 8.0F, 6.0F, 0.0F, false);
		ne_post.setTextureOffset(160, 134).addBox(-31.0F, -123.4F, -32.4F, 6.0F, 100.0F, 6.0F, 0.0F, false);
		ne_post.setTextureOffset(160, 115).addBox(-29.4F, -123.4F, -30.8F, 6.0F, 100.0F, 6.0F, 0.0F, false);
		ne_post.setTextureOffset(160, 115).addBox(-31.0F, -139.4F, -32.4F, 6.0F, 14.0F, 6.0F, 0.0F, false);
		ne_post.setTextureOffset(160, 115).addBox(-29.4F, -139.4F, -30.8F, 6.0F, 14.0F, 6.0F, 0.0F, false);
		ne_post.setTextureOffset(160, 115).addBox(-32.4F, -139.4F, -30.8F, 6.0F, 14.0F, 6.0F, 0.0F, false);
		ne_post.setTextureOffset(160, 135).addBox(-32.4F, -123.4F, -30.8F, 6.0F, 100.0F, 6.0F, 0.0F, false);
		ne_post.setTextureOffset(160, 115).addBox(-31.0F, -123.4F, -29.8F, 6.0F, 100.0F, 6.0F, 0.0F, false);
		ne_post.setTextureOffset(160, 115).addBox(-31.0F, -139.4F, -29.8F, 6.0F, 14.0F, 6.0F, 0.0F, false);
		ne_post.setTextureOffset(194, 238).addBox(-33.0F, -8.0F, -33.0F, 10.0F, 4.0F, 10.0F, 0.0F, false);
		ne_post.setTextureOffset(332, 121).addBox(-33.0F, -156.0F, -33.0F, 10.0F, 4.0F, 10.0F, 0.0F, false);

		se_post = new ModelRenderer(this);
		se_post.setRotationPoint(0.0F, 0.0F, 0.0F);
		posts.addChild(se_post);
		se_post.setTextureOffset(175, 235).addBox(-32.0F, -16.0F, 24.0F, 8.0F, 12.0F, 8.0F, 0.0F, false);
		se_post.setTextureOffset(111, 114).addBox(-32.0F, -141.0F, 24.0F, 8.0F, 120.0F, 8.0F, 0.0F, false);
		se_post.setTextureOffset(332, 118).addBox(-32.0F, -156.0F, 24.0F, 8.0F, 12.0F, 8.0F, 0.0F, false);
		se_post.setTextureOffset(164, 231).addBox(-31.0F, -24.0F, 25.0F, 8.0F, 8.0F, 6.0F, 0.0F, false);
		se_post.setTextureOffset(160, 115).addBox(-31.0F, -148.0F, 25.0F, 8.0F, 8.0F, 6.0F, 0.0F, false);
		se_post.setTextureOffset(160, 115).addBox(-31.0F, -123.4F, 23.6F, 6.0F, 100.0F, 6.0F, 0.0F, false);
		se_post.setTextureOffset(160, 115).addBox(-31.0F, -139.4F, 23.6F, 6.0F, 14.0F, 6.0F, 0.0F, false);
		se_post.setTextureOffset(160, 115).addBox(-32.4F, -139.4F, 25.2F, 6.0F, 14.0F, 6.0F, 0.0F, false);
		se_post.setTextureOffset(200, 135).addBox(-32.4F, -123.4F, 25.2F, 6.0F, 100.0F, 6.0F, 0.0F, false);
		se_post.setTextureOffset(160, 135).addBox(-31.0F, -123.4F, 26.2F, 6.0F, 100.0F, 6.0F, 0.0F, false);
		se_post.setTextureOffset(160, 115).addBox(-31.0F, -139.4F, 26.2F, 6.0F, 14.0F, 6.0F, 0.0F, false);
		se_post.setTextureOffset(169, 238).addBox(-33.0F, -8.0F, 23.0F, 10.0F, 4.0F, 10.0F, 0.0F, false);
		se_post.setTextureOffset(320, 114).addBox(-33.0F, -156.0F, 23.0F, 10.0F, 4.0F, 10.0F, 0.0F, false);
		se_post.setTextureOffset(160, 115).addBox(-29.4F, -123.4F, 25.2F, 6.0F, 100.0F, 6.0F, 0.0F, false);
		se_post.setTextureOffset(160, 115).addBox(-29.4F, -139.4F, 25.2F, 6.0F, 14.0F, 6.0F, 0.0F, false);

		curtains = new ModelRenderer(this);
		curtains.setRotationPoint(0.0F, 0.0F, 0.0F);
		box.addChild(curtains);
		

		curtains_left = new ModelRenderer(this);
		curtains_left.setRotationPoint(0.0F, 0.0F, 0.0F);
		curtains.addChild(curtains_left);
		curtains_left.setTextureOffset(3, 2).addBox(25.0F, -156.0F, -24.0F, 4.0F, 80.0F, 12.0F, 0.0F, false);
		curtains_left.setTextureOffset(33, 2).addBox(25.0F, -156.0F, -6.0F, 4.0F, 80.0F, 12.0F, 0.0F, false);
		curtains_left.setTextureOffset(62, 2).addBox(25.0F, -156.0F, 12.0F, 4.0F, 80.0F, 12.0F, 0.0F, false);
		curtains_left.setTextureOffset(50, 2).addBox(23.0F, -156.0F, 1.0F, 4.0F, 80.0F, 12.0F, 0.0F, false);
		curtains_left.setTextureOffset(52, 2).addBox(23.0F, -156.0F, -17.0F, 4.0F, 80.0F, 12.0F, 0.0F, false);

		curtains_right = new ModelRenderer(this);
		curtains_right.setRotationPoint(0.0F, 0.0F, 0.0F);
		curtains.addChild(curtains_right);
		curtains_right.setTextureOffset(61, 2).addBox(-29.0F, -156.0F, -24.0F, 4.0F, 80.0F, 12.0F, 0.0F, false);
		curtains_right.setTextureOffset(2, 2).addBox(-29.0F, -156.0F, 12.0F, 4.0F, 80.0F, 12.0F, 0.0F, false);
		curtains_right.setTextureOffset(43, 2).addBox(-29.0F, -156.0F, -6.0F, 4.0F, 80.0F, 12.0F, 0.0F, false);
		curtains_right.setTextureOffset(30, 2).addBox(-25.0F, -156.0F, 3.0F, 4.0F, 80.0F, 12.0F, 0.0F, false);
		curtains_right.setTextureOffset(32, 2).addBox(-25.0F, -156.0F, -15.0F, 4.0F, 80.0F, 12.0F, 0.0F, false);

		curtains_back = new ModelRenderer(this);
		curtains_back.setRotationPoint(0.0F, 0.0F, 0.0F);
		curtains.addChild(curtains_back);
		curtains_back.setTextureOffset(33, 10).addBox(13.0F, -156.0F, 21.0F, 12.0F, 80.0F, 4.0F, 0.0F, false);
		curtains_back.setTextureOffset(33, 10).addBox(-24.0F, -156.0F, 21.0F, 12.0F, 80.0F, 4.0F, 0.0F, false);
		curtains_back.setTextureOffset(33, 10).addBox(-6.0F, -156.0F, 21.0F, 12.0F, 80.0F, 4.0F, 0.0F, false);
		curtains_back.setTextureOffset(33, 10).addBox(3.0F, -156.0F, 17.0F, 12.0F, 80.0F, 4.0F, 0.0F, false);
		curtains_back.setTextureOffset(33, 10).addBox(-14.0F, -156.0F, 17.0F, 12.0F, 80.0F, 4.0F, 0.0F, false);

		dias = new ModelRenderer(this);
		dias.setRotationPoint(0.0F, 0.0F, 0.0F);
		box.addChild(dias);
		dias.setTextureOffset(1, 36).addBox(-12.0F, -84.0F, -18.0F, 24.0F, 4.0F, 24.0F, 0.0F, false);
		dias.setTextureOffset(119, 61).addBox(-8.0F, -86.0F, -14.0F, 16.0F, 4.0F, 16.0F, 0.0F, false);
		dias.setTextureOffset(171, 54).addBox(-5.0F, -92.0F, -10.0F, 2.0F, 8.0F, 2.0F, 0.0F, false);
		dias.setTextureOffset(148, 59).addBox(3.0F, -92.0F, -10.0F, 2.0F, 8.0F, 2.0F, 0.0F, false);
		dias.setTextureOffset(158, 44).addBox(-5.0F, -92.0F, -3.0F, 2.0F, 8.0F, 2.0F, 0.0F, false);
		dias.setTextureOffset(130, 44).addBox(3.0F, -92.0F, -3.0F, 2.0F, 8.0F, 2.0F, 0.0F, false);

		coinreturn = new ModelRenderer(this);
		coinreturn.setRotationPoint(16.0F, -48.0F, -27.0F);
		box.addChild(coinreturn);
		setRotationAngle(coinreturn, -0.0873F, 0.0F, 0.0F);
		coinreturn.setTextureOffset(141, 44).addBox(-1.0F, -8.0F, -3.0F, 2.0F, 2.0F, 4.0F, 0.0F, false);
		coinreturn.setTextureOffset(141, 48).addBox(1.0F, -8.0F, -3.0F, 3.0F, 10.0F, 4.0F, 0.0F, false);
		coinreturn.setTextureOffset(141, 44).addBox(-4.0F, -8.0F, -3.0F, 3.0F, 10.0F, 4.0F, 0.0F, false);
		coinreturn.setTextureOffset(141, 44).addBox(-1.0F, -2.0F, -3.0F, 2.0F, 4.0F, 4.0F, 0.0F, false);
		coinreturn.setTextureOffset(141, 67).addBox(-4.0F, 4.0F, -3.0F, 8.0F, 4.0F, 4.0F, 0.0F, false);
		coinreturn.setTextureOffset(141, 44).addBox(2.0F, 2.0F, -3.0F, 2.0F, 2.0F, 4.0F, 0.0F, false);
		coinreturn.setTextureOffset(141, 44).addBox(-4.0F, 2.0F, -3.0F, 2.0F, 2.0F, 4.0F, 0.0F, false);
		coinreturn.setTextureOffset(342, 69).addBox(-2.6F, 5.4F, -4.0F, 1.0F, 1.0F, 4.0F, 0.0F, false);
		coinreturn.setTextureOffset(379, 109).addBox(-2.0F, -6.0F, -2.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
		coinreturn.setTextureOffset(388, 107).addBox(-2.0F, 1.0F, -2.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);

		button = new ModelRenderer(this);
		button.setRotationPoint(-12.0F, -59.0F, -27.0F);
		box.addChild(button);
		setRotationAngle(button, 0.0F, 0.0F, 0.7854F);
		button.setTextureOffset(52, 41).addBox(-2.0F, -2.0F, -3.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
		button.setTextureOffset(128, 40).addBox(-3.0F, -3.0F, -2.0F, 6.0F, 6.0F, 4.0F, 0.0F, false);
	}

	@Override
	public void setRotationAngles(Entity entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch){
		//previously the render function, render code was moved to a method below
	}

	@Override
	public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha){
		
	}

	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}

	@Override
	public void render(ExteriorTile exterior, float scale, MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay,float alpha) {
		matrixStack.push();
		matrixStack.scale(scale, scale, scale);

		matrixStack.rotate(Vector3f.YP.rotationDegrees(180));
        this.door.rotateAngleY = (float) Math.toRadians(EnumDoorType.FORTUNE.getRotationForState(exterior.getOpen()));
        door.render(matrixStack, buffer, packedLight, packedOverlay,1,1,1, alpha);
        //boti.render(matrixStack, buffer, packedLight, packedOverlay,1,1,1, alpha);
        box.render(matrixStack, buffer, packedLight, packedOverlay,1,1,1, alpha);
		glow_crystal.setBright(exterior.getLightLevel());
		glow_topper.setBright(exterior.getLightLevel());
        glow_crystal.render(matrixStack, buffer, packedLight, packedOverlay,1,1,1, alpha);
		glow_topper.render(matrixStack, buffer, packedLight, packedOverlay,1,1,1, alpha);
		matrixStack.pop();
		
		if(exterior.getBotiWorld() != null) {
			
			PortalInfo info = new PortalInfo();
			
			info.setPosition(exterior.getPos());
			info.setWorldShell(exterior.getBotiWorld());
			
			info.setTranslate(matrix -> {
				matrix.translate(-0.5, 0, -0.5);
				ExteriorRenderer.applyTransforms(matrix, exterior);
				matrix.translate(0, 0.1, -0.85);
			});
			
			info.setTranslatePortal(matrix -> {
				matrix.rotate(Vector3f.ZN.rotationDegrees(180));
				matrix.rotate(Vector3f.YP.rotationDegrees(WorldHelper.getAngleFromFacing(exterior.getBotiWorld().getPortalDirection())));
				matrix.translate(-0.5, -0.25, -0.5);
			});
			
			info.setRenderPortal((matrix, buf) -> {
				matrix.push();
				matrix.scale(0.25F, 0.25F, 0.25F);
				this.boti.render(matrix, buf.getBuffer(RenderType.getEntityTranslucent(FortuneExteriorRenderer.TEXTURE)), packedLight, packedOverlay);
				matrix.pop();
			});
			
			BOTIRenderer.addPortal(info);
			
		}
		
	}
}