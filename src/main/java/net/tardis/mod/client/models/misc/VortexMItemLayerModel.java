package net.tardis.mod.client.models.misc;
// Made with Blockbench 3.8.3
// Exported for Minecraft version 1.15 - 1.16
// Paste this class into your mod and generate all required imports

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.renderer.entity.model.BipedModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.entity.LivingEntity;

public class VortexMItemLayerModel extends BipedModel<LivingEntity> {
	
	private final ModelRenderer bipedLeftArm;
	private final ModelRenderer vm;
	private final ModelRenderer lid;
	private final ModelRenderer studs2;
	private final ModelRenderer layer;
	private final ModelRenderer bump;
	private final ModelRenderer straps;
	private final ModelRenderer body;
	private final ModelRenderer module;
	private final ModelRenderer outline;
	private final ModelRenderer studs;

	public VortexMItemLayerModel(float size) {
		super(size);
		textureWidth = 32;
		textureHeight = 32;

		bipedLeftArm = new ModelRenderer(this);
		bipedLeftArm.setRotationPoint(5.0F, 2.0F, 0.0F);
		

		vm = new ModelRenderer(this);
		vm.setRotationPoint(-4.0F, 4.0F, 0.0F);
		bipedLeftArm.addChild(vm);
		

		lid = new ModelRenderer(this);
		lid.setRotationPoint(1.5F, 0.0F, -2.5F);
		vm.addChild(lid);
		lid.setTextureOffset(20, 0).addBox(-3.5F, -2.5F, -0.5F, 4.0F, 5.0F, 1.0F, -0.4F, false);

		studs2 = new ModelRenderer(this);
		studs2.setRotationPoint(-10.5F, 31.0F, 2.5F);
		lid.addChild(studs2);
		studs2.setTextureOffset(0, 22).addBox(7.25F, -29.75F, -3.0F, 1.0F, 1.0F, 1.0F, -0.25F, false);
		studs2.setTextureOffset(0, 22).addBox(7.25F, -33.25F, -3.0F, 1.0F, 1.0F, 1.0F, -0.25F, false);
		studs2.setTextureOffset(0, 22).addBox(9.75F, -33.25F, -3.0F, 1.0F, 1.0F, 1.0F, -0.25F, false);
		studs2.setTextureOffset(0, 22).addBox(9.75F, -29.75F, -3.0F, 1.0F, 1.0F, 1.0F, -0.25F, false);

		layer = new ModelRenderer(this);
		layer.setRotationPoint(-10.5F, 31.0F, 2.5F);
		lid.addChild(layer);
		layer.setTextureOffset(20, 7).addBox(7.5F, -33.0F, -3.0F, 3.0F, 4.0F, 1.0F, -0.3F, false);

		bump = new ModelRenderer(this);
		bump.setRotationPoint(0.0F, 0.0F, 0.0F);
		layer.addChild(bump);
		bump.setTextureOffset(20, 13).addBox(7.75F, -31.825F, -3.0F, 1.0F, 1.0F, 1.0F, -0.25F, false);

		straps = new ModelRenderer(this);
		straps.setRotationPoint(0.0F, 0.0F, 0.0F);
		vm.addChild(straps);
		straps.setTextureOffset(0, 8).addBox(1.5F, -1.0F, -2.5F, 1.0F, 2.0F, 5.0F, -0.25F, false);
		straps.setTextureOffset(0, 8).addBox(-2.5F, -1.0F, -2.5F, 1.0F, 2.0F, 5.0F, -0.25F, false);
		straps.setTextureOffset(1, 17).addBox(-2.0F, -1.0F, 1.5F, 4.0F, 2.0F, 1.0F, -0.25F, false);

		body = new ModelRenderer(this);
		body.setRotationPoint(0.0F, 0.0F, -2.0F);
		vm.addChild(body);
		

		module = new ModelRenderer(this);
		module.setRotationPoint(-9.0F, 31.0F, 2.0F);
		body.addChild(module);
		module.setTextureOffset(0, 0).addBox(7.0F, -33.5F, -2.5F, 4.0F, 5.0F, 1.0F, -0.25F, false);

		outline = new ModelRenderer(this);
		outline.setRotationPoint(0.0F, 0.0F, 0.0F);
		module.addChild(outline);
		outline.setTextureOffset(11, 0).addBox(7.5F, -33.0F, -2.75F, 3.0F, 4.0F, 1.0F, -0.35F, false);

		studs = new ModelRenderer(this);
		studs.setRotationPoint(-9.0F, 31.0F, 2.0F);
		body.addChild(studs);
		studs.setTextureOffset(0, 22).addBox(7.25F, -29.75F, -2.65F, 1.0F, 1.0F, 1.0F, -0.25F, false);
		studs.setTextureOffset(0, 22).addBox(7.25F, -33.25F, -2.65F, 1.0F, 1.0F, 1.0F, -0.25F, false);
		studs.setTextureOffset(0, 22).addBox(9.75F, -33.25F, -2.65F, 1.0F, 1.0F, 1.0F, -0.25F, false);
		studs.setTextureOffset(0, 22).addBox(9.75F, -29.75F, -2.65F, 1.0F, 1.0F, 1.0F, -0.25F, false);
	}

	@Override
	public void setRotationAngles(LivingEntity entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch){
		//previously the render function, render code was moved to a method below
	}

	@Override
	public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha){
		bipedLeftArm.render(matrixStack, buffer, packedLight, packedOverlay);
	}

	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
	
	public ModelRenderer getArmRoot() {
		return this.bipedLeftArm;
	}
	
	public ModelRenderer getLid() {
		return this.lid;
	}
	
	public ModelRenderer getVMRoot() {
		return this.vm;
	}
}