package net.tardis.mod.client.renderers.boti;

import org.lwjgl.opengl.GL11;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.MainWindow;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.WorldRenderer;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.client.renderer.vertex.VertexBuffer;
import net.minecraft.client.renderer.vertex.VertexFormat;
import net.minecraft.client.shader.Framebuffer;
import net.minecraft.util.math.vector.Matrix4f;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.tardis.mod.boti.BotiWorld;


public class BotiManager {
	
	private static VertexFormat FORMAT = DefaultVertexFormats.BLOCK;
	private VertexBuffer solidVBO;
	private VertexBuffer translucentVBO;
	public Framebuffer fbo;
	public WorldRenderer worldRenderer;
	public BotiWorld world;
	
	public void setupVBO() {
		if(this.solidVBO == null)
			solidVBO = new VertexBuffer(FORMAT);
		solidVBO.bindBuffer();
	}
	
	public void setupTranslucentVBO() {
		if(this.translucentVBO == null)
			this.translucentVBO = new VertexBuffer(FORMAT);
		this.translucentVBO.bindBuffer();
	}
	
	public void drawData(VertexBuffer buffer, Matrix4f matrixIn) {
		GlStateManager.vertexPointer(3, GL11.GL_FLOAT, FORMAT.getSize(), 0);
		GlStateManager.colorPointer(4, GL11.GL_UNSIGNED_BYTE, FORMAT.getSize(), FORMAT.getOffset(0));
		GlStateManager.texCoordPointer(2, GL11.GL_FLOAT, FORMAT.getSize(), FORMAT.getOffset(0));
		
		GlStateManager.enableClientState(GL11.GL_VERTEX_ARRAY);
		GlStateManager.enableClientState(GL11.GL_TEXTURE_COORD_ARRAY);
		GlStateManager.enableClientState(GL11.GL_COLOR_ARRAY);
		buffer.draw(matrixIn, GL11.GL_QUADS);
        GlStateManager.disableClientState(GL11.GL_VERTEX_ARRAY);
        GlStateManager.disableClientState(GL11.GL_TEXTURE_COORD_ARRAY);
        GlStateManager.disableClientState(GL11.GL_COLOR_ARRAY);
	}
	
	public void writeBufferData(BufferBuilder data, VertexBuffer buffer) {
		data.finishDrawing();
		//data.reset();
		buffer.upload(data);
	}
	
	public void endVBO() {
		VertexBuffer.unbindBuffer();
	}
	
	public void setupFramebuffer() {
		
		MainWindow res = Minecraft.getInstance().getMainWindow();
		
		if(fbo != null && (fbo.framebufferWidth != res.getFramebufferWidth() || fbo.framebufferHeight != res.getFramebufferHeight()))
			fbo = null;
		
		if(fbo == null) {
			fbo = new Framebuffer(Minecraft.getInstance().getMainWindow().getFramebufferWidth(), Minecraft.getInstance().getMainWindow().getFramebufferHeight(), true, Minecraft.IS_RUNNING_ON_MAC);
		}
		
		fbo.bindFramebuffer(false);
		fbo.checkFramebufferComplete();
		
		if(!fbo.isStencilEnabled())
			fbo.enableStencil();
		
	}
	
	public void endFBO() {
		fbo.unbindFramebuffer();
		fbo.framebufferClear(Minecraft.IS_RUNNING_ON_MAC);
	}
	
	

}
