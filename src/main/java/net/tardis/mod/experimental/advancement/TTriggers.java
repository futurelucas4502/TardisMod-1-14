package net.tardis.mod.experimental.advancement;

import net.minecraft.advancements.CriteriaTriggers;

/**
 * Created by Swirtzly
 * on 19/04/2020 @ 12:33
 */
//Now now, let's not get triggered
public class TTriggers {

    public static final TriggerBase OBTAINED = new TriggerBase("obtain_tardis");
    public static final TriggerBase EXTERIOR_UNLOCK = new TriggerBase("exterior_unlock");
    public static final TriggerBase INTERIOR_UNLOCK = new TriggerBase("interior_unlock");
    public static final TriggerBase TARDIS_MOD = new TriggerBase("tardis_mod");
    public static final TriggerBase ENVOY = new TriggerBase("envoy");
    public static final TriggerBase PANAMAX = new TriggerBase("panamax");

    public static void init() {
        CriteriaTriggers.register(OBTAINED);
        CriteriaTriggers.register(TARDIS_MOD);
        CriteriaTriggers.register(ENVOY);
        CriteriaTriggers.register(EXTERIOR_UNLOCK);
        CriteriaTriggers.register(INTERIOR_UNLOCK);
        CriteriaTriggers.register(PANAMAX);
    }

}
