package net.tardis.mod.helper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.google.common.collect.Lists;

import it.unimi.dsi.fastutil.longs.LongSet;
import net.minecraft.block.BlockState;
import net.minecraft.block.IWaterLoggable;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.item.minecart.MinecartEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.entity.projectile.FishingBobberEntity;
import net.minecraft.fluid.FluidState;
import net.minecraft.fluid.Fluids;
import net.minecraft.network.play.server.SPlayEntityEffectPacket;
import net.minecraft.network.play.server.SPlayerAbilitiesPacket;
import net.minecraft.network.play.server.SServerDifficultyPacket;
import net.minecraft.network.play.server.SSetExperiencePacket;
import net.minecraft.potion.Effect;
import net.minecraft.potion.EffectInstance;
import net.minecraft.server.MinecraftServer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.RegistryKey;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.util.math.vector.Vector3i;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.DimensionType;
import net.minecraft.world.ForcedChunksSaveData;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorldReader;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.gen.feature.StructureFeature;
import net.minecraft.world.server.ServerWorld;
import net.minecraft.world.server.TicketType;
import net.minecraft.world.storage.IWorldInfo;
import net.minecraftforge.common.world.ForgeChunkManager;
import net.minecraftforge.common.world.ForgeChunkManager.TicketOwner;
import net.minecraftforge.fml.server.ServerLifecycleHooks;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.Tardis;
import net.tardis.mod.config.TConfig;
import net.tardis.mod.world.dimensions.TDimensions;
/** Helper functions for everything world related*/
public class WorldHelper {
	
	public static String formatBlockPos(BlockPos pos) {
		return pos.getX() + ", " + pos.getY() + ", " + pos.getZ();
	}
	/** Formats a BlockPos for use in quick Copy Pasting*/
	public static String formatBlockPosDebug(BlockPos pos) {
        return pos.getX() + " " + pos.getY() + " " + pos.getZ();
    }
    /** <br> 1.16: Use RegistryKey<World> because there can be multiple dimensions for one dimension type*/
	public static String formatDimName(RegistryKey<World> dim) {
		if(dim == null)
			return "UNKNOWN";

		String original = dim.getLocation().getPath().trim().replace("	", "").replace("_", " ");
		String output = Arrays.stream(original.split("\\s+"))
				.map(t -> t.substring(0,1).toUpperCase() + t.substring(1))
				.collect(Collectors.joining(" "));

		return output;
	}
	/**
	 * Formats a Biome's RegistryKey to show a user friendly name
	 * <br> Temporary solution as Biome#getDisplayName is gone in 1.16
	 * @param biomeKey
	 * @return
	 */
	public static String formatBiomeKey(RegistryKey<Biome> biomeKey) {
		if(biomeKey == null)
			return "UNKNOWN";

		String original = biomeKey.getLocation().getPath().trim().replace("	", "").replace("_", " ");
		String output = Arrays.stream(original.split("\\s+"))
				.map(t -> t.substring(0,1).toUpperCase() + t.substring(1))
				.collect(Collectors.joining(" "));
		return output;
	}
	
	/**
	 * Formats a Structure's RegistryKey to show a user friendly name
	 * <br> DO NOT use Structure#getStructureName as vanilla uses that as a pseudo key sometimes
	 * @param biomeKey
	 * @return
	 */
	public static String formatStructureKey(RegistryKey<StructureFeature<?, ?>> structureKey) {
		if(structureKey == null)
			return "UNKNOWN";

		String original = structureKey.getLocation().getPath().trim().replace("	", "").replace("_", " ");
		String output = Arrays.stream(original.split("\\s+"))
				.map(t -> t.substring(0,1).toUpperCase() + t.substring(1))
				.collect(Collectors.joining(" "));
		return output;
	}
	
	public static String formatStructureKey(String structureKey) {
		if(structureKey == null)
			return "UNKNOWN";
        ResourceLocation parsedRL = new ResourceLocation(structureKey);
		String original = parsedRL.getPath().trim().replace("	", "").replace("_", " ");
		String output = Arrays.stream(original.split("\\s+"))
				.map(t -> t.substring(0,1).toUpperCase() + t.substring(1))
				.collect(Collectors.joining(" "));
		return output;
	}
	
	
	
	public static Effect getEffectFromRL(ResourceLocation loc) {
		return ForgeRegistries.POTIONS.getValue(loc);
	}
	
	public static ResourceLocation getKeyFromEffect(Effect effect) {
		return ForgeRegistries.POTIONS.getKey(effect);
	}
	
	public static void setFluidStateIfNot(FluidState state, World world, BlockPos pos) {
		BlockState currentState = world.getBlockState(pos);
		boolean isEmpty = state.getFluid() == Fluids.EMPTY;
		if(world.getFluidState(pos).getFluid() != state.getFluid()) {
			if(currentState.isAir(world, pos) || (isEmpty && currentState.getMaterial().isLiquid())) {
				world.setBlockState(pos, state.getBlockState());
			}
			else if(world.getBlockState(pos).getBlock() instanceof IWaterLoggable) {
				IWaterLoggable wat = (IWaterLoggable)currentState.getBlock();
				if(wat.canContainFluid(world, pos, currentState, state.getFluid()))
					wat.receiveFluid(world, pos, currentState, state.getFluidState());
				else if(state.getFluid() == Fluids.EMPTY)
					wat.pickupFluid(world, pos, currentState);
			}
		}
	}
	
	public static float getAngleBetweenPoints(Vector3d start, Vector3d end) {
		float rot = 0F;
		
		Vector3d p = start.subtract(end).normalize();
		
		float hype = (float)Math.sqrt(p.x * p.x + p.z * p.z);
		
		if(p.z < 0)
			rot = (float)Math.toDegrees(Math.asin(p.x / hype));
		else rot = -(float)Math.toDegrees(Math.asin(p.x / hype)) - 180;
		
		return rot;
	}
	
	/**
	 * Converts from Direction to Rotation, assuming the default facing is north
	 * @param dir
	 * @return
	 */
	public static Rotation getRotationFromDirection(Direction dir) {
		switch(dir) {
		case NORTH: return Rotation.NONE;
		case EAST: return Rotation.CLOCKWISE_90;
		case SOUTH: return Rotation.CLOCKWISE_180;
		case WEST: return Rotation.COUNTERCLOCKWISE_90;
		default: return Rotation.NONE;
		}
	}
	
	/**
	 * Only supports Horrizontal directions, rotates around 0, 0, 0 
	 * @param pos - The position to rotate
	 * @param dir - The direction to rotate it, assumes facing north by default
	 * @return - rotated block pos
	 */
	public static BlockPos rotateBlockPos(BlockPos pos, Direction dir) {
		switch(dir) {
			case NORTH: return pos;
			case EAST: return new BlockPos(-pos.getZ(), pos.getY(), pos.getX());
			case SOUTH: return new BlockPos(-pos.getX(), pos.getY(), -pos.getZ());
			case WEST: return new BlockPos(pos.getZ(), pos.getY(), -pos.getX());
			default: return pos;
		}
	}

	public static Vector3d vecFromPos(Vector3i location) {
		return new Vector3d(location.getX() + 0.5, location.getY() + 0.5, location.getZ() + 0.5);
	}
	
	public static float getFixedRotation(float rot) {
		float newR = rot % 360.0F;
		if(newR < 0)
			return 360.0F + newR;
		
		return newR;
	}

	public static BlockPos scaleBlockPos(BlockPos pos, double scale) {
		return new BlockPos(pos.getX() * scale, pos.getY() * scale, pos.getZ() * scale);
	}
	
	public static float getAngleFromFacing(Direction dir) {
		if(dir == Direction.NORTH)
			return 0.0F;
		else if(dir == Direction.EAST)
			return 90.0F;
		else if(dir == Direction.SOUTH)
			return 180.0F;
		return 270.0F;
	}
	
	public static float getRotationDifference(float one, float two) {
		if(one > two)
			return one - two;
		return two - one;
	}
	
	public static RegistryKey<World> getWorldKeyFromRL(ResourceLocation loc){
		return RegistryKey.getOrCreateKey(Registry.WORLD_KEY, loc);
	}
	
	public static ServerWorld getWorldFromRL(MinecraftServer server, ResourceLocation loc){
		return server.getWorld(RegistryKey.getOrCreateKey(Registry.WORLD_KEY, loc));
	}
	
	/** <br> 1.16: Compare RegistryKey<World> because there can be multiple dimensions for one dimension type */
	public static ResourceLocation getKeyFromWorld(World world) {
		ResourceLocation rl = world.getDimensionKey().getLocation();
		return rl != null ? rl : new ResourceLocation("overworld");
	}
	
	public static List<TileEntity> getTEsInChunks(ServerWorld world, ChunkPos chunkPos, int i) {
		List<TileEntity> list = Lists.newArrayList();
		for(int x = -i; x <= i; ++x) {
			for(int z = -i; z <= i; ++z) {
				list.addAll(world.getChunk(chunkPos.x + x, chunkPos.z + z).getTileEntityMap().values());
			}
		}
		return list;
	}
	
	/**
     * Get a DimensionType at runtime
     * <br> Used for dynamically created Dimensions
     * @param server
     * @param dimTypeKey
     * @return
     */
    public static DimensionType getDimensionType(MinecraftServer server, RegistryKey<DimensionType> dimTypeKey){
		return server.getDynamicRegistries() // get dynamic registries
			.getRegistry(Registry.DIMENSION_TYPE_KEY)
			.getOrThrow(dimTypeKey);
	}
    
    public static DimensionType getDimensionType(ServerWorld world, RegistryKey<DimensionType> dimTypeKey){
		return world.getServer().getDynamicRegistries() // get dynamic registries
			.getRegistry(Registry.DIMENSION_TYPE_KEY)
			.getOrThrow(dimTypeKey);
	}
    
    /**
     * Get a Dimension Type world from a world
     * <br> For Client Worlds, use the method {@link ClientWorldHelper#getDimensionType(net.minecraft.client.world.ClientWorld, RegistryKey)}
     * @param world
     * @param dimTypeKey
     * @return
     */
    public static DimensionType getDimensionType(World world, RegistryKey<DimensionType> dimTypeKey){
		return world.func_241828_r() // get dynamic registries
			.getRegistry(Registry.DIMENSION_TYPE_KEY)
			.getOrThrow(dimTypeKey);
	}
    
    /**
     * Compares the current world's dimension type against a registry key of a json based dimension type
     * @param world
     * @param dimTypeKey
     * @return True if the registry keys match, false if they do not
     */
    public static boolean areDimensionTypesSame(World world, RegistryKey<DimensionType> dimTypeKey) {
        DimensionType type = world.func_241828_r().getRegistry(Registry.DIMENSION_TYPE_KEY).getValueForKey(dimTypeKey);
        if(type != null && world.getDimensionType().isSame(type))
        	return true;
        return false;
    }
    
	/**
	 * Used to determine if an object is able to travel to the target dimension
	 * <br> 1.16: Compare RegistryKey<World> because there can be multiple dimensions for one dimension type
	 * @implNote Usage: Config defined values
	 * @implSpec Will blacklist dimensions
	 * @param dimension
	 * @return Boolean value
	 */

	public static boolean canTravelToDimension(World world) {
		ResourceLocation key = world.getDimensionKey().getLocation();
		if(key == null)
			return false;
		List<? extends String> blacklist = TConfig.SERVER.blacklistedDims.get();
		for(String s : blacklist) {
			if(key.toString().contentEquals(s))
				return false;
		}
        return !WorldHelper.areDimensionTypesSame(world, TDimensions.DimensionTypes.TARDIS_TYPE)
        		&& !WorldHelper.areDimensionTypesSame(world, TDimensions.DimensionTypes.VORTEX_TYPE);
	}
	
	/**
	 * Used to determine if the VM is able to travel to the target dimension
	 * <br> 1.16: Compare RegistryKey<World> because there can be multiple dimensions for one dimension type
	 * @implNote Usage: Config defined values
	 * @implSpec Will require a config option to blacklist dimensions
	 * @param dimension
	 * @return Boolean value
	 */
	public static boolean canVMTravelToDimension(World world) {
		ResourceLocation key = world.getDimensionKey().getLocation();
		if(key == null)
			return false;
		if (TConfig.SERVER.toggleVMWhitelistDims.get()) { //If using whitelist
			return TConfig.SERVER.whitelistedVMDims.get().stream().allMatch(dimName ->
		         key.toString().contentEquals(dimName) && canVMBeUsedInDim(world));
		} //uses anyMatch to allow for VM to not be able be used in ANY player's tardis dimension, because every player's tardis name contains uuid which is different
		else if (!TConfig.SERVER.toggleVMWhitelistDims.get()){ //If using blacklist
			return TConfig.SERVER.blacklistedVMDims.get().stream().allMatch(dimName ->
		         !key.toString().contentEquals(dimName) && canVMBeUsedInDim(world));
		}
		return canVMBeUsedInDim(world);
	}
	/**
	 * Determines if VM can be opened/used in a particular dimension. More lightweight method than the above
	 * <br> 1.16: Compare RegistryKey<World> because there can be multiple dimensions for one dimension type
	 * @param dimension
	 * @return true if can be used, false if cannot be used
	 */
	public static boolean canVMBeUsedInDim(World world) {
		return !WorldHelper.areDimensionTypesSame(world, TDimensions.DimensionTypes.TARDIS_TYPE)
		&& !WorldHelper.areDimensionTypesSame(world, TDimensions.DimensionTypes.VORTEX_TYPE);
	}
	
	/**
	 * A more performance efficient version of the above to check if an object can be used in the current dimension
	 * <br> 1.16: Compare RegistryKey<World> because there can be multiple dimensions for one dimension type
	 * @implNote Used for items/blocks that can only be used in the Tardis dimension
	 * @param world
	 * @return true if blocked, false if not blocked
	 */
	public static boolean isDimensionBlocked(World world) {
		return !WorldHelper.areDimensionTypesSame(world, TDimensions.DimensionTypes.TARDIS_TYPE);
	}
    
    public static List<World> getAllValidDimensions() {
		
		List<World> dims = new ArrayList<World>();
		
		for(ServerWorld dimension : ServerLifecycleHooks.getCurrentServer().getWorlds()) {
			if(WorldHelper.canTravelToDimension(dimension))
				dims.add(dimension);
		}
		
		return dims;
	}
    
    /**
     * Force loads and starts ticking a chunk if it is not loaded. 
     * <p> Also marks the chunk dirty to be saved after the chunk is forced
     * @param world
     * @param chunkPos
     * @param blockPos
     * @return
     */
    public static boolean forceChunkIfNotLoaded(ServerWorld world, ChunkPos chunkPos, BlockPos blockPos) {
    	if (ForgeChunkManager.forceChunk(world, Tardis.MODID, blockPos, chunkPos.x, chunkPos.z, true, true)) { //enable the chunk to receive full ticks
	        WorldHelper.markChunkDirty(world, blockPos);
	        return true;
    	}
        return false;
    }
    /**
     * Removes the force loading of a ticking chunk if it is loaded
     * <p> Also marks the chunk dirty to be saved after the chunk is unforced
     * <br> Call to disable chunks loaded by {@link WorldHelper#forceChunkIfNotLoaded(ServerWorld, ChunkPos, BlockPos)}
     * @param world
     * @param chunkPos
     * @param blockPos
     * @return
     */
    public static boolean unForceChunkIfLoaded(ServerWorld world, ChunkPos chunkPos, BlockPos blockPos) {
		if (ForgeChunkManager.forceChunk(world, Tardis.MODID, blockPos, chunkPos.x, chunkPos.z, false, true)) { //The ticking parameter needs to be true so that we are unloading the same type of chunk (ticking chunks) as the ones we loaded with WorldHelper#forceChunkIfNotLoaded
	        WorldHelper.markChunkDirty(world, blockPos);
	        return true;
		}
		return false;
    }
    
	/**
     * Checks if a position is in bounds of the world, and is loaded
     *
     * @param world world
     * @param pos   position
     *
     * @return True if the position is loaded or the given world is of a superclass of IWorldReader that does not have a concept of being loaded.
     */
 	public static boolean isBlockLoaded(@Nullable IBlockReader world, @Nonnull BlockPos pos) {
         if (world == null || !World.isValid(pos)) {
             return false;
         } else if (world instanceof IWorldReader) {
             //Note: We don't bother checking if it is a world and then isBlockPresent because
             // all that does is also validate the y value is in bounds, and we already check to make
             // sure the position is valid both in the y and xz directions
             return ((IWorldReader) world).isBlockLoaded(pos);
         }
         return true;
     }
 	/** Marks a chunk as dirty if it is currently loaded. Does validation if the position is within a world, whilst vanilla does not
 	 * <p> Call this after force loading a chunk to ensure the world saves that action*/
 	public static void markChunkDirty(World world, BlockPos pos) {
         if (isBlockLoaded(world, pos)) {
             world.getChunkAt(pos).markDirty();
         }
 	}
 	
 	/** Gets the map of force loaded chunks created at a block position, whose chunks are current ticking. 
 	 * <p> Used to check if a ticking chunk should be force loaded or not
 	 * <p> We do not use {@link ServerWorld#getForcedChunks()} because that only gets the non-ticking forced chunks*/
 	public static Map<TicketOwner<BlockPos>, LongSet> getTickingBlockForcedChunks(ServerWorld serverWorld){
 		ForcedChunksSaveData data = serverWorld.getSavedData().getOrCreate(ForcedChunksSaveData::new, "chunks");
 		if (data != null)
 			return data.getBlockForcedChunks().getTickingChunks();
 		return null;
 	}
 	/**
 	 * Force Loads or Force Unloads a set amount of chunks in a Tardis dimension
 	 * <br> Used to allow Console to find the door, or unload the chunks that were forced by the Console
 	 * @param targetWorld
 	 * @param force, if true, force load chunks. If false, unloads chunks
 	 * @return
 	 */
 	public static boolean preLoadTardisInteriorChunks(ServerWorld targetWorld, boolean force) {
 		if (WorldHelper.areDimensionTypesSame(targetWorld, TDimensions.DimensionTypes.TARDIS_TYPE)) {
			ServerWorld serverWorld = targetWorld.getServer().getWorld(targetWorld.getDimensionKey());
            ChunkPos cPos = new ChunkPos(TardisHelper.TARDIS_POS);
			for (int xPos = -3; xPos < 3; xPos ++) {
				for (int zPos = -3; zPos < 3; zPos ++) {
					ChunkPos toLoadPos = new ChunkPos(cPos.x + xPos, cPos.z + zPos);
					if (force)
						forceChunkIfNotLoaded(serverWorld, toLoadPos, TardisHelper.TARDIS_POS);
					else
						unForceChunkIfLoaded(serverWorld, toLoadPos, TardisHelper.TARDIS_POS);
				}
			}
			return true;
		}
 		return false;
 	}
 	
 	public static void teleportEntities(Entity e, ServerWorld world, double x, double y, double z, float yaw, float pitch) {
		if(e instanceof ServerPlayerEntity) {
			ServerPlayerEntity player = (ServerPlayerEntity)e;
			ChunkPos chunkpos = new ChunkPos(new BlockPos(x, y, z));
			world.getChunkProvider().registerTicket(TicketType.POST_TELEPORT, chunkpos, 1, player.getEntityId());
			//Force load the target world if it's a Tardis dimension.
			//Handles cases where player didn't right click exterior and tries to enter (exterior force loads chunks if right clicked)
			//If this was not done, if the console wasn't loaded the door cannot be found
			preLoadTardisInteriorChunks(world.getWorldServer(), true); 
			if(player.getEntityWorld() == world) {
				player.connection.setPlayerLocation(x, y, z, yaw, pitch);
			}
			if (player.isSleeping()) {
				player.wakeUp();
			}
			else {
				player.teleport(world, x, y, z, yaw, pitch);
				//Handle player active potion effects
				for(EffectInstance effectinstance : player.getActivePotionEffects()) {
					player.connection.sendPacket(new SPlayEntityEffectPacket(player.getEntityId(), effectinstance));
		         }
				//Handle player experience not getting received on client after interdimensional teleport
				IWorldInfo worldinfo = player.world.getWorldInfo();
				player.connection.sendPacket(new SSetExperiencePacket(player.experience, player.experienceTotal, player.experienceLevel));
				player.connection.sendPacket(new SPlayerAbilitiesPacket(player.abilities));//Keep abilities like creative mode flight height etc.
                player.connection.sendPacket(new SServerDifficultyPacket(worldinfo.getDifficulty(), worldinfo.isDifficultyLocked()));
			}
		}
		else {
			//In in same dimension
			if(e.world == world) {
				e.setPosition(x, y, z);
				e.rotationYaw = yaw;
				e.rotationPitch = pitch;
			}
			else {
				//If interdimensional
				ServerWorld targetDim = world;
				e.setWorld(targetDim);
				if (e != null && e.getType() != ForgeRegistries.ENTITIES.getValue(EntityType.ENDER_DRAGON.getRegistryName())) { //Prevent Ender dragon from teleporting inside the interior and destroying it
					Entity old = e;
					final ServerWorld sourceWorld = (ServerWorld)old.getEntityWorld();
					old.detach(); //Dismounts from entities and dismounts other entities riding the entity
					Entity newE = e.getType().create(world);
					if (old instanceof FishingBobberEntity) { //Explicit handling of fishing rod because it doesn't have an entity type, hence it will cause an NPE
						FishingBobberEntity bobber = (FishingBobberEntity)old;
						old.remove(true);
						ServerPlayerEntity player = (ServerPlayerEntity)bobber.func_234606_i_(); //bobber#getAngler
						newE = new FishingBobberEntity(player, world, 0, 0); //Use serverside constructor. Fix critical serverside stackoverflow issue caused when fishing bobber is spawn on the exterior door
					}
					//Handle Minecarts
					if (old.isAlive() && old instanceof MinecartEntity) {
						old.remove(true); //equivalent of old.removed = true;
						old.changeDimension(targetDim);
						old.revive(); //equivalent of old.removed = false;
			        }
					if(old.world instanceof ServerWorld) {
						sourceWorld.removeEntity(old);
					}
					if (newE != null) { //Sanity check
						newE.copyDataFromOld(old);
						newE.setLocationAndAngles(x, y, z, yaw, pitch);
						world.addFromAnotherDimension(newE);
						final boolean flag = newE.forceSpawn;
				        newE.forceSpawn = true;
				        world.addEntity(newE);
				        newE.forceSpawn = flag;
				        world.updateEntity(newE);
					}
					else {
						System.err.println("Error teleporting entity: " + old);
						System.err.println("Entity that was teleported: " + newE);
					}
				}
			}
			//Handle Elytra flying
			if (!(e instanceof LivingEntity) || !((LivingEntity)e).isElytraFlying()) {
		        e.setMotion(e.getMotion().mul(1.0D, 0.0D, 1.0D));
		        e.setOnGround(true);
		    }
		}
	}
    /** Uses vanilla force chunk system.*/
 	public static boolean forceChunkIfNot(ServerWorld world, ChunkPos pos) {
		if(!world.getForcedChunks().contains(pos.asLong())) {
			world.forceChunk(pos.x, pos.z, true);
			return true;
		}
		return false;
	}

}
