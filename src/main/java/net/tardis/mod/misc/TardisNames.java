package net.tardis.mod.misc;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Random;

import org.apache.logging.log4j.Level;

import com.google.common.collect.Lists;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import net.minecraft.resources.IResourceManager;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
import net.tardis.mod.helper.Helper;

public class TardisNames {
	
	public static final ResourceLocation NAMES = Helper.createRL("tardis_names.json");
	
	public static List<Part> PARTS = Lists.newArrayList();
	
	public static void read(IResourceManager manager) {
		
		PARTS.clear();
		
		try {
			
			InputStream is = manager.getResource(NAMES).getInputStream();
			JsonElement root = new JsonParser().parse(new InputStreamReader(is));
			
			JsonArray columnArray = root.getAsJsonArray();
			for(JsonElement ele : columnArray) {
				PARTS.add(new Part(ele.getAsJsonArray()));
			}
			
		}
		catch(IOException e) {
			Tardis.LOGGER.log(Level.ERROR, "Error in tardis_names.json!");
			Tardis.LOGGER.catching(e);
		}
	}
	
	public static String getRandomName(Random rand) {
		StringBuilder name = new StringBuilder();
		for(Part part : PARTS) {
			name.append(part.getRandomPart(rand)).append(" ");
		}
		return name.toString().trim();
	}
	
	public static class Part{
		
		List<String> names = Lists.newArrayList();
		
		public Part(JsonArray array) {
			for(JsonElement ele : array) {
				names.add(ele.getAsString());
			}
		}
		
		public String getRandomPart(Random rand) {
			return this.names.get(rand.nextInt(this.names.size()));
		}
	}

}
