package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.misc.vm.AbstractVortexMFunction;
import net.tardis.mod.misc.vm.VortexMUseType;
import net.tardis.mod.registries.VortexMFunctions;


public class VMFunctionMessage {
	
	public ResourceLocation function;
	public VortexMUseType useType;
	
	public VMFunctionMessage(ResourceLocation function, VortexMUseType useType) {
		this.function = function;
		this.useType = useType;
	}
	
	public static VMFunctionMessage decode(PacketBuffer buf) {
		return new VMFunctionMessage(buf.readResourceLocation(),buf.readEnumValue(VortexMUseType.class));
	}
	
	public static void encode(VMFunctionMessage mes, PacketBuffer buf) {
		buf.writeResourceLocation(mes.function);
		buf.writeEnumValue(mes.useType);
	}

	public static void handle(VMFunctionMessage mes, Supplier<NetworkEvent.Context> ctx) {
		ctx.get().enqueueWork(() -> {
			ServerWorld world = ctx.get().getSender().getServerWorld();
			AbstractVortexMFunction func = VortexMFunctions.FUNCTIONS_REGISTRY.get().getValue(mes.function);
			if (mes.useType == VortexMUseType.SERVER || mes.useType == VortexMUseType.BOTH && func != null)
				func.sendActionOnServer(world, ctx.get().getSender());
		});
		ctx.get().setPacketHandled(true);
	}
}
