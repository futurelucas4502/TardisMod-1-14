package net.tardis.mod.subsystem;

import net.minecraft.item.Item;
import net.minecraft.nbt.CompoundNBT;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.console.misc.SparkingLevel;
/** Protects other subsystems from explosion damage to the exterior, prevents getting sucked out whilst in space*/
public class ShieldGeneratorSubsystem extends Subsystem{
	
	private boolean isForceFieldActivated;
	
	public ShieldGeneratorSubsystem(ConsoleTile console, Item item) {
		super(console, item);
	}

	@Override
	public void onTakeoff() {}

	@Override
	public void onLand() {}

	@Override
	public void onFlightSecond() {}

	@Override
	public boolean stopsFlight() {
		return false;
	}

	@Override
	public SparkingLevel getSparkState() {
		return SparkingLevel.NONE;
	}
	
	
	
	@Override
	public CompoundNBT serializeNBT() {
		CompoundNBT tag = super.serializeNBT();
		tag.putBoolean("is_forcefield_activated", this.isForceFieldActivated);
		return tag;
	}

	@Override
	public void deserializeNBT(CompoundNBT tag) {
		super.deserializeNBT(tag);
		this.isForceFieldActivated = tag.getBoolean("is_forcefield_activated");
	}

	/** Flag to tell if the forcefield is activated.
	 * <p> This is needed because currently the shield is directly linked to the forcefield protocol
	 * This means whenever we activate the shield generator we are also activating the forcefield
	 * <p> In 1.14, the forcefield and shield generator were disconnected because the shield passively did other things like protect subsystems from damage
	 * <br> We want to preserve the old "disconnected style" logic so we will seperate the forcefield from the shield like before.
	 * */
	public boolean isForceFieldActivated() {
		return this.isForceFieldActivated;
	}
	
	/**
	 * Set the forcefield protocol to be activated
	 * @param shouldActivate
	 */
	public void setForceFieldActivated(boolean shouldActivate) {
		this.isForceFieldActivated = shouldActivate;
	}
}
