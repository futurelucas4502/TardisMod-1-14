package net.tardis.mod.subsystem;

import net.minecraft.item.Item;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.registries.ForgeRegistries;

public class SubsystemInfo implements INBTSerializable<CompoundNBT>{

	public float health = 0;
	public String translationKey;
	public Item key;
	
	public SubsystemInfo(Subsystem sub) {
		if(sub.getItem().getMaxDamage() > 0)
			this.health = sub.getHealth();
		this.translationKey = sub.getItemKey().getTranslationKey();
		this.key = sub.getItemKey();
		
	}
	
	public SubsystemInfo(CompoundNBT nbt) {
		this.deserializeNBT(nbt);
	}

	@Override
	public CompoundNBT serializeNBT() {
		CompoundNBT tag = new CompoundNBT();
		tag.putFloat("health", health);
		tag.putString("translation", this.translationKey);
		tag.putString("reg_key", this.key.getRegistryName().toString());
		return tag;
	}

	@Override
	public void deserializeNBT(CompoundNBT tag) {
		this.health = tag.getFloat("health");
		this.translationKey = tag.getString("translation");
		this.key = ForgeRegistries.ITEMS.getValue(new ResourceLocation(tag.getString("reg_key")));
	}
	
}
