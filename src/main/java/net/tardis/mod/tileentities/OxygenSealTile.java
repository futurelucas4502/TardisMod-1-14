package net.tardis.mod.tileentities;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.tardis.api.space.cap.IOxygenSealer;
import net.tardis.api.space.cap.SpaceCapabilities;
import net.tardis.api.space.thread.OxygenThread;


public class OxygenSealTile extends TileEntity implements ITickableTileEntity, IOxygenSealer{

	private OxygenThread sealerThread;
	private List<BlockPos> sealedPoses = new ArrayList<>();
	private int lastChecked = 0;
	private LazyOptional<IOxygenSealer> oxygenSupplier;
	
	public OxygenSealTile(TileEntityType<?> tileEntityTypeIn) {
		super(tileEntityTypeIn);
		this.oxygenSupplier = LazyOptional.of(() -> this);
	}
	
	public OxygenSealTile() {
		this(TTiles.OXYGEN_SEALER.get());
	}

	@Override
	public void tick() {
		if(!world.isRemote) {
			if(this.lastChecked > 0)
				--this.lastChecked;
			else this.findsealedPos();
		}
	}
	
	public void findsealedPos() {
		if(!world.isRemote) {
			if(this.sealerThread == null || !this.sealerThread.isAlive()) {
				
				this.sealerThread = new OxygenThread(world, this.getPos(), this, 2700);
				this.sealerThread.start();
			}
		}
		this.lastChecked = 100;
	}
	
	@Override
	public void setSealedPositions(List<BlockPos> list) {
		this.sealedPoses.clear();
		this.sealedPoses.addAll(list);
		this.lastChecked = 100;
	}
	
	@Override
	public List<BlockPos> getSealedPositions() {
		return this.sealedPoses;
	}

	
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> cap, Direction side) {
		return cap == SpaceCapabilities.OXYGEN_SEALER ? (LazyOptional<T>)this.oxygenSupplier : super.getCapability(cap, side);
	}
	
}
