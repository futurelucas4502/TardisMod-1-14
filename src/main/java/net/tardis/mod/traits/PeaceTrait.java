package net.tardis.mod.traits;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.console.misc.EmotionHandler.EnumHappyState;

public class PeaceTrait extends AbstractEntityDeathTrait{

	public PeaceTrait(TardisTraitType type) {
		super(type);
	}

	@Override
	public void tick(ConsoleTile tile) {}

	@Override
	public void onOwnerKilledEntity(PlayerEntity owner, ConsoleTile tardis, LivingEntity victim) {
		double mod = 1.0 - this.getWeight();
		if(tardis.getEmotionHandler().getMood() < EnumHappyState.SAD.getTreshold()) {
			tardis.getEmotionHandler().addLoyalty(owner, -(3 + (int)Math.ceil(3 * mod)));
			tardis.getEmotionHandler().addMood(-(7 + (int)Math.ceil(7 * mod)));
			this.warnPlayer(tardis, TardisTrait.GENERIC_DISLIKE);
		}
	}

}
