package net.tardis.mod.world.dimensions;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.util.math.vector.Vector3d;
import net.tardis.api.space.cap.ISpaceDimProperties;

/**
 * Legacy code for the 1.14 Space Dimension. Remains as a reference for old logic
 */
@Deprecated
public class SpaceDimension {//implements ISpaceDimProperties {
    //public class SpaceDimension extends Dimension implements IDimProperties{
//
//	private static final SingleBiomeProvider PROVIDER = new SingleBiomeProvider(new SingleBiomeProviderSettings().setBiome(Biomes.THE_VOID));
//	
//	public SpaceDimension(World worldIn, DimensionType typeIn) {
//		super(worldIn, typeIn);
//	}
//
//	@Override
//	public ChunkGenerator<?> createChunkGenerator() {
//		return new SpaceChunkGenerator(this.world, PROVIDER, new SpaceGenerationSettings());
//	}
//
//	@Override
//	public BlockPos findSpawn(ChunkPos chunkPosIn, boolean checkValid) {
//		return null;
//	}
//
//	@Override
//	public BlockPos findSpawn(int posX, int posZ, boolean checkValid) {
//		return null;
//	}
//
//	@Override
//	public float calculateCelestialAngle(long worldTime, float partialTicks) {
//		return 0;
//	}
//
//	@Override
//	public boolean isSurfaceWorld() {
//		return false;
//	}
//
//	@Override
//	public Vec3d getFogColor(float celestialAngle, float partialTicks) {
//		return new Vec3d(0, 0, 0);
//	}
//
//	@Override
//	public boolean canRespawnHere() {
//		return false;
//	}
//
//	@Override
//	public boolean doesXZShowFog(int x, int z) {
//		return true;
//	}
//
//	@Override
//	public boolean shouldMapSpin(String entity, double x, double z, double rotation) {
//		return true;
//	}
//
//	@Override
//	public boolean doesWaterVaporize() {
//		return true;
//	}
//
//    @Override
//    public Vector3d modMotion(Entity ent) {
//        ent.fallDistance = 0;
//        if (ent instanceof ServerPlayerEntity)
//            ((ServerPlayerEntity) ent).connection.floatingTickCount = 0;
//        return ent.getMotion().add(0, 0.08, 0);
//    }
//
//    @Override
//    public boolean hasAir() {
//        return false;
//    }
//
//	@Override
//	public float[] calcSunriseSunsetColors(float celestialAngle, float partialTicks) {
//		// TODO Auto-generated method stub
//		return super.calcSunriseSunsetColors(celestialAngle, partialTicks);
//	}
//
//	@Override
//	public boolean isSkyColored() {
//		return false;
//	}
//
//	@Override
//	public IRenderHandler getSkyRenderer() {
//		return MoonSkyRenderer.INSTANCE;
//	}
//
//	@Override
//	public Vec3d getSkyColor(BlockPos cameraPos, float partialTicks) {
//		return new Vec3d(0, 0, 0);
//	}
//
}
