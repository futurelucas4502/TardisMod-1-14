package net.tardis.mod.world.dimensions;

import net.minecraft.client.world.DimensionRenderInfo;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.client.ICloudRenderHandler;
import net.minecraftforge.client.ISkyRenderHandler;
import net.minecraftforge.client.IWeatherRenderHandler;
import net.tardis.mod.client.renderers.sky.EmptyCloudRenderer;
import net.tardis.mod.client.renderers.sky.EmptyRenderer;
import net.tardis.mod.client.renderers.sky.EmptyWeatherRenderer;

@OnlyIn(Dist.CLIENT) //This is one of the few cases where we can be using OnlyIn - Vanilla uses this is a marker hack to strip bytecode from one side (server or client)
public class TardisSkyProperty extends DimensionRenderInfo {
    
	public TardisSkyProperty() {
		super(Float.NaN, false, FogType.NORMAL, false, false); //Set forceBrightLightmap to false so the color isn't lerped in LightTexture
	}
	
    public TardisSkyProperty(float cloudLevel, boolean hasGround, FogType skyType, boolean forceBrightLightmap,
                             boolean constantAmbientLight) {
        super(cloudLevel, hasGround, skyType, forceBrightLightmap, constantAmbientLight);
    }

    //getBrightnessDependentFogColor
    @Override
    public Vector3d func_230494_a_(Vector3d color, float sunHeight) {
        return new Vector3d(0, 0, 0);
    }

    //isFoggyAt
    @Override
    public boolean func_230493_a_(int camX, int camY) {
        return false;
    }

    @Override
    public ICloudRenderHandler getCloudRenderHandler() {
        return new EmptyCloudRenderer();
    }

    @Override
    public IWeatherRenderHandler getWeatherRenderHandler() {
        return new EmptyWeatherRenderer();
    }

    @Override
    public ISkyRenderHandler getSkyRenderHandler() {
        return new EmptyRenderer();
    }

}
